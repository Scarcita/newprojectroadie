import React, { useEffect, useState } from 'react';
import Layout from "../../src/components/shared/layout";
import Table from "../../src/components/ads/tabladatos";
import ModalAd from "../../src/components/ads/modalAds";
import Counter from '../../src/components/ads/counter';
import Image from "next/image";

import ImgFacebook from '../../public/Dashhoard/facebook.svg';
import ImgIg from '../../public/Dashhoard/instagram.svg';
import ImgMessenger from '../../public/Dashhoard/messenger.svg';
import ImgTelegram from '../../public/Dashhoard/telegram.svg';
import ImgTiktok from '../../public/Dashhoard/tikTok.svg';
import ImgTwitter from '../../public/Dashhoard/twitter.svg';
import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg';
import ImgYoutube from '../../public/Dashhoard/youtube.svg';
import BarChart from '../../src/components/shared/BarChart';
import CounterSingleWithCurrency from '../../src/components/shared/CounterSingleWithCurrency';

export default function Ads() {

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const getDashboard = async () => {
    setLoading(true);
    try {
      const response = await fetch(
        process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPostsAds/dashboard",
        {
          method: "GET",
        }
      );
      const res = await response.json();
      console.log("response", res);
      if(res.status){
        setData(res.data);
      }
    } catch (error) {
      console.log("🚀 SettigsTab ~ getSettings ~ error:", error)
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getDashboard();
  }, []);

  return (
    <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>
      <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
        <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Ads</div>
                <div className='flex flex-row '>
                  {/* <ModalAd 
                    reloadAds={reloadAds} 
                    setReloadAds={setReloadAds}
                  /> */}
                </div>                  
            </div>              
        </div>

        <div className='grid grid-cols-12 mt-[20px] gap-6'>

            <div className='col-span-12 md:col-span-12 lg:col-span-6'>

              <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Month overview</div>

              <div className='grid grid-cols-12 mt-[20px]'>                  
                <div className="col-span-12 shadow-md rounded-md mb-7 md:mb-0">
                  <BarChart loading={loading} data={data} title={'Ads Spend (Bs.)'} />
                </div>                  
              </div>
                
            </div>

            <div className='col-span-12 md:col-span-12 lg:col-span-6'>

              <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Month status</div>

              <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                  <Image
                      src={ImgFacebook}
                      layout='fixed'
                      alt='ImgFacebook'
                      width={56}
                      height={56}
                      
                  />
                  <div className='ml-[10px] self-center'>
                      <div>
                          <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.fb_ads_total } currency={'Bs'} />
                      </div>
                      <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                  </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                  <Image
                      src={ImgTiktok}
                      layout='fixed'
                      alt='ImgTikTok'
                      width={56}
                      height={56}
                      
                  />
                  <div className="ml-[10px]">
                      <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">
                        <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.tk_ads_total } currency={'Bs'} />
                      </div>
                      <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                  </div>                      
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                  <Image
                      src={ImgIg}
                      layout='fixed'
                      alt='ImgIg'
                      width={56}
                      height={56}
                      
                  />
                  <div className="ml-[10px]">
                      <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">
                        <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.ig_ads_total } currency={'Bs'} />
                      </div>
                      <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                  </div>                        
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                  <Image
                      src={ImgYoutube}
                      layout='fixed'
                      alt='ImgYoutube'
                      width={56}
                      height={56}
                      
                  />
                  <div className="ml-[10px]">
                      <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">
                        <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.yt_ads_total } currency={'Bs'} />
                      </div>
                      <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                  </div>
                </div>
              </div>                  
            </div>              
        </div>

        <div className='grid grid-cols-12 mt-[20px]'>
          <div className='col-span-12 md:col-span-12 lg:col-span-12'>
            <Table
              loading={loading}
              data={data.ads_created_this_month === undefined ? [] : data.ads_created_this_month}
              getDashboard={getDashboard}
            />
          </div>              
        </div>
      </div>
    </div>
  )
}

Ads.auth = true
// Ads.getLayout = function getLayout(page){
//   return (
//     <Layout>{page}</Layout>
//   )
// }
