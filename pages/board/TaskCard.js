import React  from 'react';
import { useEffect, useState } from 'react'
import { Spinner} from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import Image from 'next/image';

import dynamic from 'next/dynamic';
import TooltipEditItem from './tooltipEdit';


const TaskCard = ({ item, index }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [showIconClick, setShowIconClick] = useState(false);
  const [selectedPost, setSelectedPost] = useState(null);
  const [reloadCard, setReloadCard] = useState(false)

  const onClickDay = () =>{
    setShowModal(true)
    setShowIconClick(true)
  }

  const Draggable = dynamic(
    () =>
      import('react-beautiful-dnd').then(mod => {
        return mod.Draggable;
      }),
    {ssr: false},
  );

  useEffect(() => {
    fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/all/')
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setData(data.data)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

}, [reloadCard])

  const imgUrl = "";

  if(item !== undefined){
    if(item !== null){
      if( item.social_network !== null && item.social_network !== undefined) {
        switch (item.social_network) {
          case 'Facebook': 
              imgUrl = '/SocialMedia/Facebook.svg'
              break;
          case 'TikTok':
              imgUrl= '/SocialMedia/TikTok.svg'
              break;
          case 'Instagram':
              imgUrl = '/SocialMedia/Instagram.svg'
          break;
          case 'YouTube':
              imgUrl = '/SocialMedia/Youtube.svg'
          break;
          case 'Mailing':
              imgUrl = '/Plans/gmail.svg'
          break;
          case 'LinkedIn':
              imgUrl = '/SocialMedia/messenger.svg'
          break;
          case 'Twitter':
              imgUrl = '/SocialMedia/Twitter.svg'
          break;
              
          default:
              break;
      }
      }
      
    }
  }

  const imgType = "";

  if(item !== undefined){
      if (item !== null){
        if(item.type !== null && item.type !== undefined){
          switch (item.type) {
            case 'Image': 
              imgType = '/Board/image.png'         
                break;
            case 'Album':
              imgType= '/Board/image.png'
                break;
            case 'Video':
              imgType = '/Board/video.png'
            
            break;
            case 'story':
              imgType = '/Board/video.png'
            
            break;
            case 'Reel':
              imgType = '/Board/video.png'
            
            break;
                
            default:
                break;
        }
        }
      }
  }

  if(item !== undefined){
    if (item !== null){
      if(item.planned_datetime !== null && item.planned_datetime !== undefined){
        var eventdate = item.planned_datetime;
        var splitdate = eventdate.split('-');
        //console.log(splitdate);
        var day = splitdate[2];
        var year = splitdate[0];
      }
    }
  }
  if(item !== undefined){
    if (item !== null){
      if(item.planned_datetime !== null && item.planned_datetime !== undefined){
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var d = new Date(item.planned_datetime);
        var dayName = days[d.getDay()];
      }
    }
  }

  if(item !== undefined){
    if (item !== null){
      if(item.planned_datetime !== null && item.planned_datetime !== undefined){
        var months = ['January', 'February ', 'March ', 'April', 'May', 'June', 'July','August', 'September', 'Octuber', 'November', 'December'];
        var m = new Date(item.planned_datetime);
        var monthName = months[m.getMonth()];
      }
    }
  }

  return (
    item !== undefined ? 
    <Draggable

      key={item.id}
      draggableId={item.id.toString()}
      index={index}
    >
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          className='grid grid-cols-12'
          >
          <div className='col-span-12 md:col-span-12 lg:col-span-12 min-h-[105px] rounded-[10px] bg-[#fff] shadow-md mb-[15px] mr-[15px] pl-[10px] pt-[10px] pb-[10px] pr-[10px]'>
            {isLoading ?
              <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                  <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
              </div>
              :
              <div className='grid grid-cols-12 gap-3' 
              
              //onClick={() => {setShowIconClick(true)}}
              >
                <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                  <div className='grid grid-cols-12 gap-2 mt-[5px] mb-[5px]'>
                    <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                      <div>
                        <div className='text-[36px] font-bold text-center self-center'>{day}</div>
                      </div>
                    
                      <div>
                        <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayName}</p>
                        <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthName} {year}</p>
                        <p className='text-[12px] font-semibold text-right leading-4 whitespace-normal'>7:00 AM</p>
                        
                      </div>
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[15px] text-center flex flex-row justify-self-end'>
                      <div className='pr-[10px]'>
                        <Image
                          src={imgUrl}
                          alt='img'
                          layout='fixed'
                          width={40}
                          height={40}>
                        </Image>
                      </div>
                      <div className=''>
                        <Image
                          className=''
                          src={imgType}
                          alt='img'
                          layout='fixed'
                          width={40}
                          height={40}
                        />
                      </div>
                      
                    </div>
                  </div>
                  <div className='grid grid-cols-12 gap-3 mt-[10px]'>

                    <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                      {item.title !== null && item.title !== undefined ?
                      <div>
                        <div className='text-[12px] font-semibold leading-4 whitespace-normal'>
                            {item.title}
                        </div>
                      </div>
                      : <></>
                      }

                      {item.subtitle !== null && item.subtitle !== undefined ?

                        <div className='mt-[5px] mb-[5px]'>
                          <p className='text-[12px] text-[#000000] font-light leading-4 whitespace-normal'>{item.subtitle}</p>
                        </div>
                      : <></>
                      }
                      
                      <hr></hr>

                      {item.post_copy !== null && item.post_copy !== undefined ?
                      <div className='mt-[5px] mb-[5px]'>
                        <p className='text-[10px] font-light leading-4 whitespace-normal'>{item.post_copy}</p>
                      </div>
                      : <></>
                      }
                      {item.instructions !== null && item.instructions !== undefined ?
                      <div className='mt-[5px] mb-[5px]'>
                        <div className='font-semibold text-[14px] text-[#582BE7]'>Instructions: </div>
                        <p className='text-[12px] font-light leading-4 whitespace-normal'>{item.instructions}</p>
                      </div>
                      : <></>
                      }
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                      {item.media_url !== null && item.media_url !== undefined ?
                          <div className=''>
                            <Image
                                className='rounded-md'
                                src={item.media_url}
                                alt='img'
                                layout='responsive'
                                width={100}
                                height={100}
                                >
                            </Image>
                          </div>
                        : <></>
                      }

                    </div>
                    
                  </div>
                  </div>
              </div>
            }
              <TooltipEditItem 
                showModal={showModal} 
                setShowModal={setShowModal} 
                selectedPost={selectedPost}
                reloadCard={reloadCard}
                setReloadCard={setReloadCard}
              />
              <div className='grid grid-cols-12'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 justify-self-end mt-[10px]'>
                  <button
                    onClick={() => {setSelectedPost(item), setShowModal(true)}}
                    className=''
                    >
                      <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="w-6 h-6 text-[#643DCE] "
                      >
                          <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                      </svg>
                    </button>

                </div>

              </div>
          </div>
        </div>
      )}
      
    </Draggable>
  : <></>
  );
};

export default TaskCard;
