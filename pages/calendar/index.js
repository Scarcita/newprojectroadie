import React, { useEffect, useState } from 'react';
import Layout from "../../src/components/shared/layout";
import Anuncios from "../../src/components/calendar/anuncios"
import GraficCalendar from "../../src/components/calendar/graficCalendar"
import ModalCreate from "../../src/components/calendar/Modal"
import Counter from '../../src/components/calendar/counterPostDay';
import Client from '../../src/components/calendar/clientsActive';
import ModalDetalle from '../../src/components/calendar/modalDetalle';
import SelectClient from '../../src/components/calendar/selectClient';
import CounterPostDay from '../../src/components/calendar/counterPostDay';
import CounterPostWeek from '../../src/components/calendar/counterPostWeek';

export default function Calendar() { 
  const [clientId, setClientId] = useState(0);
  const [reloadCalendar, setReloadCalentar] = useState(false);
  const [reloadEdit, setReloadEdit] = useState(false);

  return (
      <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-8 ml-[30px] mr-[30px] mt-[45px]'>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Calendar</div>
                  <div className=''>
                    <ModalCreate
                      reloadCalendar={reloadCalendar}
                      setReloadCalentar={setReloadCalentar}
                    />
                  </div>
              </div>
          </div >
          <div className="mt-[24px] text-[18px] font-semibold">
            Status
          </div>
            <div className='grid grid-cols-12 gap-3 mt-[24px]'>
              
                <div className='col-span-12 md:col-span-6 lg:col-span-6 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>
                  <div className='w-[full] justify-center text-center ml-[30px] mr-[30px]'><Client/></div>
                  <div className="text-[12px] font-medium text-[#000000] ">clients active</div>
                </div>
                  
                  
                <div className='col-span-6 md:col-span-3 lg:col-span-3 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                  <div className="text-[32px] font-semibold text-[#000000] text-center" ><CounterPostDay/></div>
                  <div className="text-[12px] font-medium text-[#000000] text-center">Scheduled for today</div>

                </div>

                <div className='col-span-6 md:col-span-3 lg:col-span-3 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                  <div className="text-[32px] font-semibold text-[#000000] " ><CounterPostWeek/></div>
                  <div className="text-[12px] font-medium text-[#000000] ">Posted this week</div>
                  
                </div>
              
            </div >

            <div className='grid grid-cols-12 mt-[35px]'>
              <button
                disabled={clientId == 0 ? true : false}
                onClick={() => {setClientId(0)}}
                className='col-span-6 md:col-span-6 lg:col-span-6 justify-self-start text-[14px] md:text-[18px] lg:text-[14px] text-[#582BE7] font-semibold hover:text-[#D1D1D1]'>
                View All
              </button>
              <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                <SelectClient clientId={clientId} setClientId={setClientId}
                />
              </div>
            
              <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[30px] pb-[30px]'>
                <GraficCalendar 
                  clientId={clientId} 
                  reloadCalendar={reloadCalendar}
                  setReloadCalentar={setReloadCalentar}
                  // reloadEdit={reloadEdit}
                  // setReloadEdit={setReloadEdit}
                />

              </div>

          </div>
        </div>
        <div className='h-screen col-span-12 md:col-span-12 lg:col-span-4'>
          <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12'>
              <Anuncios
                clientId={clientId}
                setClientId={setClientId}
              />
            </div>
          </div>

        </div>
      </div>
  )
}
Calendar.auth = true