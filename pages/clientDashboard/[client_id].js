import React, { useEffect, useState } from 'react';
import Layout from '../../src/components/shared/layout';
import { useRouter } from 'next/router';
import Image from 'next/image';


import ImgFacebook from '../../public/Dashhoard/facebook.svg'
import ImgIg from '../../public/Dashhoard/instagram.svg'
import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
import ImgYoutube from '../../public/Dashhoard/youtube.svg'
import ImgTwitter from '../../public/Dashhoard/twitter.svg'
import ImgPerfil from '../../public/DashoardClient/perfil.svg'
import ImgPublic from '../../public/Calendar/public.svg'
import TableDashoardCli from "../../src/components/clientDashboard/tableDashoardCli"
import ModalPlans from "../../src/components/clientDashboard/ModalPlans"
import CounterDashoard from '../../src/components/clientDashboard/counterDashoard';
import ModalPageInsights from '../../src/components/clientDashboard/ModalPageInsights';
import CounterSingleWithCurrency from '../../src/components/shared/CounterSingleWithCurrency';
import DualDatasetBarChart from '../../src/components/clientDashboard/DualDatasetBarChart';
import Skeleton from 'react-loading-skeleton';
import LatestPost from '../../src/components/dashboard/latestPost';
import Link from 'next/link';



export default function DashoardClient() {
    
  const [showDots, setShowDots] = useState(true);
  const [reloadClients, setReloadClients] = useState(false);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const router = useRouter();
  const {client_id} = router.query;

  const getDashboard = async () => {
    setLoading(true);
    try {
      const response = await fetch(
        process.env.NEXT_PUBLIC_SERVER_URL + "/clients/dashboardByClient/" + client_id,
        {
          method: "GET",
        }
      );
      const res = await response.json();
      console.log("response", res);
      if(res.status){
        setData(res.data);
      }
    } catch (error) {
      console.log("🚀 SettigsTab ~ getSettings ~ error:", error)
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getDashboard();
  }, []);

  useEffect(() => {

    if(router.isReady){

    }
    
}, [router.isReady])

  return (
        <div className="ml-[30px] mr-[30px] mt-[45px]"> 
            <div className='grid grid-cols-12'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                    <div className='grid grid-cols-12'>
                        <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                            <div className='text-[20px] md:text-[32px] lg:text-[36px] text-[#000000] font-bold'>Client Dashboard</div>
                            <ModalPageInsights client_id={client_id} />
                            <div className="flex flex-row">

                                {
                                    loading 
                                    ?
                                    <Skeleton />
                                    :
                                    <>
                                        {
                                            data.client.facebook_url
                                            &&
                                            <Link href={data.client.facebook_url} className="pr-[5px]">
                                                <Image
                                                    src={ImgFacebook}
                                                    layout='fixed'
                                                    alt='ImgFacebook'
                                                    width={38}
                                                    height={38}
                                                />
                                            </Link>
                                        }

                                        {
                                            data.client.instagram_url
                                            &&
                                            <Link href={data.client.instagram_url} className="pr-[5px]">
                                                <Image
                                                src={ImgIg}
                                                layout='fixed'
                                                alt='ImgIg'
                                                width={38}
                                                height={38}
                                            />
                                            </Link>
                                        }

                                        {
                                            data.client.youtube_url
                                            &&
                                            <Link href={data.client.youtube_url} className="pr-[5px]">
                                                <Image
                                                src={ImgYoutube}
                                                layout='fixed'
                                                alt='ImgFacebook'
                                                width={38}
                                                height={38}
                                            />
                                            </Link>
                                        }

                                        {
                                            data.client.tiktok_url
                                            &&
                                            <Link href={data.client.tiktok_url} className="pr-[5px]">
                                                <Image
                                                src={ImgTiktok}
                                                layout='fixed'
                                                alt='ImgFacebook'
                                                width={38}
                                                height={38}
                                            />
                                            </Link>
                                        }

                                        {
                                            data.client.twitter_url
                                            &&
                                            <Link href={data.client.twitter_url} className="pr-[5px]">
                                                <Image
                                                src={ImgTwitter}
                                                layout='fixed'
                                                alt='ImgFacebook'
                                                width={38}
                                                height={38}
                                            />
                                            </Link>
                                        }

                                        {/* {
                                            data.client.linkedin_url
                                            &&
                                            <Link href={data.client.linkedin_url} className="pr-[5px]">
                                                <Image
                                                src={Img}
                                                layout='fixed'
                                                alt='ImgFacebook'
                                                width={38}
                                                height={38}
                                            />
                                            </Link>
                                        } */}
                                    </>
                                }
                            
                            </div>
                                
                        </div>                            
                    </div>
                </div >
            </div >

            <div className='grid grid-cols-12 mt-[20px]'>
                <div className='col-span-12 md:col-span-12 lg:col-span-4'>
                   <div className='grid grid-cols-12'>
                        <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                            <div className="flex flex-row">
                                <div className="">
                                    
                                    {loading 
                                        ? <Skeleton />
                                        : (data.client.img_url !== undefined && data.client.img_url !== null)
                                            ? <Image
                                                className='rounded-full'
                                                src={data.client.img_url}
                                                layout='fixed'
                                                alt='ImgPerfil'
                                                width={110}
                                                height={110}
                                                /> 
                                            : 
                                                <div style={{width: 110, height: 110, background: '#F3F3F3', borderRadius: '50%'}}></div>
                                            }
                                </div>
                                <div className="pl-[8px]">
                                    <p className="font-semibold text-[16px]">{loading ? <Skeleton /> : data.client.name !== undefined ? data.client.name : ''}</p>
                                    <p className="text-[15px]" >Client since: {loading ? <Skeleton /> : data.client.client_since !== undefined ? data.client.client_since : ''}</p>
                                    <p className="text-[15px]" >Has plan: <span className="text-[#643DCE] font-semibold">{loading ? <Skeleton /> : data.has_active_plan !== undefined ? data.has_active_plan : ''}</span></p>
                                    <p className="font-semibold text-[16px]">{loading ? <Skeleton /> : data.active_plan !== undefined ? data.active_plan.plan.name : ''}</p>
                                </div>

                            </div>
                            
                        </div>
                   </div>

                </div>

                <div className='col-span-12 md:col-span-12 lg:col-span-8'>
                    <div className="text-[18px] font-medium mb-[10px]">
                        Accounts status  <span className="text-[13px] font-normal">- Total</span>
                    </div>
                    <div className="grid grid-cols-12 gap-4">
                        <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                            <div>
                            <div className="flex flex-row">
                                <div className="text-[32px] font-semibold text-[#000000]">
                                    <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.active_plans_profit } currency={'Bs'} />
                                </div>
                                    
                            </div>
                            <div className="text-[10px] text-[#000000]">ACTIVE PLANS</div>
                            </div>
                            <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                            
                            </div>
                        </div>
                        <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                            <div>
                            <div className="flex flex-row">
                                <div className="text-[32px] font-semibold text-[#000000]">
                                    <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.all_plans_profit } currency={'Bs'} />
                                </div>                                    
                            </div>
                            <div className="text-[10px] text-[#000000]">PLANS TOTAL</div>
                            </div>
                            <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                            
                            </div>
                        </div>
                        <div className='col-span-12 md:col-span-12 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                            <div>
                            <div className="flex flex-row">
                                <div className="text-[32px] font-semibold text-[#000000]">
                                    <CounterSingleWithCurrency loading={loading} value={data.length == 0 ? 0 : data.all_plans_ads } currency={'Bs'} />
                                </div>                                    
                            </div>
                            <div className="text-[10px] text-[#000000]">ADS TOTAL</div>
                            </div>
                            <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                            
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>


            <div className="grid grid-cols-12 gap-6">
                
                <div className='col-span-12 md:col-span-12 lg:col-span-7'>
                    
                    <div className="text-[18px] font-medium mt-[30px]">
                    Accounts status  <span className="text-[13px] font-normal">- Total</span>
                    </div>

                    <div className="grid grid-cols-12">
                        <div className='col-span-12 md:col-span-12 lg:col-span-12'>                        
                             <DualDatasetBarChart loading={loading} data={data} title1={'Profit (Bs.)'} title2={'Ads Spend (Bs.)'} />
                        </div>
                    </div>

                </div>

                <div className='col-span-12 md:col-span-12 lg:col-span-5'>
                    <div className='grid grid-cols-12 gap-4'>
                        <div className="col-span-12 md:col-span-6 lg:col-span-6">
                    
                            <div className="text-[18px] font-medium mt-[30px] mb-[10px]">
                                Latest post
                            </div>
                   
                            <div className='col-span-12'>
                                <div>
                                    <LatestPost loading={loading} data={data.latest_posted_post} title={''}/>                  
                                </div>
                            </div>
                        
                        </div>

                        <div className="col-span-12 md:col-span-6 lg:col-span-6">

                            <div className="text-[18px] font-medium mt-[30px] mb-[10px]">
                            Next scheduled post
                            </div>

                            <div className='col-span-12'>
                                <div>
                                    <LatestPost loading={loading} data={data.next_scheduled_post} title={''}/> 
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

            <div className='grid grid-cols-12 mt-[30px]'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                    <div className='text-[17px] md:text-[15px] lg:text-[17px] text-[#000000] font-semibold'>Plans</div>
                    
                    <div className='flex flex-row '>

                    {router.isReady && 
                    
                        <ModalPlans client_id={client_id} reloadClients={reloadClients} setReloadClients={setReloadClients}
                        />
                    }
                    
                    </div>
                    
                </div>

                <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[30px]'>

                    {router.isReady && 

                    <TableDashoardCli client_id={client_id} reloadClients={reloadClients}
                    />
                    }

                </div>
            
            </div >
        </div>
    )
}

DashoardClient.auth = true;