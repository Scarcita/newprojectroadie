import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import Image from 'next/image'

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../public/Dashhoard/youtube.svg'

import TableAds from '../../src/components/clientPlan/TableAds'
import ModalPost from "../../src/components/clientPlan/ModalPost"
import { Spinner } from 'react-activity';
import "react-activity/dist/Spinner.css";
import CounterDirectValue from '../../src/components/clientPlan/counterDirectValue';
import ModalGenerateAccount from '../../src/components/clientPlan/ModalGenerateAccount';
import { Tab, Tabs } from '../../src/components/shared/Tabs';
import TableExtras from '../../src/components/clientPlan/TableExtras';
import ModalExtra from '../../src/components/clientPlan/ModalExtra';
import TablePosts from '../../src/components/clientPlan/TablePosts';
import ModalAd from '../../src/components/clientPlan/ModalAd';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt, faDashboard, faMoneyCheckDollar } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';

export default function ClientPlan() {

    const [reloadPosts, setReloadPosts] = useState(false);
    const [reloadAds, setReloadAds] = useState(false);
    const [loading, setLoading] = useState(true);
    const [loadingAccount, setLoadingAccount] = useState(true);

    const [data, setData] = useState([ ]);

    const [clientPlanData, setClientPlanData] = useState(null);
    const [clientPlanAccountData,setClientPlanAccountData] = useState(null);
    
    const [service_total, setServiceTotal] = useState(0);
    const [ads_total, setAdsTotal] = useState(0);
    const [total, setTotal] = useState(0);

    const [posts, setPosts] = useState([]);
    const [loadingPosts, setLoadingPosts] = useState(true); 
    const [ads, setAds] = useState([]);
    const [loadingAds, setLoadingAds] = useState(true);
    const [extras, setExtras] = useState([]);
    const [loadingExtras, setLoadingExtras] = useState(true);

    const [ads_fb, setAdsFb] = useState(0);
    const [ads_ig, setAdsIg] = useState(0);
    const [ads_tw, setAdsTw] = useState(0);
    const [ads_yt, setAdsYt] = useState(0);
    const [count_fb, setCountFb] = useState(0);
    const [count_fb_posted, setCountFbPosted] = useState(0);
    const [count_ig, setCountIg] = useState(0);
    const [count_ig_posted, setCountIgPosted] = useState(0);
    const [count_tk, setCountTk] = useState(0);
    const [count_tk_posted, setCountTkPosted] = useState(0);
    const [count_mailing, setCountMailing] = useState(0);
    const [count_mailing_posted, setCountMailingPosted] = useState(0);
    const [count_li, setCountLi] = useState(0);
    const [count_li_posted, setCountLiPosted] = useState(0);
    const [count_tw, setCountTw] = useState(0);
    const [count_tw_posted, setCountTwPosted] = useState(0);
    const [count_yt, setCountYt] = useState(0);
    const [count_yt_posted, setCountYtPosted] = useState(0);


    const router = useRouter();
    const { client_plan_id } = router.query;

    const loadClientPlanData = () => {
        setLoading(true);
        // setLoadingPosts(true);
        // setLoadingAds(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/get/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            
            if (data.status) {
                setClientPlanData(data.data);

                // setPosts(data.data.clients_plans_posts);
                
                // var tempAds = [];
                // data.data.clients_plans_posts.map((post) => {
                //     if(post.clients_plans_posts_ads.length > 0){
                //         post.clients_plans_posts_ads.map((ad) => {
                //             ad._matchingData.ClientsPlansPosts = post;
                //             tempAds.push(ad);
                //             alert(JSON.stringify(ad));
                //         });
                //     }
                // });

                // setAds(tempAds);
                
            } else {
                console.error(data.errors);
            }
            setLoading(false);
            // setLoadingPosts(false);
            // setLoadingAds(false);
        })
        .catch((e) => {
            console.error(e);
            setLoading(false);
            // setLoadingPosts(false);
            // setLoadingAds(false);
        });
    }

    const loadClientPlanAccountData = () => {
        setLoadingAccount(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/planAccount/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            
            if (data.status) {
                setClientPlanAccountData(data.data);
                
            } else {
                console.error(data.errors);
            }
            setLoadingAccount(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingAccount(false);
        });
    }

    const countPostsByPlatform = (platform) => {
        var filteredArr = posts.filter((post) => { return post.social_network == platform });
        var postedFilteredArr = posts.filter((post) => { return post.social_network == platform && post.status == 'POSTED' });
        
        switch (platform) {
            case 'Facebook':
                setCountFb(filteredArr.length);
                setCountFbPosted(postedFilteredArr.length);

                // if(clientPlanData != null){
                //     if('plan' in clientPlanData ){
                //         var unit_price = clientPlanData.plan.price / clientPlanData.plan.facebook_posts_qty;
                //         setServiceTotal(unit_price * filteredArr.length);
                //     }
                // }

                break;
            case 'Instagram':
                setCountIg(filteredArr.length);
                setCountIgPosted(postedFilteredArr.length);
                break;
            case 'Twitter':
                setCountTw(filteredArr.length);
                setCountTwPosted(postedFilteredArr.length);
                break;
            case 'Youtube':
                setCountYt(filteredArr.length);
                setCountYtPosted(postedFilteredArr.length);
                break;
            case 'Tiktok':
                setCountTk(filteredArr.length);
                setCountTkPosted(postedFilteredArr.length);
                break;
            case 'Mailing':
                setCountMailing(filteredArr.length);
                setCountMailingPosted(postedFilteredArr.length);
                break;
            case 'Linkedin':
                setCountLi(filteredArr.length);
                setCountLiPosted(postedFilteredArr.length);
                break;
            default:
                break;
        }
    }

    const sumAdsByPlatform = (platform) => {
        var filteredArr = ads.filter((ad) => { return ad._matchingData.ClientsPlansPosts.social_network == platform });
        var sum = 0;
        filteredArr.map((ad) => { sum += parseFloat(ad.amount) } );
        
        switch (platform) {
            case 'Facebook':
                setAdsFb(sum);
                break;
            case 'Instagram':
                setAdsIg(sum);
                break;
            case 'Twitter':
                setAdsTw(sum);
                break;
            case 'Youtube':
                setAdsYt(sum);
                break;
            // case 'Tiktok':
            //     setCountTk(sum);
            //     break;
            // case 'Linkedin':
            //     setCountLi(sum);
            //     break;
            default:
                break;
        }
    }
    
    const loadClientPlanPosts = () => {
        setLoadingPosts(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setPosts(data.data);              
            } else {
                console.error(data.errors);
            }
            setLoadingPosts(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingPosts(false);
        });
    }

    const loadClientPlanAds = () => {
        setLoadingAds(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPostsAds/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setAds(data.data);              
            } else {
                console.error(data.errors);
            }
            setLoadingAds(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingAds(false);
        });
    }

    const loadClientPlanExtras = () => {
        setLoadingExtras(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansExtras/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setExtras(data.data);              
            } else {
                console.error(data.errors);
            }
            setLoadingExtras(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingExtras(false);
        });
    }

    useEffect(() => {

        if(router.isReady){
            loadClientPlanData();
        }
        
    }, [router]);    

    useEffect(() => {
        if(clientPlanData != null){
            loadClientPlanAccountData();
            loadClientPlanPosts();
            loadClientPlanAds();
            loadClientPlanExtras();
        }       
    }, [clientPlanData]);

    useEffect(() => {
        if(posts.length > 0){
            countPostsByPlatform('Facebook');  
            countPostsByPlatform('Instagram');
            countPostsByPlatform('Twitter');
            countPostsByPlatform('Youtube');
            countPostsByPlatform('Tiktok');
            countPostsByPlatform('Mailing');
            countPostsByPlatform('Linkedin');
            
            // // if(clientPlanData != null){ Already checked on previous useEffect, when loading posts
            //     if('plan' in clientPlanData ){
            //         calculateCurrentServicePrice();
            //     }
            // // }
        }       
    }, [posts]);

    useEffect(() => {
        if(ads.length > 0){
            sumAdsByPlatform('Facebook');  
            sumAdsByPlatform('Instagram');
            sumAdsByPlatform('Twitter');
            sumAdsByPlatform('Youtube');
            // countPostsByPlatform('Tiktok');
            // countPostsByPlatform('Mailing');
            // countPostsByPlatform('Linkedin'); 
            // var sum = 0;
            // ads.map((ad) => { sum += parseFloat(ad.amount) } );
            // setAdsTotal(sum);
        }
    }, [ads]);

    // useEffect(() => {
    //     if(clientPlanData != null){
    //         //We will give included ads only if at least facebook posts qty are reached/posted as expected, otherwise charge full ads spent
    //         if('plan' in clientPlanData && clientPlanData.plan.included_ads_qty != 0 && count_fb_posted >= clientPlanData.plan.facebook_posts_qty) {
    //             setTotal((service_total + ads_total) - parseFloat(clientPlanData.plan.included_ads_qty));
    //         } else {
    //             setTotal(service_total + ads_total);
    //         }
    //     } else {
    //         setTotal(service_total + ads_total);
    //     }
    // }, [service_total, ads_total]);

    return (
        <div className='ml-[30px] mt-[30px] mr-[30px]'>
            <div className='grid grid-cols-12'>
              <div className='col-span-12 flex flex-row justify-between items-center'>
                <div className='flex flex-row gap-7 items-center'>
                    <div className='text-[24px] md:text-[32px] lg:text-[36px] text-[#000000] font-semibold'>
                        Client plan
                    </div>
                    <Link href={`/clientPlanSchedule/${client_plan_id}`}>
                        <FontAwesomeIcon icon={faCalendarAlt} color='#582BE7' />
                    </Link>
                    <Link href={`/clientPlanReport/${client_plan_id}`}>
                        <FontAwesomeIcon icon={faDashboard} color='#582BE7' />
                    </Link>
                    <Link href={`/clientPlanAccount/${client_plan_id}`}>
                        <FontAwesomeIcon icon={faMoneyCheckDollar} color='#582BE7' />
                    </Link>
                </div>
                <div>
                    {
                        loading ?
                        <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                        </div>
                        :
                        'plan' in clientPlanData ?
                        <>
                            <p className="text-lg font-semibold text-right">{clientPlanData.plan.name}</p>
                            <p className="text-sm font-light text-right">{clientPlanData.start_date} - {clientPlanData.end_date}</p>
                        </>
                        : <>{JSON.stringify(clientPlanData)}</>
                    }                    
                </div>
                  
              </div>
              
            </div >

            <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        {
                            loadingAccount ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : clientPlanAccountData ?
                                <>
                                    <div className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]"><CounterDirectValue value={clientPlanAccountData.service_total} /></div>
                                    {
                                        clientPlanAccountData.posted_media_types_by_type_count.map((posted_media, index)  => (
                                            <div key={`posted-media-key-${index}`} className='flex flex-row gap-1 text-xs'>
                                                <div>{posted_media.type}</div>
                                                <div>{posted_media.posted_posts_count}</div>
                                                <div>x({posted_media.post_multiplier})</div>
                                                <div>{posted_media.posted_posts_count_for_payment}</div>
                                            </div>
                                        ))
                                    }
                                    {
                                        clientPlanAccountData.reposted_posts_count > 0 &&
                                            <>
                                                <div className=' text-xs text-[#582BE7]'>REPOSTS</div>
                                            {
                                                clientPlanAccountData.reposted_media_types_by_type_count.map((posted_media, index)  => (
                                                    <div key={`reposted-media-key-${index}`} className='flex flex-row gap-1 text-xs text-[#582BE7]'>
                                                        <div>{posted_media.type}</div>
                                                        <div>{posted_media.posted_posts_count}</div>
                                                        <div>x({posted_media.post_multiplier})</div>
                                                        <div>{posted_media.posted_posts_count_for_payment}</div>
                                                    </div>
                                                ))
                                            }
                                            </>
                                        
                                    }
                                </>
                            : <></>
                        }       
                        <div className="text-[11px] text-[#000000]">PLAN</div>                 
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        {
                            loadingAccount ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : clientPlanData && clientPlanAccountData ?
                            <>
                                <div className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]"><CounterDirectValue value={clientPlanAccountData.ads_total} /></div>
                                <div className="text-[14px] md:text-[18px] lg:text-[10px] font-semibold text-[#FFFFFF]"><CounterDirectValue value={clientPlanData.plan.included_ads_qty} /> (Incluido)</div>
                            </>
                            : <></>
                        }
                        <div className="text-[11px] text-[#000000]">ADS</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#643DCE] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        {
                            loadingAccount ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : clientPlanData && clientPlanAccountData ?
                                <>
                                    <div className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#FFFFFF]"><CounterDirectValue value={clientPlanAccountData.social_media_total} /></div>
                                    {
                                        (!clientPlanData.paid && !clientPlanData.invoiced && 'plan' in clientPlanData) ? <ModalGenerateAccount loadClientPlanData={loadClientPlanData} client_plan_id={client_plan_id} plan={clientPlanData.plan} service_total={service_total} ads_total={ads_total} included_ads_qty={( count_fb_posted >= clientPlanData.plan.facebook_posts_qty) ? clientPlanData.plan.included_ads_qty : 0} total={total} /> : <></>
                                    }
                                </>
                            : <></>
                        } 
                        <div className="text-[11px] text-[#FFFFFF]">TOTAL</div>
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        {
                            loading ?
                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            </div>
                            :
                            clientPlanData ?
                            <>
                                <div className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]">{clientPlanData.paid ? 'YES' : 'NO'}</div>
                                <div className="text-[11px] text-[#000000]">{clientPlanData.paid_date}</div>
                                <div className="text-[11px] text-[#000000]">PAID</div>
                            </>
                            : <></>
                        }                        
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#F3F3F3] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        {
                            loading ?
                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            </div>
                            :
                            clientPlanData ?
                            <>
                                <div className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]">{clientPlanData.invoiced ? 'YES (#' + clientPlanData.invoice_number + ')' : 'NO'}</div>
                                <div className="text-[11px] text-[#000000]">{clientPlanData.invoiced_date}</div>
                                <div className="text-[11px] text-[#000000]">INVOICED</div>
                            </>
                            : <></>
                        }
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#643DCE] rounded-[16px] justify-between p-[10px]'>
                    <div>
                        {
                            loading ?
                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            </div>
                            :
                            clientPlanData ?
                            <>
                                <div className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#FFFFFF]">{clientPlanData.total_with_invoice}</div>
                                <div className="text-[11px] text-[#FFFFFF]">{clientPlanData.invoice_total} (Invoice)</div>
                                <div className="text-[11px] text-[#FFFFFF]">INVOICE TOTAL</div>
                            </>
                            : <></>
                        }
                    </div>
                </div>
            </div>

            <div className='mt-[27px] flex flex-row justify-between items-center'>                
                <div className="text-lg font-semibold">
                    Status
                </div>
                <div className='flex flex-row gap-2'>
                    <ModalExtra client_plan_id={client_plan_id} loadClientPlanData={loadClientPlanData} />
                    <ModalPost client_plan_id={client_plan_id} loadClientPlanData={loadClientPlanData} />
                </div>
            </div>

            <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0062E0] to-[#19AFFF] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        {
                            loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_fb_posted}/{count_fb}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.facebook_posts_qty ? ('/' + clientPlanData.plan.facebook_posts_qty) : ''}</div>
                        }                       
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                        <Image
                            src={ImgFacebook}
                            layout='fixed'
                            alt='ImgFacebook'
                            width={36}
                            height={36}
                            
                        />                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FCA759] via-[#E82D56] via-[#A22DB4] to-[#643DCE] rounded-[16px] justify-between pl-[10px] p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        {
                            loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_ig_posted}/{count_ig}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.instagram_posts_qty ? ('/' + clientPlanData.plan.instagram_posts_qty) : ''}</div>
                        }                            
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                        <Image
                            src={ImgIg}
                            layout='fixed'
                            alt='ImgIg'
                            width={36}
                            height={36}
                            
                        />                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#161616] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        {
                            loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_tk_posted}/{count_tk}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.tiktok_posts_qty ? ('/' + clientPlanData.plan.tiktok_posts_qty) : ''}</div>
                        }
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                        <Image
                            src={ImgTiktok}
                            layout='fixed'
                            alt='ImgTiktok'
                            width={36}
                            height={36}
                            
                        />                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0CA8F6] to-[#0096E1] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        {
                            loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_tw_posted}/{count_tw}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.twitter_posts_qty ? ('/' + clientPlanData.plan.twitter_posts_qty) : ''}</div>
                        }  
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                        <Image
                            src={ImgTwitter}
                            layout='fixed'
                            alt='ImgTelegram'
                            width={36}
                            height={36}
                            
                        />                    
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#DB0505] to-[#FF0000] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        {
                            loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_yt_posted}/{count_yt}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.youtube_posts_qty ? ('/' + clientPlanData.plan.youtube_posts_qty) : ''}</div>
                        }
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                        <Image
                            src={ImgYoutube}
                            layout='fixed'
                            alt='ImgYoutube'
                            width={36}
                            height={36}
                            
                        />
                    </div>
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FBBC04] to-[#FCD462] rounded-[16px] justify-between p-[8px]'>
                    <div>
                    <div className="flex flex-row">
                        {
                            loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_mailing_posted}/{count_mailing}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.mailing_posts_qty ? ('/' + clientPlanData.plan.mailing_posts_qty) : ''}</div>
                        }
                    </div>
                    <div className="text-[10px] text-[#FFFFFF]">POST</div>
                    </div>
                    <div className='pt-[10px]'>
                        <Image
                            src={ImgGmail}
                            layout='fixed'
                            alt='ImgGmail'
                            width={36}
                            height={36}
                            
                        />                    
                    </div>
                </div>
            </div>
            
            <div className='mt-[27px] flex flex-row justify-between items-center'>                
                <div className="text-[18px] font-semibold">
                    Ads overview
                </div>
                {/* <ModalAdFormer client_plan_id={client_plan_id} loadClientPlanData={loadClientPlanData} /> */}                
                <ModalAd client_plan_id={client_plan_id} loadClientPlanData={loadClientPlanData} />
            </div>

            <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgFacebook}
                        layout='fixed'
                        alt='ImgFacebook'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        {
                            loadingAds ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterDirectValue value={ads_fb} /></div>
                        }                        
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgTwitter}
                        layout='fixed'
                        alt='ImgTelegram'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        {
                            loadingAds ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterDirectValue value={ads_tw} /></div>
                        }
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgIg}
                        layout='fixed'
                        alt='ImgIg'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        {
                            loadingAds ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterDirectValue value={ads_ig} /></div>
                        }
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                    <Image
                        src={ImgYoutube}
                        layout='fixed'
                        alt='ImgYoutube'
                        width={56}
                        height={56}
                        
                    />
                    <div className="ml-[10px]">
                        {
                            loadingAds ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            : <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><CounterDirectValue value={ads_yt} /></div>
                        }
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                    </div>
                </div>

            </div>
        
            <div className='w-full mt-10 mb-4'>
                <Tabs>
                    <Tab label="Posts">
                        <div className="pb-4">
                            {/* <h2 className="text-lg font-medium mb-2">Tab 1 Content</h2> */}
                            {/* <p className="text-gray-700">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae
                            quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis
                            harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum!
                            Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius
                            earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia
                            aut! Impedit sit sunt quaerat, odit, tenetur error, harum nesciunt ipsum debitis quas
                            aliquid. Reprehenderit, quia. Quo neque error repudiandae fuga? Ipsa laudantium
                            molestias eos sapiente officiis modi at sunt excepturi expedita sint? Sed quibusdam
                            recusandae alias error harum maxime adipisci amet laborum.
                            </p> */}
                            
                            <TablePosts data={posts} loading={loadingPosts} loadClientPlanData={loadClientPlanData} editable={true}  />    
                        </div>
                    </Tab>
                    <Tab label="Ads">
                        <div className="pb-4">
                            <TableAds data={ads} loading={loadingAds} loadClientPlanData={loadClientPlanData} client_plan_id={client_plan_id} editable={true} />     
                        </div>
                    </Tab>
                    <Tab label="Extras">
                        <div className="pb-4">
                            <TableExtras  data={extras} loading={loadingExtras} loadClientPlanData={loadClientPlanData} />
                        </div>
                    </Tab>
                </Tabs>
            </div>

            {/* <div className='grid grid-cols-12 gap-3 mt-[35px]'>
              <div className='col-span-12 md:col-span-12 lg:col-span-5'>
                <div className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                        <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>Posts</div>
                        <div className='flex flex-row'>
                            {router.isReady &&
                                <ModalPost client_plan_id={client_plan_id} loadClientPlanData={loadClientPlanData}   />
                            }
                        </div>
                        
                    </div>
                
                </div >
                <div  className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>                    
                        <TablePosts data={posts} loading={loadingPosts} loadClientPlanData={loadClientPlanData}  />                    
                    </div>

                </div>
                  
              </div> 

              <div className='col-span-12 md:col-span-12 lg:col-span-7'>
                <div className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                        <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>Ads</div>
                        <div className='flex flex-row '>
                        {router.isReady &&
                            <ModalAds client_plan_id={client_plan_id} loadClientPlanData={loadClientPlanData}  />
                        }
                        </div>
                        
                    </div>

                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>                    
                        <TableAds data={ads} loading={loadingAds} />                    
                    </div>
                
                </div >
                  
              </div>

              
            </div> */}
            {/* </div>
            )} */}
     </div>
    )
}
ClientPlan.auth = true;
// ClientPlan.getLayout = function getLayout(page){
//     return (
//       <Layout>{page}</Layout>
//     )
//   }