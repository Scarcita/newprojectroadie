import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import Image from 'next/image'

import { Spinner } from 'react-activity';
import "react-activity/dist/Spinner.css";
import Skeleton from 'react-loading-skeleton';
import moment from 'moment';
import 'moment/locale/es';

export default function ClientPlanAccount() {
    
    const router = useRouter();

    const { client_plan_id } = router.query;

    const [loading, setLoading] = useState(true);

    const [data, setData] = useState(null);

    const loadClientPlanData = () => {
        setLoading(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/planAccount/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            
            if (data.status) {

                setData(data.data);

            } else {
                console.error(data.errors);
            }

            setLoading(false);
        })
        .catch((e) => {
            console.error(e);
            setLoading(false);
        });
    }

    useEffect(() => {
        if(router.isReady){
            loadClientPlanData();
        }        
    }, [router]);

    return (
        <div className=''>

            {/* WANT Header */}
            <div className='grid grid-cols-12 px-7 bg-[#F3F3F3] '>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 py-4'>
                    <div className='flex flex-row justify-between items-center'>
                        <div>
                            {/* <div className='text-2xl font-bold '>
                                WANT Digital Agency
                            </div> */}
                            <Image src={'/Want_Logo.png'} height={47} width={100} objectFit='contain' />                            
                        </div>
                        <div>
                            {
                                loading ?
                                    <Spinner color="#582BE7" size={17} speed={1} animating={true} />
                                :
                                data.client_plan && 'client' in data.client_plan ?
                                <div className='flex flex-row gap-2 items-center'>
                                    <div>
                                        <p className="text-base font-semibold text-right">{data.client_plan.client.name}</p>
                                        <p className="text-sm font-light text-right">{moment().format('llll')}</p>
                                    </div>
                                    <div>
                                        {
                                        data.client_plan.client.img_url !== undefined && data.client_plan.client.img_url !== null
                                            ? <Image
                                                className='rounded-full'
                                                src={data.client_plan.client.img_url}
                                                layout='fixed'
                                                alt='ImgPerfil'
                                                width={47}
                                                height={47}
                                                /> 
                                            : 
                                            <div style={{width: 110, height: 110, background: '#F3F3F3', borderRadius: '50%'}}></div>
                                        }
                                    </div>
                                </div>
                                : <></>
                            }                    
                        </div>  
                    </div>
                </div>                              
            </div>
            
            {/* Container */}
            <div className='w-full container mx-auto px-7 md:px-0'>

                {/* Title */}
                <div className='font-bold mt-7'>
                    ACCOUNT INSIGHTS <span className='font-light'> DETALLE DE PLAN</span>
                </div>                

                {/* Plan - Stats */}
                <div className='my-7 md:my-10'>
                    {
                        loading ?
                        <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                        :
                        data ?
                        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mx-auto'>
                            <div className='text-center'>
                                <div className="text-2xl text-[#643DCE] font-semibold">
                                    {
                                        loading ? <Skeleton /> : (data.client_plan.plan ? data.client_plan.plan.name : <></>)
                                    }         
                                </div>
                                <div className='text-base text-[#A7A7B7] font-light'>
                                    PLAN ACTUAL
                                </div>
                            </div>
                            <div className='text-center'>
                                <div className="text-2xl text-[#643DCE] font-semibold">
                                    {
                                        loading ? <Skeleton /> : (data.client_plan.plan ? (parseInt(data.client_plan.plan.price).toFixed(2) + ' Bs.') : <></>)
                                    }         
                                </div>
                                <div className='text-base text-[#A7A7B7] font-light'>
                                    PRECIO SERVICIO
                                </div>
                            </div>
                            {
                                data.client_plan.plan.has_included_ads &&
                                <div className='text-center'>
                                    <div className="text-2xl text-[#643DCE] font-semibold">
                                        {
                                            loading ? <Skeleton /> : (data.client_plan.plan.has_included_ads ? (parseFloat(data.client_plan.plan.included_ads_qty).toFixed(2) + ' Bs.') : 'No')
                                        }         
                                    </div>
                                    <div className='text-base text-[#A7A7B7] font-light'>
                                        ADS INCLUIDOS
                                    </div>
                                </div>
                            }                            
                            <div className='text-center self-end'>
                                <div className="text-xl text-[#643DCE] font-semibold">
                                    {
                                        loading ? <Skeleton /> : (data.client_plan ? (moment(data.client_plan.start_date).format('ll') +' a '+ moment(data.client_plan.end_date).format('ll')) : <></>)
                                    }         
                                </div>
                                <div className='text-base text-[#A7A7B7] font-light'>
                                    FECHA DE PUBLICACIÓN
                                </div>
                            </div>
                        </div>
                        : <></>
                    }                            
                </div>

                {/* Title */}
                <div className='font-bold'>
                    ACCOUNT INSIGHTS <span className='font-light'> DETALLE DE CUENTA</span>
                </div> 

                {/* Posts Detail */}
                <div className='my-4'>
                    <div className='bg-[#F3F3F3] rounded-md mx-auto p-7 md:p-10' style={{maxWidth: '577px', }}>
                        {
                            loading ?
                            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            :
                            data ?
                            <>
                                <div className='flex flex-row justify-between'>
                                    <div className='font-semibold'>Número de publicaciones realizadas</div>
                                    <div className='font-semibold'>{data.posted_posts_count}</div>
                                </div>

                                {
                                    data.posted_media_types_by_type_count.map((media_type, index) => (
                                            <div key={`posted-posts-${index}`} className='flex flex-row justify-between'>
                                                <div className='font-light'>{media_type.type} {media_type.posted_posts_count} (x{media_type.post_multiplier})</div>
                                                <div className='font-light'>{media_type.posted_posts_count_for_payment}</div>
                                            </div>
                                        )
                                    )
                                }

                                {
                                    data.reposted_media_types_by_type_count.map((media_type, index) => (
                                            <div key={`posted-posts-by-type-${index}`} className='flex flex-row justify-between'>
                                                <div className='font-light text-[#582BE7]'>{media_type.type} repost {media_type.posted_posts_count} (x{media_type.post_multiplier})</div>
                                                <div className='font-light text-[#582BE7]'>- {media_type.posted_posts_count_for_payment}</div>
                                            </div>
                                        )
                                    )
                                }

                                <div className='flex flex-row justify-between'>
                                    <div className='font-bold text'>Número de publicaciones para pago</div>
                                    <div className='font-bold text'>{data.posted_posts_count_for_payment}</div>
                                </div>

                            </>
                            : <></>
                        }                    
                    </div>                     
                </div>

                {
                    loading ? <Skeleton /> 
                    : (data && data.client_plan) ? 
                        data.client_plan.new_account_calc ?
                        (
                            <>
                                <div className=''>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}>

                                        <div className='flex flex-row justify-between'>
                                            <div className='font-base'>Total inversión en ads</div>
                                            <div className='font-base'>{data.ads_total ? data.ads_total.toFixed(2) + ' Bs.': ''}</div>
                                        </div>

                                        <div className='flex flex-row justify-between mt-7'>
                                            <div className='font-base'>Total servicio</div>
                                            <div className='font-base'>{data.social_media_total.toFixed(2) + ' Bs.'}</div>
                                        </div>
                                        
                                        {
                                            data.client_plan.plan.has_included_ads &&
                                            <div className='flex flex-row justify-between'>
                                                <div className='font-base text-[#643DCE]'>(Total ads incluidos)</div>
                                                <div className='font-base text-[#643DCE]'>({data.included_ads_total ? data.included_ads_total.toFixed(2) : 0}  Bs.)</div>
                                            </div>   
                                        }
                                        
                                        <div className='flex flex-row justify-between mt-4'>
                                            <div className='font-base'>Total servicio mailing  (x{data.posted_mailings_count} mails)</div>
                                            <div className='font-base'>{data.mailing_service_total.toFixed(2) + ' Bs.'}</div>
                                        </div>

                                        <div>
                                            <div className='flex flex-row justify-between mt-4'>
                                                <div className='font-base'>Total extras  (x{data.extras_internals_count})</div>
                                                <div className='font-base'>{data.extras_internals_total.toFixed(2) + ' Bs.'}</div>
                                            </div>    
                                            {
                                                data.client_plan.clients_plans_extras.map((extra, index) => {
                                                    if(extra.status != 'DELIVERED') return null;
                                                    if(extra.type == 'External'){
                                                        return null
                                                    }
                                                    return (
                                                        <div key={`posted-extras-${index}`}  className='flex flex-row justify-between'>
                                                            <div className='font-light'>{extra.detail}</div>
                                                            <div className='font-light'>{parseFloat(extra.price).toFixed(2) + ' Bs.'}</div>
                                                        </div> 
                                                    )
                                                })
                                            }
                                        </div>
                                        
                                        <div className='flex flex-row justify-between my-2'>
                                            <div className='font-bold text-lg'>Total Servicio</div>
                                            <div className='font-bold text-lg'>{data.uber_total.toFixed(2) + ' Bs.'}</div>
                                        </div>        
                                             
                                    </div>                     
                                </div>     

                                                             
                                <div className='mt-4 mb-7'>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}>
                                        <div className='flex flex-row justify-between'>
                                            <div className='font-bold text-2xl text-[#643DCE]'>Total + IVA</div>
                                            <div className='font-bold text-2xl text-[#643DCE]'>{(data.uber_total_with_iva).toFixed(2) + ' Bs.'}</div>
                                        </div>
                                    </div>
                                </div>

                                
                                <div className=''>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}> 
                                        <div className='flex flex-row justify-between mt-4'>
                                            <div className='font-base'>Total inversión ads adicionales</div>
                                            <div className='font-base'>{data.additional_ads_total.toFixed(2) + ' Bs.'}</div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className=''>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}>
                                        <div className='flex flex-row justify-between mt-4'>
                                            <div className='font-base'>Total extras terceros  (x{data.extras_externals_count})</div>
                                            <div className='font-base'>{data.extras_externals_total.toFixed(2) + ' Bs.'}</div>
                                        </div>    
                                        {
                                            data.client_plan.clients_plans_extras.map((extra, index) => {
                                                if(extra.status != 'DELIVERED') return null;
                                                if(extra.type != 'External'){
                                                    return null
                                                }
                                                return (
                                                    <div key={`posted-extras-${index}`}  className='flex flex-row justify-between'>
                                                        <div className='font-light'>{extra.detail}</div>
                                                        <div className='font-light'>{parseFloat(extra.price).toFixed(2) + ' Bs.'}</div>
                                                    </div> 
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                                
                                <div className='mt-4'>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}> 
                                        <div className='flex flex-row justify-between my-5'>
                                            <div className='font-bold text-xl'>Total</div>
                                            <div className='font-bold text-xl'>{(data.uber_total_extras).toFixed(2) + ' Bs.'}</div>
                                        </div>
                                    </div>
                                </div>   
                            </>
                        ) 
                        : 
                        (
                            <>
                                <div className=''>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}>
                                        {
                                            loading ?
                                            <Skeleton />
                                            :
                                            data ?
                                            <>
                                                <div className='flex flex-row justify-between'>
                                                    <div className='font-base'>Total servicio</div>
                                                    <div className='font-base'>{data.service_total.toFixed(2) + ' Bs.'}</div>
                                                </div>

                                                <div className='flex flex-row justify-between'>
                                                    <div className='font-base'>Total inversión en ads</div>
                                                    <div className='font-base'>{data.ads_total ? data.ads_total.toFixed(2) + ' Bs.': ''}</div>
                                                </div>
                                                
                                                {
                                                    data.client_plan.plan.has_included_ads &&
                                                    <div className='flex flex-row justify-between'>
                                                        <div className='font-base text-[#643DCE]'>Total ads incluidos</div>
                                                        <div className='font-base text-[#643DCE]'>{data.included_ads_total ? data.included_ads_total.toFixed(2) : 0}  Bs.</div>
                                                    </div>   
                                                }                                                             

                                                <div className='flex flex-row justify-between my-2'>
                                                    <div className='font-bold text-lg'>Total Social Media</div>
                                                    <div className='font-bold text-lg'>{data.social_media_total.toFixed(2) + ' Bs.'}</div>
                                                </div>

                                            </>
                                            : <></>
                                        }                    
                                    </div>                     
                                </div>
                                
                                
                                <div className=''>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}> 
                                        {
                                            
                                            loading ?
                                                <Skeleton />
                                                :
                                                data ?
                                                <>
                                                    <div className='flex flex-row justify-between mt-4'>
                                                        <div className='font-base'>Total servicio mailing  (x{data.posted_mailings_count} mails)</div>
                                                        <div className='font-base'>{data.mailing_service_total.toFixed(2) + ' Bs.'}</div>
                                                    </div>
                                                </>
                                                : <></>
                                        }

                                    </div>
                                </div>

                                
                                <div className=''>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}> 
                                        {
                                            
                                            loading ?
                                                <Skeleton />
                                                :
                                                data ?
                                                <>
                                                    <div className='flex flex-row justify-between mt-4'>
                                                        <div className='font-base'>Total extras  (x{data.extras_count})</div>
                                                        <div className='font-base'>{data.extras_total.toFixed(2) + ' Bs.'}</div>
                                                    </div>    
                                                    {
                                                        data.client_plan.clients_plans_extras.map((extra, index) => {
                                                            return (
                                                                <div key={`posted-extras-${index}`}  className='flex flex-row justify-between'>
                                                                    <div className='font-light'>{extra.detail}</div>
                                                                    <div className='font-light'>{parseFloat(extra.price).toFixed(2) + ' Bs.'}</div>
                                                                </div> 
                                                            )
                                                        })
                                                    }                         

                                                </>
                                                : <></>
                                        }

                                    </div>
                                </div>

                                
                                <div className='mt-4'>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}> 
                                        {
                                            
                                            loading ?
                                                <Skeleton />
                                                :
                                                data ?
                                                <>                  

                                                    <div className='flex flex-row justify-between my-5'>
                                                        <div className='font-bold text-xl'>Total</div>
                                                        <div className='font-bold text-xl'>{(data.uber_total).toFixed(2) + ' Bs.'}</div>
                                                    </div>

                                                </>
                                                : <></>
                                        }

                                    </div>
                                </div>
                                
                                
                                <div className='mt-4 mb-7'>
                                    <div className='mx-auto' style={{maxWidth: '577px'}}> 
                                        {
                                            
                                            loading ?
                                                <Skeleton />
                                                :
                                                data ?
                                                <>                        

                                                    <div className='flex flex-row justify-between'>
                                                        <div className='font-bold text-2xl text-[#643DCE]'>Total + IVA</div>
                                                        <div className='font-bold text-2xl text-[#643DCE]'>{(data.uber_total_with_iva).toFixed(2) + ' Bs.'}</div>
                                                    </div>

                                                </>
                                                : <></>
                                        }

                                    </div>
                                </div>
                            </>
                        )
                    : <></>
                }

            </div>

        </div>
    )
}