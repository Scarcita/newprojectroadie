import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import Image from 'next/image'
import { Spinner } from 'react-activity';
import "react-activity/dist/Spinner.css";
import Skeleton from 'react-loading-skeleton';
import moment from 'moment';
import 'moment/locale/es';
import { getMediaUrlByType } from '../../src/utils';

export default function ClientPlanReport() {

    const [loading, setLoading] = useState(true);

    const [data, setData] = useState(null);
    const [clientPlanData, setClientPlanData] = useState(null);
    const [posts, setPosts] = useState([]);

    const router = useRouter();
    const { client_plan_id } = router.query;

    const loadClientPlanReportData = async () => {
        setLoading(true);

        try {
            const response = await fetch(process.env.NEXT_PUBLIC_SERVER_URL  + '/clientsPlans/planReport/' + client_plan_id);
            const res = await response.json();
            console.log("res", res);
            if (res.status) {
                setData(res.data);
                setClientPlanData(res.data.client_plan);
                setPosts(res.data.posted_posts);
            } else {
                console.error(res.errors);
            }
        } catch (error) {
            console.log("Error in loadClientPlanReportData", error);
        } finally {
            setLoading(false);
        }
    }    

    useEffect(() => {
        if(router.isReady){
            loadClientPlanReportData();
        }        
    }, [router]);

    return (
        <div className=''>

            {/* WANT Header */}
            <div className='grid grid-cols-12 px-7 bg-[#F3F3F3] '>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 py-4'>
                    <div className='flex flex-row justify-between items-center'>
                        <div>
                            {/* <div className='text-2xl font-bold '>
                                WANT Digital Agency
                            </div> */}
                            <Image src={'/Want_Logo.png'} height={47} width={100} objectFit='contain' />                            
                        </div>
                        <div>
                            {
                                loading ?
                                    <Spinner color="#582BE7" size={17} speed={1} animating={true} />
                                :
                                clientPlanData && 'client' in clientPlanData ?
                                <div className='flex flex-row gap-2 items-center'>
                                    <div>
                                        <p className="text-base font-semibold text-right">{clientPlanData.client.name}</p>
                                        <p className="text-sm font-light text-right">{moment().format('llll')}</p>
                                    </div>
                                    <div>
                                        {
                                        clientPlanData.client.img_url !== undefined && clientPlanData.client.img_url !== null
                                            ? <Image
                                                className='rounded-full'
                                                src={clientPlanData.client.img_url}
                                                layout='fixed'
                                                alt='ImgPerfil'
                                                width={47}
                                                height={47}
                                                /> 
                                            : 
                                            <div style={{width: 110, height: 110, background: '#F3F3F3', borderRadius: '50%'}}></div>
                                        }
                                    </div>
                                </div>
                                : <></>
                            }                    
                        </div>  
                    </div>
                </div>                              
            </div>
            
            <div className='w-full container mx-auto px-4'>

                {/* Title */}
                <div className='text-lg font-bold mt-7'>
                    DETALLE DE PLAN
                </div>

                {/* Plan - Stats */}
                <div className='my-20'>
                    {
                        loading ?
                        <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                        :
                        data ?
                        <div className='grid grid-cols-3 gap-4'>
                            <div className='text-center'>
                                <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                    {
                                        loading ? <Skeleton /> : (data.posted_posts_count ? data.posted_posts_count : <></>)
                                    }         
                                </div>
                                <div className='text-base text-[#A7A7B7] font-light'>
                                    NÚMERO DE POSTS
                                </div>
                            </div>
                            <div className='text-center'>
                                <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                    {
                                        loading ? <Skeleton /> : (data.ads_total ? (data.ads_total + ' Bs.') : <></>)
                                    }         
                                </div>
                                <div className='text-base text-[#A7A7B7] font-light'>
                                    INVERSIÓN ADS
                                </div>
                            </div>
                            <div className='text-center self-end'>
                                <div className="text-base md:text-2xl text-[#643DCE] font-semibold">
                                    {
                                        loading ? <Skeleton /> : (clientPlanData ? (moment(clientPlanData.start_date).format('ll') +' a '+ moment(clientPlanData.end_date).format('ll')) : <></>)
                                    }         
                                </div>
                                <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                    FECHA DE PUBLICACIÓN
                                </div>
                            </div>
                        </div>
                        : <></>
                    }                            
                </div>

                {/* Facebook - Title */}
                <div className='text-lg font-base'>
                    <div className='flex flex-row items-center gap-2'>
                        <Image
                            className='rounded-full'
                            src={'/SocialMedia/Facebook.svg'}
                            alt='Facebook'
                            width={47}
                            height={47}
                        /> 
                        <span><span className='font-bold'>FACEBOOK</span> ESTADÍSTICAS</span>
                    </div>
                </div>

                {/* Facebook - Stats */}
                <div className='my-20'>
                    <div className=''>
                        {
                            loading ?
                            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            :
                            data ?
                            <>
                                <div className='grid grid-cols-12'>

                                    <div className='col-span-12 md:col-span-3 lg:col-span-3'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.fb_posted_posts_count ? data.fb_posted_posts_count : <></>)
                                                }         
                                            </div>
                                            <div className='text-[#A7A7B7] font-light'>
                                                NÚMERO DE PUBLICACIONES
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-12 md:col-span-3 lg:col-span-3 mt-2 md:mt-0'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ads_total_fb ? (data.ads_total_fb + ' Bs.') : <></>)
                                                }         
                                            </div>
                                            <div className='text-[#A7A7B7] font-light'>
                                                INVERSIÓN ADS
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-12 md:col-span-3 lg:col-span-3 mt-2 md:mt-0'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.page_insights ? data.page_insights[0]['values'][0]['value'] : <></>)
                                                }         
                                            </div>
                                            <div className='text-[#A7A7B7] font-light'>
                                                NUEVOS LIKES A LA PÁGINA
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-12 md:col-span-3 lg:col-span-3 mt-2 md:mt-0'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.page_insights ? data.page_insights[1]['values'][0]['value'] : <></>)
                                                }         
                                            </div>
                                            <div className='text-[#A7A7B7] font-light'>
                                                DISLIKES A LA PÁGINA
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div className='grid grid-cols-12 mt-10'>  

                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.posts_impressions_unique_people_reached ? data.posts_impressions_unique_people_reached : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                PERSONAS ALCANZADAS
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.posts_impressions ? data.posts_impressions : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                IMPRESIONES
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div  className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.posts_impressions_organic_unique ? data.posts_impressions_organic_unique : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                PERSONAS ALCANZADAS
                                                (Orgánico)
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.posts_impressions_organic ? data.posts_impressions_organic : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                IMPRESIONES(Orgánico)
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.posts_impressions_paid_unique ? data.posts_impressions_paid_unique : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                PERSONAS ALCANZADAS
                                                (Pagado)
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.posts_impressions_paid ? data.posts_impressions_paid : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                IMPRESIONES
                                                (Pagado)
                                            </div>
                                        </div>
                                    </div>

                                </div>   
                            </>
                            : <></>
                        }                    
                    </div>                     
                </div>

                {/* Facebook - Title */}
                <div className='text-lg font-base'>
                    <div className='flex flex-row items-center gap-2'>
                        <Image
                            className='rounded-full'
                            src={'/SocialMedia/Facebook.svg'}
                            alt='Facebook'
                            width={47}
                            height={47}
                        /> 
                        <span><span className='font-bold'>FACEBOOK</span> PUBLICACIONES REALIZADAS</span>
                    </div>
                </div>

                {/* Facebook - Posts */}                
                <div className='my-20 grid grid-cols-4 gap-4'>  
                    {
                        loading ? <Spinner color="#582BE7" size={17} speed={1} animating={true} />
                        : 
                        posts.length > 0 ? 
                        
                            posts.filter(post => {
                                return post.social_network == 'Facebook';
                            }).map((post, index) => {

                                const thumbnail_url = post.thumbnail_url ? post.thumbnail_url : '/SocialMedia/no_img.jpg';

                                // const post_content = {
                                //     username: clientPlanData.client.name,
                                //     userImg : clientPlanData.client.img_url ? clientPlanData.client.img_url : null,
                                //     imageSrc: thumbnail_url,
                                //     postType : post.type,
                                //     caption: post.post_copy,
                                //     docId: post.id.toString(),
                                //     userLikedPhoto: false,
                                //     likes: [],
                                //     comments: [],
                                //     dateCreated: post.created,
                                // }
                                return (
                                    // <div key={`facebook-${index}`} >
                                    //     <IgPost content={post_content} />
                                    // </div>
                                    <div key={`facebook-${index}`} className='text-center'>
                                        <Image
                                            src={thumbnail_url}
                                            width={320}
                                            height={320}
                                            objectFit='contain'
                                        />
                                        <div className='flex flex-row justify-center items-center gap-2 mt-1'>
                                            {
                                                post.repost &&
                                                <div className='rounded-lg py-1 px-2 border border-[#582BE7] text-[#582BE7] text-xs font-light'>REPOST</div>
                                            }
                                            <Image
                                                src={getMediaUrlByType(post.type)}
                                                width={37}
                                                height={37}
                                                objectFit='contain'
                                                className='mt-2'
                                            />
                                        </div>
                                    </div>
                                )
                            })

                            : <></>
                    }  
                </div>

                {/* Facebook - Title */}
                <div className='text-lg font-base'>
                    <div className='flex flex-row items-center gap-2'>
                        <Image
                            className='rounded-full'
                            src={'/SocialMedia/Facebook.svg'}
                            alt='Facebook'
                            width={47}
                            height={47}
                        /> 
                        <span><span className='font-bold'>FACEBOOK</span> DETALLE DE PUBLICACIONES</span>
                    </div>
                </div>

                {/* Facebook - Table */}
                <div className='my-20 grid grid-cols-12'>

                    <div className='col-span-12'>

                        <div className='hidden md:block'>
                            <table className="table-auto w-full border-separate border-spacing-y-7">
                                <thead className='text-gray-500 '>
                                    <tr>
                                        <th>Post</th>
                                        <th>Fecha</th>
                                        <th>Alcance</th>
                                        <th>Impresiones</th>
                                        <th>Reacciones</th>
                                        <th>Comentarios</th>
                                        <th>Compartidos</th>
                                        <th>Clicks</th>
                                        <th>Ads</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    {
                                        loading ? <tr><td><Skeleton /> </td></tr>
                                        : 
                                        posts.length > 0 ? 
                                        
                                            posts.filter(post => {
                                                return post.social_network == 'Facebook';
                                            }).map((post, index) => {

                                                const thumbnail_url = post.thumbnail_url ? post.thumbnail_url : '/SocialMedia/no_img.jpg';

                                                var total_post_ads = 0;

                                                if(post.clients_plans_posts_ads){
                                                    post.clients_plans_posts_ads.map(post_ad => {
                                                        total_post_ads += parseFloat(post_ad.amount);
                                                    })
                                                }

                                                return (        
                                                    <tr key={`facebook-row-${index}`}>                                                
                                                        <td className='text-center'>
                                                            <div className='flex flex-row gap-2 justify-center'>
                                                                <Image
                                                                    src={getMediaUrlByType(post.type)}
                                                                    width={17}
                                                                    height={17}
                                                                    objectFit='contain'
                                                                />     
                                                                <Image
                                                                    src={thumbnail_url}
                                                                    width={57}
                                                                    height={57}
                                                                    objectFit='contain'
                                                                />
                                                            </div>
                                                        </td>
                                                        <td className='text-center'>
                                                            <div className='flex flex-col'>
                                                                {post.planned_datetime}
                                                                {
                                                                    post.repost &&
                                                                    <span className='rounded-lg mt-1 py-1 px-1 border border-[#582BE7] text-[#582BE7] text-xs font-light'>REPOST</span>
                                                                }
                                                            </div>
                                                        </td>                                                    
                                                        <td className='text-center'>
                                                            {
                                                                post.insights && post.insights.length >= 5
                                                                ? 
                                                                <div className='text-center'>
                                                                    <div className='text-center text-xl font-bold'>{post.insights[0].values[0].value}</div>      
                                                                    <div className='flex flex-row gap-4 justify-center'>
                                                                        <div className='text-base'>{post.insights[2].values[0].value}</div>
                                                                        <div className='text-base text-[#643DCE]'>{post.insights[4].values[0].value}</div>
                                                                    </div>                                                      
                                                                </div> 
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                                
                                                        <td className='text-center'>
                                                            {
                                                                post.insights && post.insights.length >= 6
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-xl font-bold'>{post.insights[1].values[0].value}</div>      
                                                                    <div className='flex flex-row gap-4 justify-center'>
                                                                        <div className='text-base'>{post.insights[3].values[0].value}</div>
                                                                        <div className='text-base text-[#643DCE]'>{post.insights[5].values[0].value}</div>
                                                                    </div>                                                      
                                                                </div> 
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                               
                                                        <td className='text-center'>
                                                            {
                                                                post.reactions_insights
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.reactions_insights.reactions.summary.total_count}</div>
                                                                    <div className='text-center'>👍❤️🤗😆😮😢😡</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                         
                                                        <td className='text-center'>
                                                            {
                                                                post.reactions_insights
                                                                ?
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.reactions_insights.comments.summary.total_count}</div>
                                                                    <div className='text-center'>💬</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                         
                                                        <td className='text-center'>
                                                            {
                                                                post.reactions_insights
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.reactions_insights.shares.count}</div>
                                                                    <div className='text-center'>📣</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                        
                                                        <td className='text-center'>
                                                            {
                                                                post.insights && post.insights.length >= 7
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.insights[6].values[0].value}</div> 
                                                                    <div className='text-center'>👆</div>                                                       
                                                                </div> 
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                        
                                                        <td className='text-center'>
                                                            {
                                                                post.clients_plans_posts_ads
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-semibold'>{total_post_ads + ' Bs.'}</div>
                                                                    <div className='text-center'>💵</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })

                                            : <></>
                                    }

                                </tbody>
                            </table>
                        </div>

                        <div className='md:hidden'>
                            <div>
                                
                                {
                                    loading ? <tr><td><Skeleton /> </td></tr>
                                    : 
                                    posts.length > 0 ? 
                                    
                                        posts.filter(post => {
                                            return post.social_network == 'Facebook';
                                        }).map((post, index) => {

                                            const thumbnail_url = post.thumbnail_url ? post.thumbnail_url : '/SocialMedia/no_img.jpg';

                                            var total_post_ads = 0;

                                            if(post.clients_plans_posts_ads){
                                                post.clients_plans_posts_ads.map(post_ad => {
                                                    total_post_ads += parseFloat(post_ad.amount);
                                                })
                                            }

                                            return (        
                                                <div key={`facebook-row-${index}`}>

                                                    <div className='bg-[#fff] shadow-md rounded-[20px] py-[20px] mb-[20px] mx-[27px]'>

                                                        <div className='px-[20px] flex flex-row gap-1 items-center justify-between'>
                                                            <div className='flex flex-row gap-1'>
                                                                <div className="w-[48px] h-[48px] rounded-full">
                                                                    {post.clients_plan.client.img_url != null && post.clients_plan.client.img_url.length > 0 ?
                                                                    
                                                                        <Image 
                                                                            className='rounded-full'
                                                                            src={post.clients_plan.client.img_url}
                                                                            alt='media'
                                                                            layout='fixed'
                                                                            height={48}
                                                                            width={48}
                                                                        /> 
                                                                        : <></>  
                                                                    }    
                                                                </div>
                                                                <div className="pl-[10px]">        
                                                                    {post.clients_plan.client.name !== null ?
                                                                        <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal '>
                                                                        {post.clients_plan.client.name}
                                                                        </p>
                                                                        : <></>
                                                                    }
                                                                    <p className='text-[12px]'>
                                                                        {moment(post.planned_datetime).format("ddd")} {moment(post.planned_datetime).format("DD")} {moment(post.planned_datetime).format("MMM")}, {moment(post.planned_datetime).format("YYYY")}
                                                                    </p>    
                                                                </div>   
                                                            </div>
                                                            <div>
                                                                <Image
                                                                    src={getMediaUrlByType(post.type)}
                                                                    width={27}
                                                                    height={27}
                                                                    objectFit='contain'
                                                                />    
                                                                {
                                                                    post.repost &&
                                                                    <span className='rounded-lg mt-1 py-1 px-1 border border-[#582BE7] text-[#582BE7] text-xs font-light'>REPOST</span>
                                                                }
                                                            </div> 
                                                        </div>
                                                        
                                                        <div className='mt-[15px] mb-[7px]'>
                                                            <Image
                                                                src={thumbnail_url}
                                                                width={570}
                                                                height={570}
                                                                layout='responsive'
                                                                // objectFit='contain'
                                                            />
                                                        </div>

                                                        <div className='px-[20px]'>
                                                            {post.post_copy !== null && post.post_copy !== undefined ?
                                                                <><p className='mt-[7px] text-[12px] whitespace-normal line-clamp-2'>{post.post_copy}</p><hr className='my-2'></hr></>
                                                                : <></>
                                                            }
                                                        </div>

                                                        <div className='px-[20px]'>

                                                            <div className='flex flex-row items-center justify-between mb-2'>

                                                                <div>
                                                                    <h4 className='font-bold'>Alcance</h4>
                                                                    <div className='flex flex-row gap-1 justify-center'>
                                                                        <div className='text-xs'>Orgánico</div>
                                                                        <div className='text-xs text-[#643DCE]'>Pagado</div>
                                                                    </div> 
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.insights && post.insights.length >= 5
                                                                        ? 
                                                                        <div className='text-center'>
                                                                            <div className='text-center text-xl font-bold'>{post.insights[0].values[0].value}</div>      
                                                                            <div className='flex flex-row gap-4 justify-center'>
                                                                                <div className='text-base'>{post.insights[2].values[0].value}</div>
                                                                                <div className='text-base text-[#643DCE]'>{post.insights[4].values[0].value}</div>
                                                                            </div>                                                      
                                                                        </div> 
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div>    
                                                                
                                                            </div>

                                                            <div className='flex flex-row items-center justify-between mb-2'>

                                                                <div>
                                                                    <h4 className='font-bold'>Impresiones</h4>
                                                                    <div className='flex flex-row gap-1 justify-center'>
                                                                        <div className='text-xs'>Orgánico</div>
                                                                        <div className='text-xs text-[#643DCE]'>Pagado</div>
                                                                    </div> 
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.insights && post.insights.length >= 6
                                                                        ? 
                                                                        <div>
                                                                            <div className='text-center text-xl font-bold'>{post.insights[1].values[0].value}</div>      
                                                                            <div className='flex flex-row gap-2 justify-center'>
                                                                                <div className='text-base'>{post.insights[3].values[0].value}</div>
                                                                                <div className='text-base text-[#643DCE]'>{post.insights[5].values[0].value}</div>
                                                                            </div>                                                      
                                                                        </div> 
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div>
                                                                
                                                            </div>

                                                            <div className='flex flex-row items-center justify-between mb-3'>

                                                                <div>
                                                                    <h4 className='font-bold'>Reacciones</h4>
                                                                    <div className='text-xs'>👍❤️🤗😆😮😢😡</div>
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.reactions_insights
                                                                        ? 
                                                                        <div>
                                                                            <div className='text-center text-2xl font-bold'>{post.reactions_insights.reactions.summary.total_count}</div>                                                                        
                                                                        </div>
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div> 
                                                                
                                                            </div>

                                                            <div className='flex flex-row items-center justify-between mb-2'>

                                                                <div>
                                                                    <h4 className='font-bold'>💬 Comentarios</h4>
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.reactions_insights
                                                                        ?
                                                                        <div>
                                                                            <div className='text-center text-2xl font-bold'>{post.reactions_insights.comments.summary.total_count}</div>
                                                                        </div>
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div>    
                                                                
                                                            </div>

                                                            <div className='flex flex-row items-center justify-between mb-2'>

                                                                <div>
                                                                    <h4 className='font-bold'>📣 Compartidos</h4>
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.reactions_insights
                                                                        ? 
                                                                        <div>
                                                                            <div className='text-center text-2xl font-bold'>{post.reactions_insights.shares.count}</div>
                                                                        </div>
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div>   
                                                                
                                                            </div>

                                                            <div className='flex flex-row items-center justify-between mb-2'>

                                                                <div>
                                                                    <h4 className='font-bold'>👆 Clicks</h4>
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.insights && post.insights.length >= 7
                                                                        ? 
                                                                        <div>
                                                                            <div className='text-center text-2xl font-bold'>{post.insights[6].values[0].value}</div>                                                  
                                                                        </div> 
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div>   
                                                                
                                                            </div>

                                                            <div className='flex flex-row items-center justify-between'>

                                                                <div>
                                                                    <h4 className='font-bold'>💵 Ads</h4>
                                                                </div>

                                                                <div className='text-center'>
                                                                    {
                                                                        post.clients_plans_posts_ads
                                                                        ? 
                                                                        <div>
                                                                            <div className='text-center text-2xl font-bold'>{total_post_ads + ' Bs.'}</div>
                                                                        </div>
                                                                        : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                    }
                                                                </div>
                                                                
                                                            </div>

                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            )
                                        })

                                        : <></>
                                }

                            </div>
                           
                        </div>

                    </div>

                </div>

                {/* Instagram - Title */}
                <div className='text-lg font-base'>
                    <div className='flex flex-row items-center gap-2'>
                        <Image
                            className='rounded-full'
                            src={'/SocialMedia/Instagram.svg'}
                            alt='Instagram'
                            width={47}
                            height={47}
                        /> 
                        <span><span className='font-bold'>INSTAGRAM</span> ESTADÍSTICAS</span>
                    </div>
                </div>

                {/* Instagram - Stats */}
                <div className='my-20'>
                    <div className=''>
                        {
                            loading ?
                            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            :
                            data ?
                            <>
                                <div className='grid grid-cols-12'>

                                    <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_posted_posts_count ? data.ig_posted_posts_count : <></>)
                                                }         
                                            </div>
                                            <div className='text-[#A7A7B7] font-light'>
                                                NÚMERO DE PUBLICACIONES
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-12 md:col-span-6 lg:col-span-6 mt-2 md:mt-0'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ads_total_ig ? (data.ads_total_ig + ' Bs.') : <></>)
                                                }         
                                            </div>
                                            <div className='text-[#A7A7B7] font-light'>
                                                INVERSIÓN ADS
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div className='grid grid-cols-12 mt-10'>  

                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_reach ? data.ig_reach : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                PERSONAS ALCANZADAS
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_posts_impressions ? data.ig_posts_impressions : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                IMPRESIONES
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div  className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_reach_organic ? data.ig_reach_organic : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                PERSONAS ALCANZADAS
                                                (Orgánico)
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_posts_impressions_organic ? data.ig_posts_impressions_organic : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                IMPRESIONES(Orgánico)
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_reach_paid ? data.ig_reach_paid : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                PERSONAS ALCANZADAS
                                                (Pagado)
                                            </div>
                                        </div>
                                    </div> 
                                    <div className='col-span-4 md:col-span-2 lg:col-span-2'>
                                        <div className='text-center'>
                                            <div className="text-2xl md:text-4xl text-[#643DCE] font-semibold">
                                                {
                                                    loading ? <Skeleton /> : (data.ig_posts_impressions_paid ? data.ig_posts_impressions_paid : <></>)
                                                }         
                                            </div>
                                            <div className='text-xs md:text-base text-[#A7A7B7] font-light'>
                                                IMPRESIONES
                                                (Pagado)
                                            </div>
                                        </div>
                                    </div>

                                </div>   
                            </>
                            : <></>
                        }                    
                    </div>                   
                </div>

                {/* Instagram - Title */}
                <div className='text-lg font-base'>
                    <div className='flex flex-row items-center gap-2'>
                        <Image
                            className='rounded-full'
                            src={'/SocialMedia/Instagram.svg'}
                            alt='Instagram'
                            width={47}
                            height={47}
                        /> 
                        <span><span className='font-bold'>INSTAGRAM</span> PUBLICACIONES REALIZADAS</span>
                    </div>
                </div>

                {/* Instagram - Posts */}                
                <div className='my-20 grid grid-cols-4 gap-4'>  
                    {
                        loading ? <Spinner color="#582BE7" size={17} speed={1} animating={true} />
                        : 
                        posts.length > 0 ? 
                        
                            posts.filter(post => {
                                return post.social_network == 'Instagram';
                            }).map((post, index) => {

                                const thumbnail_url = post.thumbnail_url ? post.thumbnail_url : '/SocialMedia/no_img.jpg';

                                // const post_content = {
                                //     username: clientPlanData.client.name,
                                //     userImg : clientPlanData.client.img_url ? clientPlanData.client.img_url : null,
                                //     imageSrc: thumbnail_url,
                                //     postType : post.type,
                                //     caption: post.post_copy,
                                //     docId: post.id.toString(),
                                //     userLikedPhoto: false,
                                //     likes: [],
                                //     comments: [],
                                //     dateCreated: post.created,
                                // }
                                return (
                                    <div key={`instagram-${index}`} className='text-center'>
                                        <Image
                                            src={thumbnail_url}
                                            width={320}
                                            height={320}
                                            objectFit='contain'
                                        />
                                        <div className='flex flex-row justify-center items-center gap-2 mt-1'>
                                            {
                                                post.repost &&
                                                <div className='rounded-lg py-1 px-2 border border-[#582BE7] text-[#582BE7] text-xs font-light'>REPOST</div>
                                            }
                                            <Image
                                                src={getMediaUrlByType(post.type)}
                                                width={37}
                                                height={37}
                                                objectFit='contain'
                                                className='mt-2'
                                            />
                                        </div>
                                    </div>
                                )
                            })

                            : <></>
                    }  
                </div>

                {/* Instagram - Title */}
                <div className='text-lg font-base'>
                    <div className='flex flex-row items-center gap-2'>
                        <Image
                            className='rounded-full'
                            src={'/SocialMedia/Instagram.svg'}
                            alt='Instagram'
                            width={47}
                            height={47}
                        /> 
                        <span><span className='font-bold'>INSTAGRAM</span> DETALLE DE PUBLICACIONES</span>
                    </div>
                </div>

                {/* Instagram - Table */}
                <div className='my-20 grid grid-cols-12'>

                    <div className='col-span-12'>

                        <div className='hidden md:block'>
                            <table className="table-auto w-full border-separate border-spacing-y-7">
                                <thead className='text-gray-500 '>
                                    <tr>
                                        <th>Post</th>
                                        <th>Fecha</th>
                                        <th>Alcance</th>
                                        <th>Impresiones</th>
                                        <th>Reacciones</th>
                                        <th>Comentarios</th>
                                        <th>Compartidos</th>
                                        <th>Guardados</th>
                                        <th>Ads</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    {
                                        loading ? <tr><td><Skeleton /> </td></tr>
                                        : 
                                        posts.length > 0 ? 
                                        
                                            posts.filter(post => {
                                                return post.social_network == 'Instagram';
                                            }).map((post, index) => {

                                                const thumbnail_url = post.thumbnail_url ? post.thumbnail_url : '/SocialMedia/no_img.jpg';

                                                var total_post_ads = 0;

                                                if(post.clients_plans_posts_ads){
                                                    post.clients_plans_posts_ads.map(post_ad => {
                                                        total_post_ads += parseFloat(post_ad.amount);
                                                    });
                                                }

                                                return (        
                                                    <tr key={`instagram-row-${index}`}>                                                
                                                        <td className='text-center'>
                                                            <div className='flex flex-row gap-2 justify-center'>
                                                                <Image
                                                                    src={getMediaUrlByType(post.type)}
                                                                    width={17}
                                                                    height={17}
                                                                    objectFit='contain'
                                                                />     
                                                                <Image
                                                                    src={thumbnail_url}
                                                                    width={57}
                                                                    height={57}
                                                                    objectFit='contain'
                                                                />
                                                            </div>
                                                        </td>
                                                        <td className='text-center'>
                                                            <div className='flex flex-col'>
                                                                {post.planned_datetime}
                                                                {
                                                                    post.repost &&
                                                                    <span className='rounded-lg mt-1 py-1 px-1 border border-[#582BE7] text-[#582BE7] text-xs font-light'>REPOST</span>
                                                                }
                                                            </div>
                                                        </td>
                                                        <td className='text-center'>
                                                            {
                                                                post.insights
                                                                ? 
                                                                <div className='text-center'>
                                                                    <div className='text-center text-xl font-bold'>{post.real_post_reach}</div>      
                                                                    <div className='flex flex-row gap-4 justify-center'>
                                                                        <div className='text-base'>{post.real_post_reach_organic}</div>
                                                                        <div className='text-base text-[#643DCE]'>{post.real_post_reach_paid}</div>
                                                                    </div>                                                      
                                                                </div> 
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                                
                                                        <td className='text-center'>
                                                            {
                                                                post.insights
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-xl font-bold'>{post.real_post_impressions}</div>      
                                                                    <div className='flex flex-row gap-4 justify-center'>
                                                                        <div className='text-base'>{post.real_post_impressions_organic}</div>
                                                                        <div className='text-base text-[#643DCE]'>{post.real_post_impressions_paid}</div>
                                                                    </div>                                                      
                                                                </div> 
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                               
                                                        <td className='text-center'>
                                                            {
                                                                post.insights
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.real_post_likes}</div>
                                                                    <div className='text-center'>❤️</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                         
                                                        <td className='text-center'>
                                                            {
                                                                post.insights
                                                                ?
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.real_post_comments}</div>
                                                                    <div className='text-center'>💬</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                         
                                                        <td className='text-center'>
                                                            {
                                                                post.insights
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.real_post_shares}</div>
                                                                    <div className='text-center'>📣</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                      
                                                        <td className='text-center'>
                                                            {
                                                                post.insights
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-bold'>{post.real_post_saves}</div>
                                                                    <div className='text-center'>🔖</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>                                      
                                                        <td className='text-center'>
                                                            {
                                                                post.clients_plans_posts_ads
                                                                ? 
                                                                <div>
                                                                    <div className='text-center text-2xl font-semibold'>{total_post_ads + ' Bs.'}</div>
                                                                    <div className='text-center'>💵</div>
                                                                </div>
                                                                : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })

                                            : <></>
                                    }

                                </tbody>
                            </table>
                        </div>

                        <div className='md:hidden'>

                            {
                                loading ? <tr><td><Skeleton /> </td></tr>
                                : 
                                posts.length > 0 ? 
                                
                                    posts.filter(post => {
                                        return post.social_network == 'Instagram';
                                    }).map((post, index) => {

                                        const thumbnail_url = post.thumbnail_url ? post.thumbnail_url : '/SocialMedia/no_img.jpg';

                                        var total_post_ads = 0;

                                        if(post.clients_plans_posts_ads){
                                            post.clients_plans_posts_ads.map(post_ad => {
                                                total_post_ads += parseFloat(post_ad.amount);
                                            });
                                        }

                                        return (     

                                            <div key={`instagram-row-${index}`}>

                                                <div className='bg-[#fff] shadow-md rounded-[20px] py-[20px] mb-[20px] mx-[27px]'>

                                                    <div className='px-[20px] flex flex-row gap-1 items-center justify-between'>
                                                        <div className='flex flex-row gap-1'>
                                                            <div className="w-[48px] h-[48px] rounded-full">
                                                                {post.clients_plan.client.img_url != null && post.clients_plan.client.img_url.length > 0 ?
                                                                
                                                                    <Image 
                                                                        className='rounded-full'
                                                                        src={post.clients_plan.client.img_url}
                                                                        alt='media'
                                                                        layout='fixed'
                                                                        height={48}
                                                                        width={48}
                                                                    /> 
                                                                    : <></>  
                                                                }    
                                                            </div>
                                                            <div className="pl-[10px]">        
                                                                {post.clients_plan.client.name !== null ?
                                                                    <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal '>
                                                                    {post.clients_plan.client.name}
                                                                    </p>
                                                                    : <></>
                                                                }
                                                                <p className='text-[12px]'>
                                                                    {moment(post.planned_datetime).format("ddd")} {moment(post.planned_datetime).format("DD")} {moment(post.planned_datetime).format("MMM")}, {moment(post.planned_datetime).format("YYYY")}
                                                                </p>    
                                                            </div>   
                                                        </div>
                                                        <div>
                                                            <Image
                                                                src={getMediaUrlByType(post.type)}
                                                                width={27}
                                                                height={27}
                                                                objectFit='contain'
                                                            />    
                                                            {
                                                                post.repost &&
                                                                <span className='rounded-lg mt-1 py-1 px-1 border border-[#582BE7] text-[#582BE7] text-xs font-light'>REPOST</span>
                                                            }
                                                        </div> 
                                                    </div>
                                                    
                                                    <div className='mt-[15px] mb-[7px]'>
                                                        <Image
                                                            src={thumbnail_url}
                                                            width={570}
                                                            height={570}
                                                            layout='responsive'
                                                            // objectFit='contain'
                                                        />
                                                    </div>

                                                    <div className='px-[20px]'>
                                                        {post.post_copy !== null && post.post_copy !== undefined ?
                                                            <><p className='mt-[7px] text-[12px] whitespace-normal line-clamp-2'>{post.post_copy}</p><hr className='my-2'></hr></>
                                                            : <></>
                                                        }
                                                    </div>

                                                    <div className='px-[20px]'>

                                                        <div className='flex flex-row items-center justify-between mb-2'>

                                                            <div>
                                                                <h4 className='font-bold'>Alcance</h4>
                                                                <div className='flex flex-row gap-1 justify-center'>
                                                                    <div className='text-xs'>Orgánico</div>
                                                                    <div className='text-xs text-[#643DCE]'>Pagado</div>
                                                                </div> 
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.insights
                                                                    ? 
                                                                    <div className='text-center'>
                                                                        <div className='text-center text-xl font-bold'>{post.real_post_reach}</div>      
                                                                        <div className='flex flex-row gap-4 justify-center'>
                                                                            <div className='text-base'>{post.real_post_reach_organic}</div>
                                                                            <div className='text-base text-[#643DCE]'>{post.real_post_reach_paid}</div>
                                                                        </div>                                                      
                                                                    </div> 
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div>    
                                                            
                                                        </div>

                                                        <div className='flex flex-row items-center justify-between mb-2'>

                                                            <div>
                                                                <h4 className='font-bold'>Impresiones</h4>
                                                                <div className='flex flex-row gap-1 justify-center'>
                                                                    <div className='text-xs'>Orgánico</div>
                                                                    <div className='text-xs text-[#643DCE]'>Pagado</div>
                                                                </div> 
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.insights
                                                                    ? 
                                                                    <div>
                                                                        <div className='text-center text-xl font-bold'>{post.real_post_impressions}</div>      
                                                                        <div className='flex flex-row gap-4 justify-center'>
                                                                            <div className='text-base'>{post.real_post_impressions_organic}</div>
                                                                            <div className='text-base text-[#643DCE]'>{post.real_post_impressions_paid}</div>
                                                                        </div>                                                      
                                                                    </div> 
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div>
                                                            
                                                        </div>

                                                        <div className='flex flex-row items-center justify-between mb-3'>

                                                            <div>
                                                                <h4 className='font-bold'>Reacciones</h4>
                                                                <div className='text-xs'>❤️</div>
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.insights
                                                                    ? 
                                                                    <div>
                                                                        <div className='text-center text-2xl font-bold'>{post.real_post_likes}</div>
                                                                    </div>
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div> 
                                                            
                                                        </div>

                                                        <div className='flex flex-row items-center justify-between mb-2'>

                                                            <div>
                                                                <h4 className='font-bold'>💬 Comentarios</h4>
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.insights
                                                                    ?
                                                                    <div>
                                                                        <div className='text-center text-2xl font-bold'>{post.real_post_comments}</div>
                                                                    </div>
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div>    
                                                            
                                                        </div>

                                                        <div className='flex flex-row items-center justify-between mb-2'>

                                                            <div>
                                                                <h4 className='font-bold'>📣 Compartidos</h4>
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.insights
                                                                    ? 
                                                                    <div>
                                                                        <div className='text-center text-2xl font-bold'>{post.real_post_shares}</div>
                                                                    </div>
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div>   
                                                            
                                                        </div>

                                                        <div className='flex flex-row items-center justify-between mb-2'>

                                                            <div>
                                                                <h4 className='font-bold'>🔖 Guardados</h4>
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.insights
                                                                    ? 
                                                                    <div>
                                                                        <div className='text-center text-2xl font-bold'>{post.real_post_saves}</div>
                                                                    </div>
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div>   
                                                            
                                                        </div>

                                                        <div className='flex flex-row items-center justify-between'>

                                                            <div>
                                                                <h4 className='font-bold'>💵 Ads</h4>
                                                            </div>

                                                            <div className='text-center'>
                                                                {
                                                                    post.clients_plans_posts_ads
                                                                    ? 
                                                                    <div>
                                                                        <div className='text-center text-2xl font-semibold'>{total_post_ads + ' Bs.'}</div>
                                                                    </div>
                                                                    : <div className='text-xs text-center text-[#D7D7D7]'>No data available</div>
                                                                }
                                                            </div>
                                                            
                                                        </div>

                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                        )
                                    })

                                    : <></>
                            }

                        </div>

                    </div>

                </div>

            </div>

        </div>
    )
}
// ClientPlanReport.auth = true;
// ClientPlanReport.getLayout = function getLayout(page){
//     return (
//       <Layout>{page}</Layout>
//     )
//   }