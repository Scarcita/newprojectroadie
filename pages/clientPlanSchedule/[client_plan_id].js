import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import Layout from '../../src/components/shared/layout';
import Image from 'next/image'

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../public/Dashhoard/youtube.svg'

import TableAds from '../../src/components/clientPlan/TableAds'
import ModalPost from "../../src/components/clientPlan/ModalPost"
import ModalAd from "../../src/components/clientPlan/ModalAdFormer"
import CounterClient from '../../src/components/clientPlan/counterClient';
import { Spinner } from 'react-activity';
import CounterDirectValue from '../../src/components/clientPlan/counterDirectValue';
import ModalGenerateAccount from '../../src/components/clientPlan/ModalGenerateAccount';
import { Tab, Tabs } from '../../src/components/shared/Tabs';
import TableExtras from '../../src/components/clientPlan/TableExtras';
import ModalExtra from '../../src/components/clientPlan/ModalExtra';
import moment from 'moment';
import TablePosts from '../../src/components/clientPlan/TablePosts';


export default function ClientPlanSchedule() {

    const [reloadPosts, setReloadPosts] = useState(false);
    const [reloadAds, setReloadAds] = useState(false);
    const [loading, setLoading] = useState(true);

    const [data, setData] = useState([ ]);

    const [clientPlanData, setClientPlanData] = useState(null);
    
    const [service_total, setServiceTotal] = useState(0);
    const [ads_total, setAdsTotal] = useState(0);
    const [total, setTotal] = useState(0);

    const [posts, setPosts] = useState([]);
    const [loadingPosts, setLoadingPosts] = useState(true); 
    const [ads, setAds] = useState([]);
    const [loadingAds, setLoadingAds] = useState(true);
    const [extras, setExtras] = useState([]);
    const [loadingExtras, setLoadingExtras] = useState(true);

    const [ads_fb, setAdsFb] = useState(0);
    const [ads_ig, setAdsIg] = useState(0);
    const [ads_tw, setAdsTw] = useState(0);
    const [ads_yt, setAdsYt] = useState(0);
    const [count_fb, setCountFb] = useState(0);
    const [count_fb_posted, setCountFbPosted] = useState(0);
    const [count_ig, setCountIg] = useState(0);
    const [count_ig_posted, setCountIgPosted] = useState(0);
    const [count_tk, setCountTk] = useState(0);
    const [count_tk_posted, setCountTkPosted] = useState(0);
    const [count_mailing, setCountMailing] = useState(0);
    const [count_mailing_posted, setCountMailingPosted] = useState(0);
    const [count_li, setCountLi] = useState(0);
    const [count_li_posted, setCountLiPosted] = useState(0);
    const [count_tw, setCountTw] = useState(0);
    const [count_tw_posted, setCountTwPosted] = useState(0);
    const [count_yt, setCountYt] = useState(0);
    const [count_yt_posted, setCountYtPosted] = useState(0);


    const router = useRouter();
    const { client_plan_id } = router.query;

    const loadClientPlanData = () => {
        setLoading(true);
        // setLoadingPosts(true);
        // setLoadingAds(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/get/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            
            if (data.status) {
                setClientPlanData(data.data);

                // setPosts(data.data.clients_plans_posts);
                
                // var tempAds = [];
                // data.data.clients_plans_posts.map((post) => {
                //     if(post.clients_plans_posts_ads.length > 0){
                //         post.clients_plans_posts_ads.map((ad) => {
                //             ad._matchingData.ClientsPlansPosts = post;
                //             tempAds.push(ad);
                //             alert(JSON.stringify(ad));
                //         });
                //     }
                // });

                // setAds(tempAds);
                
            } else {
                console.error(data.errors);
            }
            setLoading(false);
            // setLoadingPosts(false);
            // setLoadingAds(false);
        })
        .catch((e) => {
            console.error(e);
            setLoading(false);
            // setLoadingPosts(false);
            // setLoadingAds(false);
        });
    }

    const countPostsByPlatform = (platform) => {
        var filteredArr = posts.filter((post) => { return post.social_network == platform });
        var postedFilteredArr = posts.filter((post) => { return post.social_network == platform && post.status == 'POSTED' });
        
        switch (platform) {
            case 'Facebook':
                setCountFb(filteredArr.length);
                setCountFbPosted(postedFilteredArr.length);

                // if(clientPlanData != null){
                //     if('plan' in clientPlanData ){
                //         var unit_price = clientPlanData.plan.price / clientPlanData.plan.facebook_posts_qty;
                //         setServiceTotal(unit_price * filteredArr.length);
                //     }
                // }

                break;
            case 'Instagram':
                setCountIg(filteredArr.length);
                setCountIgPosted(postedFilteredArr.length);
                break;
            case 'Twitter':
                setCountTw(filteredArr.length);
                setCountTwPosted(postedFilteredArr.length);
                break;
            case 'Youtube':
                setCountYt(filteredArr.length);
                setCountYtPosted(postedFilteredArr.length);
                break;
            case 'Tiktok':
                setCountTk(filteredArr.length);
                setCountTkPosted(postedFilteredArr.length);
                break;
            case 'Mailing':
                setCountMailing(filteredArr.length);
                setCountMailingPosted(postedFilteredArr.length);
                break;
            case 'Linkedin':
                setCountLi(filteredArr.length);
                setCountLiPosted(postedFilteredArr.length);
                break;
            default:
                break;
        }
    }

    const sumAdsByPlatform = (platform) => {
        var filteredArr = ads.filter((ad) => { return ad._matchingData.ClientsPlansPosts.social_network == platform });
        var sum = 0;
        filteredArr.map((ad) => { sum += parseFloat(ad.amount) } );
        
        switch (platform) {
            case 'Facebook':
                setAdsFb(sum);
                break;
            case 'Instagram':
                setAdsIg(sum);
                break;
            case 'Twitter':
                setAdsTw(sum);
                break;
            case 'Youtube':
                setAdsYt(sum);
                break;
            // case 'Tiktok':
            //     setCountTk(sum);
            //     break;
            // case 'Linkedin':
            //     setCountLi(sum);
            //     break;
            default:
                break;
        }
    }

    const calculateCurrentServicePrice = () => {

        //!!Youtube and Mailing will be calculated separately!! Because they are add on services
        // const social_networks = ['Facebook', 'Instagram', 'Twitter', 'Youtube', 'Tiktok', 'Mailing', 'LinkedIn'];
        const social_networks = ['Facebook', 'Instagram', 'Twitter', 'Tiktok'];

        var service_total = 0.0;

        var total_plan_posts = clientPlanData.plan.facebook_posts_qty + 
                                clientPlanData.plan.instagram_posts_qty +
                                clientPlanData.plan.twitter_posts_qty + 
                                clientPlanData.plan.tiktok_posts_qty;
        
        social_networks.forEach((platform) => {           

            var expectedPosts = 0;

            switch (platform) {
                case 'Facebook':
                    expectedPosts = clientPlanData.plan.facebook_posts_qty;
                    break;
                case 'Instagram':
                    expectedPosts = clientPlanData.plan.instagram_posts_qty;
                    break;
                case 'Twitter':
                    expectedPosts = clientPlanData.plan.twitter_posts_qty;
                    break;
                case 'Tiktok':
                    expectedPosts = clientPlanData.plan.tiktok_posts_qty;
                    break;
                default:
                    break;
            }
            
            if(expectedPosts > 0){
                const postedFilteredArr = posts.filter((post) => { return post.social_network == platform && post.status == 'POSTED' });
                const postedPosts = postedFilteredArr.length;

                const assigned_budget_percentage_for_platform = (expectedPosts * 100)/total_plan_posts;

                const unit_price = ((clientPlanData.plan.price*assigned_budget_percentage_for_platform) / 100) / expectedPosts;
                const platform_total = unit_price*postedPosts;
                service_total += platform_total;
            }

        });

        if(clientPlanData.plan.mailing_posts_qty){
            const postedFilteredArr = posts.filter((post) => { return post.social_network == 'Mailing' && post.status == 'POSTED' });
            const sentMails = postedFilteredArr.length;

            service_total += ((150/4) * sentMails);
        }

        if(clientPlanData.plan.youtube_posts_qty){
            const postedFilteredArr = posts.filter((post) => { return post.social_network == 'Youtube' && post.status == 'POSTED' });
            const postedPosts = postedFilteredArr.length;

            service_total += ((350/4) * postedPosts);
        }        
        
        setServiceTotal(service_total);
    }
    
    const loadClientPlanPosts = () => {
        setLoadingPosts(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setPosts(data.data);              
            } else {
                console.error(data.errors);
            }
            setLoadingPosts(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingPosts(false);
        });
    }

    const loadClientPlanAds = () => {
        setLoadingAds(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPostsAds/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setAds(data.data);              
            } else {
                console.error(data.errors);
            }
            setLoadingAds(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingAds(false);
        });
    }

    const loadClientPlanExtras = () => {
        setLoadingExtras(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansExtras/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setExtras(data.data);              
            } else {
                console.error(data.errors);
            }
            setLoadingExtras(false);
        })
        .catch((e) => {
            console.error(e);
            setLoadingExtras(false);
        });
    }


    useEffect(() => {

        if(router.isReady){
            loadClientPlanData();
        }
        
    }, [router]);
    

    useEffect(() => {
        if(clientPlanData != null){
            loadClientPlanPosts();
            loadClientPlanAds();
            loadClientPlanExtras();
        }       
    }, [clientPlanData]);

    useEffect(() => {
        if(posts.length > 0){
            countPostsByPlatform('Facebook');  
            countPostsByPlatform('Instagram');
            countPostsByPlatform('Twitter');
            countPostsByPlatform('Youtube');
            countPostsByPlatform('Tiktok');
            countPostsByPlatform('Mailing');
            countPostsByPlatform('Linkedin');
            
            // if(clientPlanData != null){ Already checked on previous useEffect, when loading posts
                if('plan' in clientPlanData ){
                    calculateCurrentServicePrice();
                }
            // }
        }       
    }, [posts]);

    useEffect(() => {
        if(ads.length > 0){
            sumAdsByPlatform('Facebook');  
            sumAdsByPlatform('Instagram');
            sumAdsByPlatform('Twitter');
            sumAdsByPlatform('Youtube');
            // countPostsByPlatform('Tiktok');
            // countPostsByPlatform('Mailing');
            // countPostsByPlatform('Linkedin'); 
            var sum = 0;
            ads.map((ad) => { sum += parseFloat(ad.amount) } );
            setAdsTotal(sum);
        }
    }, [ads]);

    useEffect(() => {
        if(clientPlanData != null){
            //We will give included ads only if at least facebook posts qty are reached/posted as expected, otherwise charge full ads spent
            if('plan' in clientPlanData && clientPlanData.plan.included_ads_qty != 0 && count_fb_posted >= clientPlanData.plan.facebook_posts_qty) {
                setTotal((service_total + ads_total) - parseFloat(clientPlanData.plan.included_ads_qty));
            } else {
                setTotal(service_total + ads_total);
            }
        } else {
            setTotal(service_total + ads_total);
        }
    }, [service_total, ads_total]);

    return (
        <div className=''>

            {/* WANT Header */}
            <div className='grid grid-cols-12 px-7 bg-[#F3F3F3] '>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 py-4'>
                    <div className='flex flex-row justify-between items-center'>
                        <div>
                            {/* <div className='text-2xl font-bold '>
                                WANT Digital Agency
                            </div> */}
                            <Image src={'/Want_Logo.png'} height={47} width={100} objectFit='contain' />                            
                        </div>
                        <div>
                            {
                                loading ?
                                    <Spinner color="#582BE7" size={17} speed={1} animating={true} />
                                :
                                clientPlanData && 'client' in clientPlanData ?
                                <div className='flex flex-row gap-2 items-center'>
                                    <div>
                                        <p className="text-base font-semibold text-right">{clientPlanData.client.name}</p>
                                        <p className="text-sm font-light text-right">{moment().format('llll')}</p>
                                    </div>
                                    <div>
                                        {
                                        clientPlanData.client.img_url !== undefined && clientPlanData.client.img_url !== null
                                            ? <Image
                                                className='rounded-full'
                                                src={clientPlanData.client.img_url}
                                                layout='fixed'
                                                alt='ImgPerfil'
                                                width={47}
                                                height={47}
                                                /> 
                                            : 
                                            <div style={{width: 110, height: 110, background: '#F3F3F3', borderRadius: '50%'}}></div>
                                        }
                                    </div>
                                </div>
                                : <></>
                            }                    
                        </div>  
                    </div>
                </div>                              
            </div>


            <div className='w-full container mx-auto'>

                <div className="mt-[20px] grid grid-cols-12 gap-3">
                    <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0062E0] to-[#19AFFF] rounded-[16px] justify-between p-[8px]'>
                        <div>
                        <div className="flex flex-row">
                            {
                                loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_fb_posted}/{count_fb}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.facebook_posts_qty ? ('/' + clientPlanData.plan.facebook_posts_qty) : ''}</div>
                            }                       
                        </div>
                        <div className="text-[10px] text-[#FFFFFF]">POST</div>
                        </div>
                        <div className='pt-[10px]'>
                            <Image
                                src={ImgFacebook}
                                layout='fixed'
                                alt='ImgFacebook'
                                width={36}
                                height={36}
                                
                            />                    
                        </div>
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FCA759] via-[#E82D56] via-[#A22DB4] to-[#643DCE] rounded-[16px] justify-between pl-[10px] p-[8px]'>
                        <div>
                        <div className="flex flex-row">
                            {
                                loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_ig_posted}/{count_ig}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.instagram_posts_qty ? ('/' + clientPlanData.plan.instagram_posts_qty) : ''}</div>
                            }                            
                        </div>
                        <div className="text-[10px] text-[#FFFFFF]">POST</div>
                        </div>
                        <div className='pt-[10px]'>
                            <Image
                                src={ImgIg}
                                layout='fixed'
                                alt='ImgIg'
                                width={36}
                                height={36}
                                
                            />                    
                        </div>
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#161616] rounded-[16px] justify-between p-[8px]'>
                        <div>
                        <div className="flex flex-row">
                            {
                                loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_tk_posted}/{count_tk}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.tiktok_posts_qty ? ('/' + clientPlanData.plan.tiktok_posts_qty) : ''}</div>
                            }
                        </div>
                        <div className="text-[10px] text-[#FFFFFF]">POST</div>
                        </div>
                        <div className='pt-[10px]'>
                            <Image
                                src={ImgTiktok}
                                layout='fixed'
                                alt='ImgTiktok'
                                width={36}
                                height={36}
                                
                            />                    
                        </div>
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0CA8F6] to-[#0096E1] rounded-[16px] justify-between p-[8px]'>
                        <div>
                        <div className="flex flex-row">
                            {
                                loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_tw_posted}/{count_tw}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.twitter_posts_qty ? ('/' + clientPlanData.plan.twitter_posts_qty) : ''}</div>
                            }  
                        </div>
                        <div className="text-[10px] text-[#FFFFFF]">POST</div>
                        </div>
                        <div className='pt-[10px]'>
                            <Image
                                src={ImgTwitter}
                                layout='fixed'
                                alt='ImgTelegram'
                                width={36}
                                height={36}
                                
                            />                    
                        </div>
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#DB0505] to-[#FF0000] rounded-[16px] justify-between p-[8px]'>
                        <div>
                        <div className="flex flex-row">
                            {
                                loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_yt_posted}/{count_yt}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.youtube_posts_qty ? ('/' + clientPlanData.plan.youtube_posts_qty) : ''}</div>
                            }
                        </div>
                        <div className="text-[10px] text-[#FFFFFF]">POST</div>
                        </div>
                        <div className='pt-[10px]'>
                            <Image
                                src={ImgYoutube}
                                layout='fixed'
                                alt='ImgYoutube'
                                width={36}
                                height={36}
                                
                            />
                        </div>
                    </div>
                    <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FBBC04] to-[#FCD462] rounded-[16px] justify-between p-[8px]'>
                        <div>
                        <div className="flex flex-row">
                            {
                                loadingPosts ? <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                : <div className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{count_mailing_posted}/{count_mailing}{clientPlanData != null && 'plan' in clientPlanData && clientPlanData.plan.mailing_posts_qty ? ('/' + clientPlanData.plan.mailing_posts_qty) : ''}</div>
                            }
                        </div>
                        <div className="text-[10px] text-[#FFFFFF]">POST</div>
                        </div>
                        <div className='pt-[10px]'>
                            <Image
                                src={ImgGmail}
                                layout='fixed'
                                alt='ImgGmail'
                                width={36}
                                height={36}
                                
                            />                    
                        </div>
                    </div>
                </div>
                

                <div className='w-full mt-10 mb-4'>
                    <div className="pb-4">
                        <TablePosts data={posts} loading={loadingPosts} loadClientPlanData={loadClientPlanData} />    
                    </div>
                </div>

            </div>
        </div>
    )
}