import React, { useEffect, useState } from 'react';
import Layout from "../../src/components/shared/layout";
import TableDatosClients from '../../src/components/clients/tabladatos';
import ModalClient from "../../src/components/clients/modalClient"
import CounterClientsAll from '../../src/components/clients/counterClientsAll';
import CounterPlansActive from '../../src/components/clients/counterPlanActive';
import CounterPlansMonth from '../../src/components/clients/counterPlansMonth';
import BarChartClients from '../../src/components/clients/DualDatasetBarChart';
import DualDatasetBarChart from '../../src/components/clients/DualDatasetBarChart';
import CounterSingle from '../../src/components/dashboard/CounterSingle';

export default function Clients() {

  const [reloadClients, setReloadClients] = useState(false);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const getDashboard = async () => {
    setLoading(true);
    try {
      const response = await fetch(
        process.env.NEXT_PUBLIC_SERVER_URL + "/clients/dashboard",
        {
          method: "GET",
        }
      );
      const res = await response.json();
      console.log("response", res);
      if(res.status){
        setData(res.data);
      }
    } catch (error) {
      console.log("🚀 SettigsTab ~ getSettings ~ error:", error)
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getDashboard();
  }, []);


  return (


    <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Clients</div>
                  <div className='flex flex-row '>
                    <ModalClient
                     reloadClients={reloadClients} 
                     setReloadClients={setReloadClients}
                     />
                  </div> 
              </div>
          </div >

          <div className='grid grid-cols-12 mt-[20px] gap-6'>

              <div className='col-span-12 md:col-span-12 lg:col-span-6'>

                <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Status</div>

                  <div className='grid grid-cols-12  gap-3 mt-[20px]'> 

                    <div className='col-span-12 md:col-span-4 lg:col-span-4 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] text-center" >
                        <CounterSingle loading={loading} value={data.length == 0 ? 0 : data.all_clients_count } />
                      </div>
                      <div className="text-[12px] font-medium text-[#000000] text-center">Total</div>

                    </div>

                    <div className='col-span-12 md:col-span-4 lg:col-span-4 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] " >  
                        <CounterSingle loading={loading} value={data.length == 0 ? 0 : data.active_clients_count } />
                      </div>
                      <div className="text-[12px] font-medium text-[#000000] ">Active (With Plan)</div>
                      
                    </div>
                    <div className='col-span-12 md:col-span-4 lg:col-span-4 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] " >                        
                        <CounterSingle loading={loading} value={data.length == 0 ? 0 : data.active_clients_count } />
                      </div>
                      <div className="text-[12px] font-medium text-[#000000] ">New this month</div>
                      
                    </div>
                  
                  </div >
                  
              </div>

              <div className='col-span-12 md:col-span-12 lg:col-span-6'>

              <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>This month client overview by plan</div>

                <div className='grid grid-cols-12 mt-[20px]'>

                  <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                    <DualDatasetBarChart loading={loading} data={data} title1={'Ads Spend (Bs.)'} title2={'Planned Profit (Bs.)'} />
                  </div>
                  
                </div >
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px] ml-[20px] mr-[20px]'>

              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                <TableDatosClients 
                  reloadClients={reloadClients} 
                  setReloadClients={setReloadClients}
                />
  
              </div>
              
          </div >
        </div>
      </div>


  )
}
Clients.auth = true
Clients.getLayout = function getLayout(page) {
  return (
    <Layout>{page}</Layout>
  )
}
