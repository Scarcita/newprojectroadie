import React, { useEffect, useState } from 'react';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import LatestPost from '../../src/components/dashboard/latestPost';
import BarChart from '../../src/components/shared/BarChart';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import CounterPostsStatus from '../../src/components/dashboard/CounterPostsStatus';
import CounterSingle from '../../src/components/dashboard/CounterSingle';

 /////// IMAGENES ////////////////////
import ImgFacebook from '../../public/Dashhoard/facebook.svg';
import ImgGmail from '../../public/Dashhoard/gmail.svg';
import ImgIg from '../../public/Dashhoard/instagram.svg';
import ImgMessenger from '../../public/Dashhoard/messenger.svg';
import ImgTelegram from '../../public/Dashhoard/telegram.svg';
import ImgTiktok from '../../public/Dashhoard/tikTok.svg';
import ImgTwitter from '../../public/Dashhoard/twitter.svg';
import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg';
import ImgYoutube from '../../public/Dashhoard/youtube.svg';

export default function Dashboard() {

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const getDashboard = async () => {
    setLoading(true);
    try {
      const response = await fetch(
        process.env.NEXT_PUBLIC_SERVER_URL + "/users/dashboard",
        {
          method: "GET",
        }
      );
      const res = await response.json();
      console.log("response", res);
      if(res.status){
        setData(res.data);
      }
      // res.data.map((item: any) => {
      //   if (item.name === "company_name") {
      //     setCompanyName(item.value);
      //   }
      //   if (item.name === "company_address") {
      //     setAdress(item.value);
      //   }
      //   if (item.name === "company_phone") {
      //     setPhone(item.value);
      //   }
      //   if (item.name === "company_email") {
      //     setEmail(item.value);
      //   }
      //   if (item.name === "company_license_number") {
      //     setLicenseNumber(item.value);
      //   }
      //   if (item.name === "company_zip") {
      //     setZipNumber(item.value);
      //   }
      //   if (item.name === "company_sales_tax") {
      //     setSalesTax(item.value);
      //   }
      //   if (item.name === "company_order_message") {
      //     setCompanyOrderMessage(item.value);
      //   }
      //   if (item.name === "company_appointment_message") {
      //     setCompanyAppointmentMessage(item.value);
      //   }
      //   if (item.name === "company_quotation_message") {
      //     setCompanyQuotationMessage(item.value);
      //   }
      //   if (item.name === "company_logo") {
      //     setSelectedImageURL(item.value);
      //   }
      // });
    } catch (error) {
      console.log("🚀 SettigsTab ~ getSettings ~ error:", error)
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getDashboard();
  }, []);

  return (
    <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-8 ml-[30px] mr-[30px] mt-[45px]'>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>
                    Dashboard
                  </div> 
              </div>
              
          </div >
          <div className="mt-[24px] text-[18px] font-semibold">
            {loading ? <Skeleton /> : 'Status'}
          </div>
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#0062E0] to-[#19AFFF] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[42px] font-semibold text-[#FFFFFF]">
                      <CounterPostsStatus loading={loading} plannedValue={data.length == 0 ? 0 : data.fb_planned_posts_count }  postedValue={data.length == 0 ? 0 : data.fb_posted_posts_count } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgFacebook}
                    layout='fixed'
                    alt='ImgFacebook'
                    width={48}
                    height={48}                    
                  />                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#FCA759] via-[#E82D56] via-[#A22DB4] to-[#643DCE] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[42px] font-semibold text-[#FFFFFF]">
                      <CounterPostsStatus loading={loading} plannedValue={data.length == 0 ? 0 : data.ig_planned_posts_count }  postedValue={data.length == 0 ? 0 : data.ig_posted_posts_count } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgIg}
                    layout='fixed'
                    alt='ImgIg'
                    width={48}
                    height={48}                    
                  />                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#161616] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[42px] font-semibold text-[#FFFFFF]">
                      <CounterPostsStatus loading={loading} plannedValue={data.length == 0 ? 0 : data.tk_planned_posts_count }  postedValue={data.length == 0 ? 0 : data.tk_posted_posts_count } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgTiktok}
                    layout='fixed'
                    alt='ImgTiktok'
                    width={48}
                    height={48}                    
                  />                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#0CA8F6] to-[#0096E1] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[42px] font-semibold text-[#FFFFFF]">
                      <CounterPostsStatus loading={loading} plannedValue={data.length == 0 ? 0 : data.x_planned_posts_count }  postedValue={data.length == 0 ? 0 : data.x_posted_posts_count } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgTwitter}
                    layout='fixed'
                    alt='ImgTwitter'
                    width={48}
                    height={48}                    
                  />                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#DB0505] to-[#FF0000] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[42px] font-semibold text-[#FFFFFF]">
                      <CounterPostsStatus loading={loading} plannedValue={data.length == 0 ? 0 : data.yt_planned_posts_count }  postedValue={data.length == 0 ? 0 : data.yt_posted_posts_count } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgYoutube}
                    layout='fixed'
                    alt='ImgYoutube'
                    width={48}
                    height={48}                    
                  />                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#FBBC04] to-[#FCD462] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[42px] font-semibold text-[#FFFFFF]">
                      <CounterPostsStatus loading={loading} plannedValue={data.length == 0 ? 0 : data.mailing_planned_posts_count }  postedValue={data.length == 0 ? 0 : data.mailing_posted_posts_count } />
                    </div>                           
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgGmail}
                    layout='fixed'
                    alt='ImgGmail'
                    width={48}
                    height={48}                    
                  />                
                </div>
            </div>
          </div>

          <div className="mt-[24px] text-[18px] font-semibold">
          Accounts status
          </div>
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[32px] font-semibold text-[#000000]">
                      <CounterSingle loading={loading} value={data.length == 0 ? 0 : data.active_plans_count } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#000000]">ACTIVE PLANS</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[32px] font-semibold text-[#000000]">
                      <CounterSingle loading={loading} value={data.length == 0 ? 0 : data.plans_service_total.toLocaleString(undefined, {maximumFractionDigits:2}) } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#000000]">PLANS TOTAL</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-12 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <div className="text-[32px] font-semibold text-[#000000]">
                      <CounterSingle loading={loading} value={data.length == 0 ? 0 : data.ads_total.toLocaleString(undefined, {maximumFractionDigits:2}) } />
                    </div>                          
                  </div>
                  <div className="text-[10px] text-[#000000]">ADS TOTAL</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                
                </div>
            </div>
          </div>

          <div className="mt-[24px] text-[18px] font-semibold">
            Ads overview
          </div>

          <div className='grid grid-cols-12 mt-0'>
            {/* <div className="col-span-12 md:col-span-12 lg:col-span-12 h-[200px] md:h-[330px] lg:h-[188px] bg-[#FFFFFF] rounded-[24px] pt-[10px] pr-[5px]"> */}
            <div className="col-span-12 shadow-md rounded-md mb-7 md:mb-0">
               <BarChart loading={loading} data={data} title={'Ads Spend (Bs.)'} />
            </div>
          </div> 

        </div>

        <div className='col-span-12 md:col-span-12 lg:col-span-4 bg-[#F7F7F7] p-5'>
            <div>
              <LatestPost loading={loading} data={data.latest_posted_post} title={'Latest post'}/>                  
            </div>
            <div>
              <LatestPost loading={loading} data={data.next_scheduled_post} title={'Next scheduled post'}/> 
            </div>
        </div>
      
      </div>
  )
}

Dashboard.auth = true