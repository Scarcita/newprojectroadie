// import React  from 'react';
// import { useEffect, useState } from 'react'
// import { Spinner} from 'react-activity';
// import "react-activity/dist/Spinner.css";
// import "react-activity/dist/Dots.css";
// import Image from 'next/image';

// import dynamic from 'next/dynamic';
// import { getImgUrlForSocialNetwork, getMediaUrlByType } from '../../src/utils';
// import moment from 'moment';
// import TooltipPost from '../../src/components/designerBoard/tooltipPost';

const TaskCard = ({ item, index, reloadFunction }) => {

  // const [showModal, setShowModal] = useState(false);
  // const [showIconClick, setShowIconClick] = useState(false);
  // const [selectedPost, setSelectedPost] = useState(null);

  // const onClickDay = () =>{
  //   setShowModal(true)
  //   setShowIconClick(true)
  // }

  // const Draggable = dynamic(
  //   () =>
  //     import('react-beautiful-dnd').then(mod => {
  //       return mod.Draggable;
  //     }),
  //   {ssr: false},
  // );

  // // Moment
  // if(item !== undefined){
  //   if (item !== null){
  //     if(item.planned_datetime !== null && item.planned_datetime !== undefined){
  //       var eventdate = item.planned_datetime;
  //       var splitdate = eventdate.split('-');
  //       //console.log(splitdate);
  //       var day = splitdate[2];
  //       var year = splitdate[0];
  //     }
  //   }
  // }

  // if(item !== undefined){
  //   if (item !== null){
  //     if(item.planned_datetime !== null && item.planned_datetime !== undefined){
  //       var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  //       var d = new Date(item.planned_datetime);
  //       var dayName = days[d.getDay()];
  //     }
  //   }
  // }

  // if(item !== undefined){
  //   if (item !== null){
  //     if(item.planned_datetime !== null && item.planned_datetime !== undefined){
  //       var months = ['January', 'February ', 'March ', 'April', 'May', 'June', 'July','August', 'September', 'Octuber', 'November', 'December'];
  //       var m = new Date(item.planned_datetime);
  //       var monthName = months[m.getMonth()];
  //     }
  //   }
  // }

  // return (
  //   <Draggable
  //     key={item.id}
  //     draggableId={item.id.toString()}
  //     index={index}
  //   >
  //     {(provided) => (

  //       <div
  //         ref={provided.innerRef}
  //         {...provided.draggableProps}
  //         {...provided.dragHandleProps}
  //         className='rounded-[10px] bg-[#fff] shadow-md mb-[15px] pl-[10px] pt-[10px] pb-[10px] pr-[10px]'
  //         >

  //           <div className='flex flex-row items-center justify-between p-2'>

  //             <div className='flex flex-row items-end gap-1'>
  //                 <div className='text-5xl text-[#582BE7] font-bold p-0 m-0 leading-none'>{moment(item.planned_datetime).format("DD")}</div>
  //                 <div>
  //                     <div className='text-xs font-bold uppercase p-0 m-0 leading-none'>{moment(item.planned_datetime).format("ddd")}</div>
  //                     <div className='text-xl font-bold uppercase p-0 m-0 leading-none '>{moment(item.planned_datetime).format("MMM")}</div>
  //                     <div className='text-xs text-gray-400'>{moment(item.planned_datetime).format("YYYY")}</div>
  //                 </div>
  //             </div>

  //             <div className='flex flex-item items-center gap-2'>
  //                 <Image
  //                     src={getMediaUrlByType(item.type)}
  //                     width={21}
  //                     height={21}
  //                     objectFit='cover'
  //                 />
  //                 <Image
  //                     src={getImgUrlForSocialNetwork(item.social_network)}
  //                     layout='fixed'
  //                     width={37}
  //                     height={37}
  //                 />
  //                 {
  //                   item.clients_plan && item.clients_plan.client ?                      
  //                   <Image
  //                       src={ item.clients_plan.client.img_url ? item.clients_plan.client.img_url : '/SocialMedia/no_img.jpg'}
  //                       layout='fixed'
  //                       width={37}
  //                       height={37}
  //                       className='rounded-full'
  //                   />
  //                   : <></>
  //                 }
  //                 {item.status === 'DONE' && <TooltipPost item={item} reloadFunction={reloadFunction} />}
  //             </div>

  //           </div>

  //           {
  //             item.status === 'DONE' ?
  //               <div style={{position: 'relative', height: 170}}>
  //                   <Image
  //                       src={ item.thumbnail_url ? item.thumbnail_url : '/SocialMedia/no_img.jpg'}
  //                       layout='fill'
  //                       objectFit='cover'
  //                   />
  //               </div>
  //               : <></>
  //           }

  //           <div className='px-2 py-2'>
  //               <div className='font-bold uppercase'>{item.clients_plan.client.name}</div>
  //               <div className='font-semibold uppercase'>{item.title}</div>
  //               <div className='text-sm font-light'>{item.subtitle}</div>
  //               {/* <div className='text-xs font-light text-[#A7A7B7] pt-1'>{item.post_copy}</div> */}
  //               <div className='text-sm font-light text-[#582BE7] pt-1'>Instrucciones</div>
  //               <div className='text-sm font-semibold text-[#582BE7] pt-1'>{item.instructions ? item.instructions : 'Ninguna'}</div>
  //           </div>

  //           {/* <div className='flex flex-row justify-between gap-3 mt-[10px]'>

  //             <div className=''>

  //               <div className='text-[12px] font-semibold leading-4 whitespace-normal'>
  //                   {item.title}
  //               </div>

  //               <div className='mt-[5px] mb-[5px]'>
  //                 <p className='text-[12px] text-[#000000] font-light leading-4 whitespace-normal'>{item.subtitle}</p>
  //               </div>
                
  //               <hr></hr>

  //               <div className='mt-[5px] mb-[5px]'>
  //                 <p className='text-[10px] font-light leading-4 whitespace-normal'>{item.post_copy}</p>
  //               </div>
                
  //               <div className='mt-[5px] mb-[5px]'>
  //                 <div className='font-semibold text-[14px] text-[#582BE7]'>Instructions: </div>
  //                 <p className='text-[12px] font-light leading-4 whitespace-normal'>{item.instructions}</p>
  //               </div>

  //             </div>

  //             <div className=''>
  //               {item.thumbnail_url !== null && item.thumbnail_url !== undefined ?
  //                   <div className=''>
  //                     <Image
  //                         className='rounded-md'
  //                         src={item.thumbnail_url}
  //                         alt='img'
  //                         layout='responsive'
  //                         width={100}
  //                         height={100}
  //                         >
  //                     </Image>
  //                   </div>
  //                 : <></>
  //               }
  //             </div>
              
  //           </div> */}
          
  //           {/* <TooltipEditItem 
  //             showModal={showModal} 
  //             setShowModal={setShowModal} 
  //             selectedPost={selectedPost}
  //             reloadFunction={reloadFunction}
  //           />
  //           <div className='grid grid-cols-12'>
  //             <div className='col-span-12 md:col-span-12 lg:col-span-12 justify-self-end mt-[10px]'>
  //               <button
  //                 onClick={() => {setSelectedPost(item), setShowModal(true)}}
  //                 className=''
  //                 >
  //                   <svg
  //                       xmlns="http://www.w3.org/2000/svg"
  //                       fill="none"
  //                       viewBox="0 0 24 24"
  //                       strokeWidth="1.5"
  //                       stroke="currentColor"
  //                       className="w-6 h-6 text-[#643DCE] "
  //                   >
  //                       <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
  //                   </svg>
  //                 </button>

  //             </div>
  //           </div> */}              
  //       </div>
  //     )}      
  //   </Draggable>
  // );

  return <></>
};

export default TaskCard;
