import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Spinner} from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import Kanban from '../../src/components/designerBoard/Kanban';
import { resetServerContext } from "react-beautiful-dnd";
import { v4 as uuidv4 } from 'uuid';
import 'moment/locale/es';


export default function Board() {
  
  const router = useRouter();
  const { designer_id } = router.query;

  const [columns, setColumns] = useState([]);
  const [loading, setLoading] = useState(true);

  const loadDesignerUnpostedPosts = async () => {
    setLoading(true);

    let todo_tasks = [];
    let in_progress_tasks = [];
    let done_tasks = [];

    try {

      const response = await fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/designerUnpostedPosts/' + designer_id);
      const res = await response.json();
      console.log("res", res);

      if (res.status) {
        
        res.data.map((item, index) => {
          switch (item.status) {
            case "PLANNED":
              todo_tasks.push(item);
              break;
            case "IN PROGRESS":
              in_progress_tasks.push(item);
              break;
            case "DONE":
              done_tasks.push(item);
              break;        
            default:
              break;
          }
        });

        setColumns(
            {[uuidv4()]: {
              id: 1,
              title: "PLANNED",
              items: todo_tasks,
            },
            [uuidv4()]: {
              id: 2,
              title: "IN PROGRESS",
              items: in_progress_tasks,
            },
            [uuidv4()]: {
              id:3,
              title: "DONE",
              items: done_tasks,
            }
          }
        );

      } else {
          console.error(res.errors);
      }

    } catch (error) {
        console.error("Error in loadDesignerUnpostedPosts", error);
    } finally {
        setLoading(false);
    }
  }
  
  useEffect(() => {
      if(router.isReady){
        loadDesignerUnpostedPosts();
      }        
  }, [router]);

  // useEffect(() => {

  // }, []);

  return (
      <div className='flex flex-col mx-7 my-7'>
        
        <div className='text-2xl md:text-4xl text-[#000000] font-bold'>Designer Tracking Board</div>

        <div className='self-center'>
          {
            loading ? <Spinner color="#582BE7" size={17} speed={1} animating={true} />
            : <Kanban columns={columns} setColumns={setColumns} reloadFunction={loadDesignerUnpostedPosts} loading={loading} setLoading={setLoading} />
          }
        </div>

      </div>
  )
}

Board.auth = true