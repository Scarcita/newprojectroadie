import React, { useEffect, useState } from 'react';
import Counter from '../../src/components/calendar/counterPostDay';
import Layout from "../../src/components/shared/layout";
import BarChartPlans from '../../src/components/plans/barChart';
import CounterActivePlans from '../../src/components/plans/counterActivePlans';
import CounterPlans from '../../src/components/plans/counterPlans';
import ModalPlans from '../../src/components/plans/modalPlans'
import TablePlans from '../../src/components/plans/tablePlans'



export default function Plans() {
  const [reloadPlansAll, setReloadPlansAll] = useState(false);

  return (
      <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>PLANS</div>
                  <div className='flex flex-row '>
                    <ModalPlans 
                      reloadPlansAll={reloadPlansAll} 
                      setReloadPlansAll={setReloadPlansAll}
                    />
                  </div>
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px] gap-6'>

              <div className='col-span-12 md:col-span-12 lg:col-span-5'>

                <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Status</div>

                  <div className='grid grid-cols-12  gap-6 mt-[20px]'> 

                    <div className='col-span-6 md:col-span-6 lg:col-span-6 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] text-center" >
                        <CounterPlans
                            reloadPlansAll={reloadPlansAll} 
                            setReloadPlansAll={setReloadPlansAll}
                        />
                      </div>
                      <div className="text-[12px] font-medium text-[#000000] text-center">Total</div>

                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-6 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] " >
                        <CounterActivePlans
                            reloadPlansAll={reloadPlansAll} 
                            setReloadPlansAll={setReloadPlansAll}
                        />
                      </div>
                      <div className="text-[12px] font-medium text-[#000000] ">Active (With Clients)</div>
                      
                    </div>
                  
                  </div >
                  
              </div>

              <div className='col-span-12 md:col-span-12 lg:col-span-7'>

              <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>This month client overview by plan</div>

                <div className='grid grid-cols-12 mt-[20px]'>
                  
                  
  
                  <div className='col-span-12 md:col-span-12 lg:col-span-12'>

                    <BarChartPlans/>
  
                  </div>
                  
                </div >
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px]'>

              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                <TablePlans 
                  reloadPlansAll={reloadPlansAll} 
                  setReloadPlansAll={setReloadPlansAll}
                />
  
              </div>
              
          </div >
        </div>
      </div>
  )
}
Plans.auth = true;