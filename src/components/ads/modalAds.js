import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import ReactSelect from 'react-select';
import Image from 'next/image';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


export default function ModalAds(props) {
    const {reloadAds, setReloadAds} = props
    const [isLoadingClients, setIsLoadingClients] = useState(true);
    const [isLoadingPost, setIsLoadingPost] = useState(true);
    const [selectedClient, setSelectedClient] = useState(null);
    const [selectedPlan, setSelectedPlan] = useState(null);
    const [isLoadingPlans, setIsLoadingPlans] = useState(true);
    const [plans, setPlans] = useState([]);
    const [showPlansSelect, setShowPlansSelect] = useState(false);
    const [showPostSelect, setShowPostSelect] = useState(false);
    const [showFormFields, setShowFormFields] = useState(false);
    const [clients, setClients] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [clientPlanPosts, setClientPlanPosts] = useState([])
    const [PlanPostId, setPlanPostId] = useState(null)
    const [clientPlanPostId, setClientPlanPostId] = useState(null)
    const [startDate, setStartDate] = useState('')
    const [startTime, setStartTime] = useState('')
    const [endDate, setEndDate] = useState('')
    const [endTime, setEndTime] = useState('')
    const [amount, setAmount] = useState('')

    const clearForm = () => {
        setClientPlanPostId('')
        setStartDate('')
        setEndDate('')
        setAmount('')
    }

    /////////////////////// SELECT NAME ///////////////////////

    const formatOptionLabelClient = ({ value, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>

                <div>
                    <small className='text-black text-xs'>{post.name}</small>
                </div>
            </div>
        )
    }; 

    useEffect(() => {

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clients/all')
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.name, post: result})
                })

                setClients(temp)
                //console.log('DATA CLIENTS: ', temp);
                
            } else {
                console.error(data.error)
            }
            setIsLoadingClients(false)
        })

    }, [])

    /////////////////////////////////SELECT PLAN///////////////////////////////////////

        const formatOptionLabelPlan = ({ row, label, post }) => {

            return (
                <div className='grid grid-cols-12 align-center'>

                    <div>
                        <small className='text-gray-500 text-xs'>{post.plan.name}</small>
                    </div>
                </div>
            )
        }; 

        useEffect(() => {
            if(selectedClient){
                setIsLoadingPlans(true)
                setShowPlansSelect(true)
                fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/all/' + selectedClient.value)
                .then(response => response.json())
                .then(data => {
                    if (data.status) {
        
                        var temp = [];
                        data.data.map((row) => {
                            temp.push({value: row.id, label: row.plan.name, post: row})
                        })
        
                        setPlans(temp)
                    } else {
                        console.error(data.error)
                    }
                    setIsLoadingPlans(false)
                })
            }
        }, [selectedClient])

    
///////////////////////// SELECT POST //////////////////////////////
    const formatOptionLabelPost = ({ value, label, post }) => {
       
        const imgUrl = "";

        if(post.social_network !== null){
            switch (post.social_network) {
                case 'Facebook': 
                    imgUrl = '/SocialMedia/Facebook.svg'
                    break;
                case 'TikTok':
                    imgUrl= '/SocialMedia/TikTok.svg'
                    break;

                case 'Instagram':
                    imgUrl = '/SocialMedia/Instagram.svg'
                break;
                case 'YouTube':
                    imgUrl = '/SocialMedia/Youtube.svg'
                break;
                case 'Mailing':
                    imgUrl = '/Plans/gmail.svg'
                break;
                case 'LinkedIn':
                    imgUrl = '/SocialMedia/messenger.svg'
                break;
                case 'Twitter':
                    imgUrl = '/SocialMedia/Twitter.svg'
                break;
                    
                default:
                    break;
            }
        }

        const imgType = "";

        if(post !== undefined){
            if (post !== null){
              if(post.type !== null && post.type !== undefined){
                switch (post.type) {
                  case 'Image': 
                    imgType = '/Board/image.png'         
                      break;
                  case 'Album':
                    imgType= '/Board/image.png'
                      break;
                  case 'Video':
                    imgType = '/Board/video.png'
                  
                  break;
                  case 'story':
                    imgType = '/Board/video.png'
                  
                  break;
                  case 'Reel':
                    imgType = '/Board/video.png'
                  
                  break;
                      
                  default:
                      break;
              }
              }
            }
        }

        const hasImage = false;
        if(post.media_url !== null && post.media_url.trim().length !== 0){
            hasImage = true;
        }

        return (
            <div className='grid grid-cols-12 align-center'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row mt-[5px] mb-[5px]'>
                    {hasImage ?
                        <div className='pr-[10px] self-center'>
                            <Image
                                className='rounded-md'
                                src={imgUrl}
                                alt='media'
                                layout='fixed'
                                width={30}
                                height={30}
                            />
                        </div>
                        : <></>
                    }
                    {hasImage ?
                        <div className='pr-[10px] self-center'>
                            <Image
                                className=''
                                src={imgType}
                                alt='img'
                                layout='fixed'
                                width={30}
                                height={30}
                                />
                        </div>
                        : <></>
                    }
                    {hasImage ?
                        <div className='pr-[10px] self-center'>
                            <Image
                                className='rounded-md'
                                src={post.media_url}
                                alt='media'
                                layout='fixed'
                                width={30}
                                height={30}
                            />
                        </div>
                        : <></>
                    }
                   
                <div className={ hasImage ? 'col-span-8' : 'col-span-12'}>
                    <p className='text-[12px] self-center'>
                        {post.title}
                    </p>
                    <div>
                        <p className='text-gray-500 text-[10px]'>{post.subtitle}</p>
                    </div>
                </div>
                </div>
            </div>
        )
    };      
    
    useEffect(() => {
        if(selectedPlan){
            setIsLoadingPost(true)
            setShowPostSelect(true)
            setShowFormFields(false)

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/all/' + selectedPlan.value)
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.title, post: result})
                })

                setClientPlanPosts(temp)
            } else {
                console.error(data.error)
            }
            setIsLoadingPost(false)
        })
    }
    }, [selectedPlan])

    useEffect(() => {
        if(selectedPlan){
            setShowFormFields(true)
        }
    }, [selectedPlan])
    
    
    const getDataOrden = async () => {
        var data = new FormData();
        console.warn(clientPlanPostId)
        console.warn(endDate)
        console.warn(startDate)
        data.append("client_id", selectedClient.value);
        data.append("client_plan_id", selectedPlan.value);
        data.append("client_plan_post_id", clientPlanPostId.value);
        data.append("start_datetime", startDate + ' ' + startTime);
        data.append("end_datetime", endDate + ' ' + endTime );
        data.append("amount", amount);
    
        fetch(process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPostsAds/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            //console.log('VALOR ENDPOINTS: ', data);

            setIsLoading(false)
            if(data.status){
              clearForm()
              setShowModal(false)
              setReloadAds(!reloadAds)
            } else {
              alert(JSON.stringify(data.errors, null, 4))
            }
          },
          (error) => {
            console.log(error)
          }
          )
    
      }


    
    const validationSchema = Yup.object().shape({

        startDate: Yup.string()
            .required('startDate is required'),
    
        startTime: Yup.string()
        .required('startTime is required'),
    
        endTime: Yup.string()
            .required('endTime is required'),

        endDate: Yup.string()
        .required('endDate is required'),

        amount: Yup.string()
        .required('amount is required'),
    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            getDataOrden()
            return false;
            
        }

    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[22px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[46px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[12px] md:text-[15px] lg:text-[15px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Ads
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE ADS
                                    </h4>

                                    </div>
                                    <div>
                                        <form onSubmit={handleSubmit(onSubmit)}>

                                            <div className="mt-[20px] grid grid-cols-12 gap-4">

                                                <div className='col-span-12 md:col-span-12 lg:col-span-12 '>

                                                    {isLoadingClients ?

                                                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                                        <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                                    </div>
                                                    :
                                                        <div>
                                                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Select Client</p>
                                                            <ReactSelect 
                                                                defaultValue={selectedClient}
                                                                onChange={setSelectedClient}
                                                                formatOptionLabel={formatOptionLabelClient}
                                                                options={clients}
                                                                />
                                                        </div>
                                                    }
                                                </div>

                                                {showPlansSelect ?
                                                    
                                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                                                    {isLoadingPlans ?
                                                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                                        <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                                    </div>
                                                    :
                                                        <div>
                                                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Select Plan</p>
                                                            
                                                            <ReactSelect 
                                                                defaultValue={selectedPlan}
                                                                onChange={setSelectedPlan}
                                                                formatOptionLabel={formatOptionLabelPlan}
                                                                options={plans}    
                                                                />
                                                        </div>
                                                        }
                                                    </div>
                                                : <></>}

                                                {showPostSelect ?
                                                    
                                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                                                    {isLoadingPost ?
                                                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                                        <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                                    </div>
                                                    :
                                                        <div>
                                                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Select Post</p>
                                                            
                                                            <ReactSelect 
                                                            defaultValue={clientPlanPostId}
                                                            onChange={setClientPlanPostId}
                                                            formatOptionLabel={formatOptionLabelPost}
                                                            options={clientPlanPosts} 
                                                        />
                                                        </div>
                                                        }
                                                    </div>
                                                : <></>}

                                                {
                                                showFormFields ? 
                                                    <>
                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date start </p>
                                                        <input name="startDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                        {...register('startDate')}
                                                        value={startDate}
                                                        onChange={(e) => {
                                                            setStartDate(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.startDate?.message}</div>
                                                    </div>
                                                    
                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time Star</p>
                                                        <input name="startTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                        {...register('startTime')}
                                                        value={startTime}
                                                        onChange={(e) => {
                                                            setStartTime(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.startTime?.message}</div>
                                                    </div>

                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date End </p>
                                                        <input name="endDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                        {...register('endDate')}
                                                        value={endDate}
                                                        onChange={(e) => {
                                                            setEndDate(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.endDate?.message}</div>
                                                    </div>
                                                    
                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time End</p>
                                                        <input name="endTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                        {...register('endTime')}
                                                        value={endTime}
                                                        onChange={(e) => {
                                                            setEndTime(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.endTime?.message}</div>
                                                    </div>

                                                    <div className="col-span-12 md:col-span-12 lg:col-span-12">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>amount</p>
                                                        <input name='amount' type="number" placeholder="1.0" step="0.01" min="0.01"  className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                        {...register('amount')}
                                                        value={amount}
                                                        onChange={(e) => {
                                                            setAmount(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.amount?.message}</div>
                                                    </div>
                                                    </>
                                                    : <></>}


                                            </div>
                                            <div className="flex flex-row justify-between mt-[20px]">

                                                <div>
                                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                                </div>

                                                <div>
                                                    <button
                                                        className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                        onClick={() =>
                                                            setShowModal(false)
                                                        }
                                                        disabled={isLoading}
                                                    >
                                                        Cancel
                                                    </button>
                                                    <button
                                                        className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                        type="submit"
                                                        disabled={isLoading}
                                                    >
                                                        {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}