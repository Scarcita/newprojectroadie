import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import moment from 'moment';

import Tooltip from './tooltip';
import Link from 'next/link';

const Table = (props) => {

    const {loading, data, getDashboard} = props;

    const headlist = [
        'Date', "Client", "Social Network", 'Post', 'Duration', "Amount", ""
    ];

    return (
        loading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>
                <TablaProductos headlist={headlist} data={data} getDashboard={getDashboard} />
                <TablaResponsive headlist={headlist} data={data} getDashboard={getDashboard} />
            </div>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, getDashboard } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
            if(date[1] !== undefined){
                element['created_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitData(data);

    const splitStartDate = (obj) => {
        data.forEach(element => {
            let date = element.start_datetime.split("T");
            element.start_datetime = date[0];
            if(date[1] !== undefined){
                element['start_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitStartDate(data);

    const splitEndDate = (obj) => {
        data.forEach(element => {
            let date = element.end_datetime.split("T");
            element.end_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitEndDate(data);

    return (
        <div className="rounded-lg shadow hidden lg:block md:block mt-[20px]">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map((header, index) => <th key={`header-${index}`} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>
                <tbody className=''>
                    {
                        data.map((row, index) => {

                            const imgUrl = "";

                            if(row.clients_plans_post.social_network !== null){
                                switch (row.clients_plans_post.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;                            
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;                                        
                                    default:
                                        break;
                                }
                            }
                            return (
                                <tr key={row.id} >
                                    <td>
                                        <div className='text-[12px] whitespace-nowrap text-center pl-[10px]'>
                                            {row.created}
                                        </div>
                                        <div className='text-[12px] whitespace-nowrap text-center'>
                                            {row.created_hour}
                                        </div>
                                    </td>
                                    <td className='pl-[10px]'>
                                        <Link href={`/clientPlan/${row.clients_plans_post.clients_plan.id}`} style={{cursor: 'POINTER'}}>
                                            <div className='flex flex-row'>
                                                    <div className='w-[30px] h-[30px] rounded-full self-center items-center'>
                                                        {row.clients_plans_post.clients_plan.client.img_url !== null ?
                                                            <Image
                                                                className='rounded-full '
                                                                src={row.clients_plans_post.clients_plan.client.img_url}
                                                                alt='media'
                                                                layout='fixed'
                                                                width={30}
                                                                height={30}
                                                            />
                                                            : <></>
                                                        }
                                                    </div>
                                                    <div className='ml-[5px] text-[12px] font-semibold self-center'>
                                                        {row.clients_plans_post.clients_plan.client.name}
                                                    </div>
                                            </div>
                                        </Link>
                                    </td>
                                    <td>
                                        <div  className='self-center items-center text-center'>
                                            {row.clients_plans_post.social_network !== null ?
                                                <Image
                                                    src={imgUrl}
                                                    alt='imagenFaceboock'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>

                                    </td>
                                    <td>
                                        <div className='flex flex-row self-center'>
                                            <div className='w-[40px] h-[40px] rounded-lg self-center items-center'>
                                                {row.clients_plans_post.media_url !== null ?
                                                    <Image
                                                        className='rounded-lg'
                                                        src={row.clients_plans_post.media_url}
                                                        alt='media'
                                                        layout='fixed'
                                                        width={40}
                                                        height={40}
                                                    ></Image>
                                                    : <></>
                                                }
                                            </div>

                                            <div className='ml-[5px]'>
                                                <p className='text-[12px] text-left font-medium leading-2 whitespace-normal'>{row.clients_plans_post.title}</p>
                                                <p className='text-[#582BE7] font-medium leading-2 whitespace-normal text-[10px] text-left'>{row.clients_plans_post.subtitle}</p>
                                                <p className='text-gray font-light leading-2 whitespace-normal text-[10px] text-left'>{row.clients_plans_post.post_copy}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td className='pl-[10px] pr-[10px]'>
                                        <div className='text-[12px] whitespace-nowrap text-center'>
                                            {row.start_datetime}

                                        </div>
                                        
                                        <div className='text-[12px] whitespace-nowrap text-center'>
                                            {row.end_datetime}
                                        </div>

                                    </td>
                                    <td className='text-center self-center items-center'>

                                        {row.amount !== null ?
                                            <div className='text-[#643DCE] text-[20px] font-semibold flex'>
                                                {row.amount}
                                                <span className='text-[12px] text-[#643DCE] self-center pl-[5px]'> USD</span>
                                            </div>
                                            : <></>
                                        }

                                    </td>
                                    <td>
                                        <div className='text-center pr-[10px]'>
                                            <Tooltip 
                                                row={row}
                                                id={row.id}
                                                title={row.clients_plans_post.title} 
                                                subtitle={row.clients_plans_post.subtitle} 
                                                social_network={row.clients_plans_post.social_network} 
                                                type={row.clients_plans_post.type} 
                                                start_datetime={row.start_datetime} 
                                                end_datetime={row.end_datetime} 
                                                amount={row.amount}
                                                // reloadAds={reloadAds} 
                                                // setReloadAds={setReloadAds}
                                            />
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    );
};


const TablaResponsive = (props) => {
    const { headlist, data, getDashboard } = props;
    
    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
            if(date[1] !== undefined){
                element['created_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitData(data);

    const splitStartDate = (obj) => {
        data.forEach(element => {
            let date = element.start_datetime.split("T");
            element.start_datetime = date[0];
            if(date[1] !== undefined){
                element['start_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitStartDate(data);

    const splitEndDate = (obj) => {
        data.forEach(element => {
            let date = element.end_datetime.split("T");
            element.end_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitEndDate(data);

    return (
        <div className='grid grid-cols-12 '>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                        {

                            const imgUrl = "";

                            if(row.clients_plans_post.social_network !== null){
                                switch (row.clients_plans_post.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;
                            
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;
                                        
                                    default:
                                        break;
                                }
                            }

                            if(row.start_datetime !== null && row.start_datetime !== undefined){
                                var eventdate = row.start_datetime;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var startday = splitdate[2];
                                var startyear = splitdate[0];
                            }


                            if(row.start_datetime !== null && row.start_datetime !== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.start_datetime);
                                var startdayName = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.start_datetime !== null && row.start_datetime !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.start_datetime);
                                var startmonthName = months[m.getMonth()];
                            }

                            if(row.end_datetime!== null && row.end_datetime !== undefined){
                                var eventdate = row.end_datetime;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var endday = splitdate[2];
                                var endyear = splitdate[0];
                            }


                            if(row.end_datetime !== null && row.end_datetime !== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.end_datetime);
                                var enddayName = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.end_datetime !== null && row.end_datetime !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.end_datetime);
                                var endmonthName = months[m.getMonth()];
                            }
                              
                            
                        return (
                        <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                            <div className='col-span-12 md:col-span-12'>
                                <div className='grid grid-cols-12'>
                                    <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                        <div className='grid grid-cols-12 gap-4'>
                                            <div className='col-span-6 md:col-span-6 flex flex-row'>
                                                <div className='w-[30px] h-[30px] rounded-full self-center items-center'>
                                                    {row.clients_plans_post.clients_plan.client.img_url !== null ?
                                                        <Image
                                                            className='rounded-full '
                                                            src={row.clients_plans_post.clients_plan.client.img_url}
                                                            alt='media'
                                                            layout='fixed'
                                                            width={30}
                                                            height={30}
                                                        />
                                                        : <></>
                                                    }
                                                </div>
                                                <div className='ml-[5px] text-[12px] font-semibold self-center'>
                                                    {row.clients_plans_post.clients_plan.client.name}
                                                </div>

                                            </div>

                                            <div className='col-span-6 md:col-span-6'>
                                                {/* <p className='text-[12px]'>created: </p> */}
                                                <div className='text-[12px] text-[#582BE7] font-semibold text-center'>{row.created} {row.created_hour}</div>

                                            </div>

                                            <div className='col-span-6 md:col-span-6 flex flex-row justify-between'>
                                                <div className='text-left'>
                                                    <p className='text-[26px] font-bold text-center'>{startday}</p>
                                                    
                                                </div>
                                                <div className='text-end'>
                                                    <p className='text-[14px] text-[#582BE7]  leading-4 whitespace-normal'>{startdayName}</p>
                                                    <p className='text-[16px] font-semibold leading-4 whitespace-normal'>{startmonthName} {startyear}</p>
                                                    
                                                </div>

                                            </div>

                                            <div className='col-span-6 md:col-span-6 flex flex-row justify-between'>
                                                <div className='text-left'>
                                                    <p className='text-[26px] font-bold text-center'>{endday}</p>
                                                    
                                                </div>
                                                <div className='text-end'>
                                                    <p className='text-[14px] text-[#582BE7]  leading-4 whitespace-normal'>{enddayName}</p>
                                                    <p className='text-[16px] font-semibold leading-4 whitespace-normal'>{endmonthName} {endyear}</p>
                                                    
                                                </div>

                                            </div>

                                            <div className='col-span-12 md:col-span-12 flex flex-row'>
                                                <div className='w-[80px] h-[80px] rounded-lg self-center items-center'>
                                                    {row.clients_plans_post.media_url !== null ?
                                                        <Image
                                                            className='rounded-lg'
                                                            src={row.clients_plans_post.media_url}
                                                            alt='media'
                                                            layout='fixed'
                                                            width={80}
                                                            height={80}
                                                        ></Image>
                                                        : <></>
                                                    }
                                                </div>

                                                <div className='ml-[5px]'>
                                                    <p className='text-[12px] text-left font-medium leading-2 whitespace-normal'>{row.clients_plans_post.title}</p>
                                                    <p className='text-[#582BE7] font-medium leading-2 whitespace-normal text-[10px] text-left'>{row.clients_plans_post.subtitle}</p>
                                                    <p className='text-gray font-light leading-2 whitespace-normal text-[10px] text-left'>{row.clients_plans_post.post_copy}</p>
                                                </div>
                                            </div>

                                            <div className='col-span-6 md:col-span-6 self-center items-center text-center'>
                                        
                                                <div>
                                                    {row.social_network !== null ?
                                                        <Image
                                                            src={imgUrl}
                                                            alt=''
                                                            layout='fixed'
                                                            width={30}
                                                            height={30}
                                                        />
                                                        : <></>
                                                    }
                                                </div>

                                            </div>
                                            <div className='col-span-6 md:col-span-6 self-center items-center text-center '>
                                                {row.amount !== null ?
                                                    <div className='flex'>
                                                        <p className='text-[#643DCE] text-[30px] font-semibold'>{row.amount}</p>
                                                        <p className='text-[18px] text-[#643DCE] self-center pl-[5px]'> USD</p>
                                                    </div>
                                                    : <></>
                                                }

                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-2 md:col-span-12'>
                                        <div className=' items-center text-center '>
                                        <Tooltip 
                                            row={row}
                                            id={row.id}
                                            title={row.clients_plans_post.title} 
                                            subtitle={row.clients_plans_post.subtitle} 
                                            social_network={row.clients_plans_post.social_network} 
                                            type={row.clients_plans_post.type} 
                                            start_datetime={row.start_datetime} 
                                            end_datetime={row.end_datetime} 
                                            amount={row.amount}
                                            // reloadAds={reloadAds} 
                                            // setReloadAds={setReloadAds}
                                        />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            )
                        }
                    )}
            </div>
        </div>

        
    );
};



export default Table;
