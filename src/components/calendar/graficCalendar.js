
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import Calendar from 'react-calendar';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import ModalDetalle from './modalDetalle';
import TooltipPlans from '../plans/tooltip';
//import 'react-calendar/dist/Calendar.css';

const GraficCalendar = (props) => {

  const { 
    clientId, 
    reloadCalendar, 
    setReloadCalentar, 
    reloadEdit, 
    setReloadEdit
  } = props
  const [todaysDate, setTodaysDate] = useState(new Date());
  const [isLoading, setIsLoading] = useState(true)
  const [data, setData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedPost, setSelectedPost] = useState(null);

  const getDatacalendar = async (clientId) => {

    var url = process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/monthCalendar';

    if(clientId != 0){
      url = process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/monthCalendarByClient/' + clientId; 
    }

    setIsLoading(true)

    fetch(url)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setData(data.data)

            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })
  }

  useEffect(() => {
    getDatacalendar(clientId);
  }, [clientId, reloadCalendar,])

  const onClickDay = (value, event) =>{
    setShowModal(true)
  }
  

  // state の日付と同じ表記に変換
  const getFormatDate = (date) => {
    return `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}${('0' + date.getDate()).slice(-2)}`;
  }

  //日付のクラスを付与 (祝日用)
  const getTileClass = ({ date, view }) => {
    // 月表示のときのみ
    if (view !== 'month') {
      return '';
    }
    const day = getFormatDate(date);
    return (data[day] && data[day].planned_datetime) ?
      'holiday' : '';
  }

  //日付の内容を出力
  const getTileContent = ({ date, view }) => {
    // 月表示のときのみ
    if (view !== 'month') {
      return null;
    }
    const day = getFormatDate(date);
    
    if(!(day in data)){
      return <></>
    } else {
      if(data[day].length == 0){
        return <></>
      }
    }


    const posts = data[day];

    //console.warn(posts);

    if(posts.length === 0){
      return <></>
    }
  
    return (
      
        <div className='h-[100px] overflow-x-auto rounded-lg bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 ml-[10px] pl-[3px] pr-[3px]'>
          
          {
            posts.map((post, index) => {

              const imgUrl = "";

              if(post.social_network !== null){
                  switch (post.social_network) {
                      case 'Facebook': 
                          imgUrl = '/SocialMedia/Facebook.svg'
                          break;
                      case 'TikTok':
                          imgUrl= '/SocialMedia/TikTok.svg'
                          break;
                      case 'Instagram':
                          imgUrl = '/SocialMedia/Instagram.svg'
                      break;
                      case 'YouTube':
                          imgUrl = '/SocialMedia/Youtube.svg'
                      break;
                      case 'Mailing':
                          imgUrl = '/Plans/gmail.svg'
                      break;
                      case 'LinkedIn':
                          imgUrl = '/SocialMedia/messenger.svg'
                      break;
                      case 'Twitter':
                          imgUrl = '/SocialMedia/Twitter.svg'
                      break;
                          
                      default:
                          break;
                  }
              }
              
              return (
                
                <div className='grid grid-cols-12 gap-3' onClick={() => {setSelectedPost(post), setShowModal(true)}}
                key={post.id}>
                  <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[10px]'>
                    <div className='flex flex-row'>
                      <div className='mr-[5px]'>

                        {clientId != 0 ?
                          (
                            post.social_network !== null ?
                                          <Image
                                              src={imgUrl}
                                              alt=''
                                              layout='fixed'
                                              width={25}
                                              height={25}
                                          />
                                          : <></>
                          )
                          :
                          (
                            post.clients_plan.client.img_url != null && post.clients_plan.client.img_url.length > 0 ?
                              
                              <Image 
                                className='rounded-full'
                                src={post.clients_plan.client.img_url}
                                alt='media'
                                layout='fixed'
                                height={18}
                                width={18}
                                >

                              </Image> 
                              : <></>  
                          ) 
                        }

                      </div>
                    
                      <div>
                        {post.status !== null ?
                          <div className='text-[7px] rounded-[15px] pl-[3px] pr-[3px] self-center bg-[#fff] text-[#000000]'>
                            {post.status}
                          </div>
                          : <></>
                        }
                        
                        {post.type !== null ?
                          <div className='text-[8px] text-[#FFFFFF] text-bold'>
                            {post.type}
                          </div>
                          : <></>
                        }
                      </div>

                    </div>
                      {post.title !== null ?
                          <p className='text-[10px] font-semibold text-left leading-2 whitespace-normal text-ellipsis ...'>
                            {post.title}
                          </p>
                          : <></>
                        }

                  </div>
                </div>             
              )
                  
            })
    
          }

        </div>
    );
  }

  return (
      isLoading ?
          <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
              <Spinner color="#582BE7" size={16} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
          </div>
              :
          <div className='grid grid-cols-7 overflow-auto'>
            <div className='col-span-7 md:col-span-7 lg:col-span-7'> 
              <Calendar
                locale="US"
                value={todaysDate}
                onChange={setTodaysDate} 
                tileClassName={getTileClass}
                tileContent={getTileContent}
                // width={700}
                // height={48}
                className={'bg-[#000000] opacity-80 text-[#fff] text-center p-[20px]'}
                //activeStartDate={new Date()}
                calendarType="US"
                selectRange={true}
                showNavigation={true}
                
              />
              <ModalDetalle 
                showModal={showModal} 
                setShowModal={setShowModal} 
                todaysDate={todaysDate} 
                selectedPost={selectedPost} 
                // reloadEdit={reloadEdit}
                // setReloadEdit={setReloadEdit}
              ></ModalDetalle>
            </div>
          </div>
  );
} 

export default GraficCalendar;