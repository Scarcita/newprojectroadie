import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useRouter } from 'next/router';
import Link from 'next/link';

import imagenFaceboock from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/ClientPlan/logo.svg"
import marca from "../../../public/ClientPlan/marca.svg"
import editar from "../../../public/ClientPlan/editar.svg"
import TooltipClientDashboard from './tooltip';





const TableDashoardCli = (props) => {
    

    const {reloadPlans, setReloadPlans, client_id} = props
    //const {reloadPosts, reloadAds, client_id} = props

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const router = useRouter()
    const headlist = [
        'Plan', "Dates", "Service Total", "Ads Total", "Total", "Paid", "Invoice", "Total W/Invoice"
    ];

    useEffect(() => {

        setIsLoading(true)

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/all/' + client_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadPlans);

        }, [reloadPlans])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos data={data} headlist={headlist} client_id={client_id}  reloadPlans={reloadPlans} setReloadPlans={setReloadPlans}/>
                    <TablaResponsive data={data} headlist={headlist} client_id={client_id} reloadPlans={reloadPlans} setReloadPlans={setReloadPlans} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, reloadPlans, setReloadPlans, client_id} = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="rounded-lg shadow hidden lg:block md:block mt-[20px]">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map((header, index) => <th key={index} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row => {
                        return (
                            <tr key={row.id} >

                                <td className='pl-[15px] text-[14px] font-medium] '>
                                    <Link
                                        href={`/clientPlan/${encodeURIComponent(row.id)}`}
                                    >
                                        {row.plan.name}
                                    </Link>                                
                                </td>
                            
                                <td className=' text-center text-[13px] '>
                                    <div>

                                        {row.start_date}

                                    </div>
                                    
                                    <div>

                                        {row.end_date}

                                    </div>

                                </td>
                                <td className='text-center text-[24px] font-semibold text-cente '>

                                    {row.service_total}
    
                                </td>


                                <td className='text-[24px] font-semibold text-center'>
                                    {row.ads_total}

                                </td>
                                <td className='text-[24px] font-semibold text-center '>
                                    {row.total}

                                </td>
                                <td className='text-center '> 
                                    <div className='text-[#643DCE] font-semibold text-[14px] '>
                                        {row.paid}
                                    </div>
                                    <div className='text-[#643DCE] text-[10px]'>
                                        {row.paid_date}
                                    </div>

                                </td>

                                <td className='text-center'> 
                                    <div className='text-[#643DCE] font-semibold text-[14px]'>
                                        {row.invoiced}
                                    </div>
                                    <div className='text-[#643DCE] text-[10px]'>
                                        {row.invoiced_date}
                                    </div>

                                </td>

                                <td className=''> 
                                    <div className='text-[24px] font-semibold text-center'>
                                    {row.total_with_invoice}
                                    </div>
                                    
                                    {row.invoice_number !== null ?
                                        <div className='text-[12px] text-center'>
                                        {row.invoice_number} (Invoice)
                                        </div>
                                    : <></>
                                    }

                                </td>
                                <td className='text-center'> 
                                    <TooltipClientDashboard
                                        row={row}
                                        id={row.id}
                                        client_id={client_id}
                                        plan={row.plan.name}
                                        startDate={row.start_date}
                                        endDate={row.end_date}
                                        ads_total={row.ads_total}
                                        total={row.total}
                                        service_total={row.service_total}
                                        paid={row.paid_date}
                                        paid_date={row.paid_date}
                                        invoiced={row.invoiced}
                                        price={row.plan.price}
                                        invoiced_date={row.invoiced_date}
                                        total_with_invoice={row.total_with_invoice}
                                        invoice_number={row.invoice_number} 
                                        reloadPlans={reloadPlans} 
                                        setReloadPlans={setReloadPlans}
                                    />

                                </td>


                            </tr>

                        )
                    }
                    )}
                </tbody>
            </table>
        </div>
    );
};

const TablaResponsive = (props) => {
    const { headlist, data, reloadPlans, setReloadPlans, client_id } = props;

    return (
        <div className='grid grid-cols-12 mt-[20px]'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                        {

                        

                            if(row.start_date !== null && row.start_date !== undefined){
                                var eventdate = row.start_date;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var day = splitdate[2];
                                var year = splitdate[0];
                            }


                            if(row.start_date !== null && row.start_date !== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.start_date);
                                var dayName = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.start_date !== null && row.start_date !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.start_date);
                                var monthName = months[m.getMonth()];
                            }

                            if(row.end_date !== null && row.end_date !== undefined){
                                var eventdate = row.end_date;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var dayEnd = splitdate[2];
                                var yearEnd = splitdate[0];
                            }


                            if(row.end_date !== null && row.end_date!== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.end_date);
                                var dayNameEnd = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.end_date!== null && row.end_date !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.end_date);
                                var monthNameEnd = months[m.getMonth()];
                            }
                              
                            
                        return (
                        <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                            <div className='col-span-12 md:col-span-12'>
                                <div className='grid grid-cols-12'>
                                    <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                        <div className='grid grid-cols-12 gap-1'>
                                            <div className='col-span-12 md:col-span-12'>
                                                <div className='grid grid-cols-12 gap-4 mt-[5px] mb-[5px]'>
                                                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                                                        <div>
                                                        {row.plan.name}
                                                        </div>
                                                        
                                                    </div>
                                                    <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row justify-between'>
                                                        <div>
                                                            <p className='text-[30px] font-bold pr-[5px]'>{day}</p> 
                                                        </div>
                                                        <div className='self-center items-center'>
                                                            <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayName}</p>
                                                            <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthName} {year}</p>
                                                            
                                                        </div>
                                                    </div>
                                                    <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row justify-between'>
                                                        <div>
                                                            <p className='text-[30px] font-bold text-left pr-[5px]'>{dayEnd}</p>
                                                            
                                                        </div>
                                                        <div className='self-center items-center'>
                                                            <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayNameEnd}</p>
                                                            <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthNameEnd} {yearEnd}</p>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                                                        <div className='text-[14px] font-semibold text-left pr-[5px]'>Servicio Total: {row.service_total}</div>
                                                        <div className='text-[14px] font-semibold text-left pr-[5px]'>Ads total: {row.ads_total}</div>
                                                        <div className='text-[14px] font-semibold text-left pr-[5px]'>Total{row.total}</div>
                                                        <div className='text-[14px] font-semibold text-left pr-[5px]'>
                                                            Paid:
                                                            <div className='text-[#643DCE] font-semibold text-[14px] '>
                                                                {row.paid}
                                                            </div>
                                                            <div className='text-[#643DCE] text-[10px]'>
                                                                {row.paid_date}
                                                            </div>
                                                        </div>
                                                        <div className='text-[14px] font-semibold text-left pr-[5px]'>
                                                            Invoice
                                                            <div className='text-[#643DCE] font-semibold text-[14px]'>
                                                                {row.invoiced}
                                                            </div>
                                                            <div className='text-[#643DCE] text-[10px]'>
                                                                {row.invoiced_date}
                                                            </div>
                                                        </div>

                                                        <div className='text-[14px] font-semibold text-left pr-[5px]'>
                                                            Total W/Invoice:
                                                            <div className='text-[14px] font-semibold text-center'>
                                                                {row.total_with_invoice}
                                                            </div>
                                                            
                                                            {row.invoice_number !== null ?
                                                                <div className='text-[12px] text-center'>
                                                                {row.invoice_number} (Invoice)
                                                                </div>
                                                            : <></>
                                                            }
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                

                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div className='col-span-2 md:col-span-12'>
                                        <div className=' items-center text-center '>
                                            <TooltipClientDashboard
                                                row={row}
                                                client_id={client_id}
                                                id={row.id}
                                                plan={row.plan.name}
                                                startDate={row.start_date}
                                                endDate={row.end_date}
                                                ads_total={row.ads_total}
                                                total={row.total}
                                                service_total={row.service_total}
                                                paid={row.paid_date}
                                                paid_date={row.paid_date}
                                                invoiced={row.invoiced}
                                                price={row.plan.price}
                                                invoiced_date={row.invoiced_date}
                                                total_with_invoice={row.total_with_invoice}
                                                invoice_number={row.invoice_number} 
                                                reloadPlans={reloadPlans} 
                                                setReloadPlans={setReloadPlans}
                                            />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                            )
                        }
                    )}
            </div>
        </div>

        
    );
};



export default TableDashoardCli;