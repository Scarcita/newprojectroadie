import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';
import Image from 'next/image';


export default function CounterAdsYouTube(props) {
    const { client_plan_id, reloadAds } = props
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    //const [CounterAdsFb, setCounterAdsFacebook] = useState(0);
    const [counterRrss, setCounterRrss] = useState([]);
    const [counterAdsYouTube, setCounterAdsYouTube] = useState(0);



    useEffect(() => {
        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPostsAds/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    data.data.map((result) => {
                        temp.push(result)
                    })
                    let temporal = 0;
                    var youtubeAdsAmountCounter = 0
                    for (let i = 0; i < temp.length; i++) {
                        temp[i]._matchingData.ClientsPlansPosts.social_network === "YouTube" ?
                            (
                                temporal++,
                                youtubeAdsAmountCounter += parseFloat(temp[i].amount),
                                setCounterAdsYouTube(youtubeAdsAmountCounter)
                            )
                            :
                            null

                    }
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadAds])

    return (
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={10} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div className='text-[24px] font-semibold self-center'>{counterAdsYouTube} <span className='text-[12px] self-center'>USD</span></div>
            
    )
}