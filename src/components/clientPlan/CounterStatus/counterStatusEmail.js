import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';
import Image from 'next/image';


export default function CounterStatusEmail(props) {
    const { client_plan_id, reloadPosts} = props
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    //const [CounterAdsFb, setCounterAdsFacebook] = useState(0);
    const [counterRrss, setCounterRrss] = useState([]);
    const [counterAdsAll, setCounterAdsAll] = useState(0);
    const [counterPlan, setCounterPlan] = useState(0);



    useEffect(() => {
        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    data.data.map((result) => {
                        temp.push(result)
                    })
                    let temporal = 0;
                    let totalPlan = 0;

                    //var facebookAdsAmountCounter = 0
                    for (let i = 0; i < temp.length; i++) {
                        temp[i].social_network === "Mailing" ?
                            (
                                temporal++,
                                //facebookAdsAmountCounter += parseFloat(temp[i].amount),
                                setCounterAdsAll(temporal)
                            )
                            :
                            null

                    }
                    for (let i = 0; i < temp.length; i++) {
                        temp[i].client_plan_id !== null ?
                            (
                                totalPlan++,
                                //facebookAdsAmountCounter += parseFloat(temp[i].amount),
                                setCounterPlan(totalPlan)
                            )
                            :
                            null

                    }
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadPosts])

    return (
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', }}>
                <Spinner color="#fff" size={10} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div className='text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF] self-center'>{counterAdsAll}/{counterPlan}</div>
            
    )
}