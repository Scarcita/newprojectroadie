import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';
import Image from 'next/image';


export default function CounterPlanPrice(props) {
    const { client_plan_id, client_id} = props
    const [isLoading, setIsLoading] = useState(true)
    const [counterAdsPlan, setCounterAdsPlan] = useState(0);
    console.log(client_plan_id);



    useEffect(() => {

        setIsLoading(true)

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlans/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            //console.warn('reloaddd' + reloadPlans);

        }, [])

    return (
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', }}>
                <Spinner color="#582BE7" size={10} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div className='text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]'>{counterAdsPlan} <span className='text-[16px] self-center'>USD</span></div>
            
    )
}