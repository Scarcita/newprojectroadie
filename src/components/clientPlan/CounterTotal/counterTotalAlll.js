import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';
import Image from 'next/image';


export default function CounterAll(props) {
    
    const { counterAdsPrice, counterAdsPlan} = props
    console.log( 'suma nan: ' + counterAdsPrice);
    const [isLoading, setIsLoading] = useState(true)

    // let total = parseFloat(counterAdsPrice) + parseFloat(counterAdsPlan)
    //              console.log('suma: ' + total);

    //     useEffect(() => {
    //     fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPostsAds/all/')
    //         .then(response => response.json())
    //         .then(data => {
    //             if (data.status) {
    //                 var temp = [];
    //                 data.data.map((result) => {
    //                     temp.push(result)
    //                 })
    //             } else {
    //                 console.error(data.error)
    //             }
    //             setIsLoading(false)
    //         })
    // }, [])


    return (
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', }}>
                <Spinner color="#582BE7" size={10} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div className='text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]'>{'0'} <span className='text-[16px] self-center'>USD</span></div>
            
    )
}