import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactModal from 'react-modal';
import ReactSelect from 'react-select';
import { customModalStyles, getImgUrlForSocialNetwork } from '../../utils';
import moment from 'moment';

export default function ModalAd(props) {

    const { loadClientPlanData, client_plan_id, row, edit } = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const [clientPlanPostId, setClientPlanPostId] = useState(edit && row ? row.client_plan_post_id : null);    
    const [clientPlanPosts, setClientPlanPosts] = useState([]);

    const validationSchema = Yup.object().shape({  
        startDateTime: Yup.string().required('startDateTime is required'),   
        endDateTime: Yup.string().required('endDateTime is required'),   
        amount: Yup.string().required('Amount is required'),
        adId: Yup.string().optional().nullable(),
    });

    const formOptions = {
        defaultValues: {
            startDateTime: edit && row ? row.start_datetime : '',
            endDateTime: edit && row ? row.end_datetime : '',
            amount: edit && row ? row.amount : '',
            adId: edit && row ? row.fb_ig_ad_id : '',
        },
        resolver: yupResolver(validationSchema)
    };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        registerPost(data);
    }

    const registerPost = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        data.append("fb_ig_ad_id", formData.adId);
        // data.append("client_plan_id", client_plan_id);
        if(!edit){
            data.append("client_plan_post_id", clientPlanPostId.value);
        }
        data.append("start_datetime", moment(formData.startDateTime).format('YYYY-MM-DD HH:mm:ss'));
        data.append("end_datetime", moment(formData.endDateTime).format('YYYY-MM-DD HH:mm:ss'));
        data.append("amount", formData.amount);
        
        var url = process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPostsAds/addMobile";

        if(edit && row){
            url = process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPostsAds/editMobile/"+ row.id;
        }
    
        fetch(url, {
          method: 'POST',
          body: data,
        })
        .then(response => response.json())
        .then(data => {
            //console.log('VALOR ENDPOINTS: ', data);

            setIsLoading(false)
            if(data.status){
                reset();
                loadClientPlanData();
                setShowModal(false);
            } else {
                alert(JSON.stringify(data.errors, null, 4));
            }
                
        },
            (error) => {
                console.log(error)
            }
        );
    
    }

    const loadPosts = () => {
        
        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.title, post: result})
                })
                setClientPlanPosts(temp)
            } else {
                console.error(data.errors)
            }
            setIsLoading(false)
        });

    }

    const selectOptionItem = ({ value, label, post }) => {
        
        const imgUrl = "";

        if(post.social_network !== null){
            imgUrl = getImgUrlForSocialNetwork(post.social_network);
        }

        const hasImage = false;
        if(post.media_url !== null && post.media_url.trim().length !== 0){
            hasImage = true;
        }

        return (
            <div className='grid grid-cols-12 align-center'>
                <div className='col-span-12 flex flex-row mt-[5px] mb-[5px]'>
                    {hasImage ?
                        <div className='pr-[10px]'>
                            <Image
                                className='rounded-md'
                                src={post.media_url}
                                alt=''
                                layout='fixed'
                                width={40}
                                height={40}
                            />
                        </div>
                        : <></>
                    }
                    <div className='pr-[10px]'>
                        <Image
                            className='rounded-md'
                            src={imgUrl}
                            alt=''
                            layout='fixed'
                            width={40}
                            height={40}
                        />
                    </div>
                <div className={ hasImage ? 'col-span-8' : 'col-span-12' + ' text-[14px]'}>
                    {post.title}
                    <div>
                        <small className='text-gray-500 text-[12px]'>{post.subtitle}</small>
                    </div>
                </div>
                </div>
            </div>
        )
    };

    useEffect(() => {
        loadPosts();
    }, []);

    return (
        <>
            <div>                
                {
                    edit && row
                    ?
                        <button
                            className="pt-1 pb-1 items-center flex"
                            onClick={() => setShowModal(true)}
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                className="w-5 h-5 text-[#643DCE]"
                            >
                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                            </svg>
                            {/* <p className="text-[14px] font-semibold hover:text-[#643DCE]">Edit</p> */}

                        </button>
                    :
                        <button
                            className="px-7 py-1 bg-[#582BE7] border border-[#582BE7] text-[#FFFFFF] text-sm rounded-[25px] hover:bg-[#fff] hover:border-[#582BE7] hover:text-[#582BE7]"
                            endDateTime="button"
                            onClick={() => setShowModal(true)}
                        >
                            Create Ad
                        </button>
                }
                <ReactModal
                    isOpen={showModal}
                    shouldCloseOnEsc={true}
                    onRequestClose={() => setShowModal(false)}
                    style={customModalStyles}
                >
                    <div className="max-w-[600px] md:min-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                        <div>
                            <h4 className="text-lg font-medium text-gray-800">
                                {edit && row ? 'EDIT' : 'CREATE'} AD
                            </h4>
                        </div>

                        <div>
                            <form onSubmit={handleSubmit(onSubmit)} >

                                <div className="mt-[20px] grid grid-cols-12 gap-4">

                                    {
                                        !edit && 
                                        <div className='col-span-12'>
                                            <p className='text-xs text-gray-500 mb-[2px]'>*Post</p>
                                            <ReactSelect
                                                defaultValue={clientPlanPostId}
                                                onChange={setClientPlanPostId}
                                                options={clientPlanPosts} 
                                                formatOptionLabel={selectOptionItem}
                                            />
                                        </div>
                                    }
                            
                                    <div className="col-span-4">
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Start Date</p>
                                        <input type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`} {...register('startDateTime')}  />
                                        <div className="text-[14px] text-[#FF0000]">{errors.startDateTime?.message}</div>
                                    </div>
                            
                                    <div className="col-span-4">
                                        <p className='text-xs text-gray-500 mb-[2px]'>*End Date</p>
                                        <input type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`} {...register('endDateTime')}  />
                                        <div className="text-[14px] text-[#FF0000]">{errors.endDateTime?.message}</div>
                                    </div>

                                    <div className="col-span-4">
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Amount</p>
                                        <input className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`} {...register('amount')}  />
                                        <div className="text-[14px] text-[#FF0000]">{errors.amount?.message}</div>
                                    </div>

                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Ad ID</p>
                                        <input className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`} {...register('adId')} />
                                        <div className="text-[14px] text-[#FF0000]">{errors.adId?.message}</div>
                                    </div>
                                </div>

                                <div className="flex flex-row justify-between mt-[20px]">

                                    <div>
                                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                    </div>

                                    <div>
                                        <input
                                            readOnly={true}
                                            className="w-[75px] h-[35px] text-center border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                            onClick={() => {
                                                    reset();
                                                    setShowModal(false);
                                                }
                                            }
                                            disabled={isLoading}
                                            value={'Cancel'} 
                                        />
                                        <button
                                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                            endDateTime={'submit'}
                                            disabled={isLoading}
                                        >
                                            {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : (edit && row ? 'Update' : 'Create')}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>                    
                </ReactModal>
            </div>
        </>
    );
}