import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


export default function ModalGenerateAccount(props) {

    const { loadClientPlanData, client_plan_id, plan, service_total, ads_total, included_ads_qty, total } = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [invoice, setInvoice] = useState(false);

    const defaultValidationSchema = Yup.object().shape({});

    const invoiceValidationSchema = Yup.object().shape({
        invoiceNumber: Yup.string()
        .required('Invoice Number is required'),
        invoiceName: Yup.string()
        .required('Invoice Name is required'),
        invoiceNit: Yup.string()
        .required('Invoice Nit is required'),

        // // subtitle: Yup.string()
        // //     .required("subtitle is required"),
        // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // // mediaUrl: Yup.string()
        // //     .required('mediaUrl is required'),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // socialNetwork: Yup.string()
        //     .required('socialNetwork is required')
        //     .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),
        // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // type: Yup.string()
        //     .required('type is required'),
        // //   //.min(6, 'minimo 6 caracteres'),
        // // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),
    
        // plannedDateTime: Yup.string()
        //     .required('plannedDateTime is required'),
    
        // // instructions: Yup.string()
        // //     .required('instructions is required'),
    
        // // postCopy: Yup.string()
        // //     .required('postCopy is required'),

    
    });

    const [dynamicValidationSchema, setDynamicValidationSchema] = useState(defaultValidationSchema);


    const formOptions = { resolver: yupResolver(dynamicValidationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        registerPost(data);
    }
   
    const registerPost = async (formData) => {

        setIsLoading(true);

        var data = new FormData();

        if(invoice){
            data.append("invoiced", invoice ? '1' : 0);
            data.append("invoice_number", formData.invoiceNumber);
            data.append("invoice_nanme", formData.invoiceName);
            data.append("invoice_cinit", formData.invoiceCiNit);
        }

        data.append("service_total", service_total);
        data.append("ads_total", included_ads_qty > 0 ? (ads_total-included_ads_qty) : ads_total);
        data.append("total", total);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlans/editMobile/" + client_plan_id, {
            method: 'POST',
            body: data,
        })
        .then(response => response.json())
        .then(data => 
            {
                //console.log('VALOR ENDPOINTS: ', data);

                setIsLoading(false);

                if(data.status){
                    reset();
                    setShowModal(false);
                    loadClientPlanData();
                } else {
                    alert(JSON.stringify(data.errors, null, 4));
                }
                
            },
            (error) => {
                console.log(error);
            }
        );
    
    }

    useEffect(() => {
        if(invoice){
            setDynamicValidationSchema(invoiceValidationSchema);
        } else {
            setDynamicValidationSchema(defaultValidationSchema);
        }
    }, [invoice]);

    return (
        <>
            <div>
                <button
                    className="px-4 py-1 bg-[#582BE7] text-[#FFFFFF] font-semibold text-[10px] rounded-[25px] hover:bg-[#fff] hover:border-[#582BE7] hover:text-[#582BE7]"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Generate account detail
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-70"
                            onClick={() => {
                                setInvoice(false);
                                reset();
                                setShowModal(false);
                            }}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE POST
                                    </h4>
                                </div>

                                <div>
                                    {/* {client_plan_id } */}
                                    <form onSubmit={handleSubmit(onSubmit)} >

                                        <div className="mt-[20px]">
                                        <div className="invoice">
                                            <input
                                                type="checkbox"
                                                id="invoice"
                                                name="invoice"
                                                value="Paneer"
                                                checked={invoice}
                                                onChange={() => {setInvoice(!invoice)}}
                                            />
                                            Invoice?
                                        </div>

                                            {
                                                invoice
                                                ?
                                                <div className="grid grid-cols-12 gap-4 mt-2">
                                                    <div className="col-span-4 md:col-span-4 lg:col-span-4">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>*Invoice Number</p>
                                                        <input className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]' {...register('invoiceNumber')} />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.invoiceNumber?.message}</div>
                                                    </div>

                                                    <div className="col-span-4 md:col-span-4 lg:col-span-4">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>*Invoice Name</p>
                                                        <input className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]' {...register('invoiceName')} />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.invoiceName?.message}</div>
                                                    </div>
                                                    
                                                    <div className="col-span-4 md:col-span-4 lg:col-span-4">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>*Invoice CI/NIT</p>
                                                        <input className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]' {...register('invoiceNit')} />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.invoiceNit?.message}</div>
                                                    </div>
                                                </div>
                                                : <></>
                                            }
                                        </div>

                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <input
                                                    className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                    type="button"
                                                    onClick={() => {
                                                        setInvoice(false);
                                                        reset();
                                                        setShowModal(false);
                                                    }}
                                                    value="Cancel"
                                                />
                                                <button
                                                    className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                    type={'submit'}
                                                    disabled={isLoading}
                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}