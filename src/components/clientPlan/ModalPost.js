import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactModal from 'react-modal';
import { customModalStyles } from '../../utils';



export default function ModalPost(props) {

    const { loadClientPlanData, client_plan_id, row, edit } = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [mediaUrl, setMediaUrl] = useState(null);

    const validationSchema = Yup.object().shape({
        title: Yup.string().required('title is required'),        
        subtitle: Yup.string().optional().nullable(),
        // mediaUrl: Yup.string().optional().nullable(),    
        socialNetwork: Yup.string()
            .required('socialNetwork is required')
            .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'X']),    
        type: Yup.string()
            .required('type is required'),    
        plannedDateTime: Yup.string()
            .required('plannedDateTime is required'),
        repost: Yup.bool(),
        // .oneOf([true], 'You need to accept the terms and conditions'),
        instructions: Yup.string().optional().nullable(),
        postCopy: Yup.string().optional().nullable(),
        postId: Yup.string().optional().nullable(),
        status: Yup.string()
            .required('status is required'),    
    });

    const formOptions = {
        defaultValues: {
            title: edit && row ? row.title : '',
            subtitle: edit && row ? row.subtitle : '',
            socialNetwork: edit && row ? row.social_network : '',
            type: edit && row ? row.type : '',
            plannedDateTime: edit && row ? row.planned_datetime : '',
            instructions: edit && row ? row.instructions : '',
            postCopy: edit && row ? row.post_copy : '',
            postId: edit && row ? row.fb_ig_post_id : '',
            repost: edit && row ? row.repost : false,
            status: edit && row ? row.status : '',
        },
        resolver: yupResolver(validationSchema)
    };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        registerPost(data);
    }

    const registerPost = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        data.append("fb_ig_post_id", formData.postId);
        data.append("client_plan_id", client_plan_id);
        data.append("social_network", formData.socialNetwork);
        data.append("type", formData.type);
        data.append("planned_datetime", formData.plannedDateTime);
        data.append("repost", formData.repost ? '1' : '0');
        // data.append("planned_datetime", formData.plannedDateTime.split('T')[0]);
        data.append("title", formData.title);
        data.append("subtitle", formData.subtitle);
        data.append("instructions", formData.instructions);
        data.append("post_copy", formData.postCopy);        
        data.append("status", formData.status);
        
        if(mediaUrl){
            data.append("media_url", mediaUrl);
        }
        
        var url = process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPosts/addMobile";

        if(edit && row){
            url = process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPosts/editMobile/"+ row.id;
        }
    
        fetch(url, {
          method: 'POST',
          body: data,
        })
        .then(response => response.json())
        .then(data => {
            //console.log('VALOR ENDPOINTS: ', data);

            setIsLoading(false)
            if(data.status){
                reset();
                loadClientPlanData();
                setShowModal(false);
            } else {
                alert(JSON.stringify(data.errors, null, 4));
            }
            
        },
        (error) => {
            console.log(error)
        }
        )
    
    }

    return (
        <>
            <div>                
                {
                    edit && row
                    ?
                        <button
                            className="pt-1 pb-1 items-center flex"
                            onClick={() => setShowModal(true)}
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                className="w-5 h-5 text-[#643DCE]"
                            >
                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                            </svg>
                            {/* <p className="text-[14px] font-semibold hover:text-[#643DCE]">Edit</p> */}

                        </button>
                    :
                        <button
                            className="px-7 py-1 bg-[#582BE7] border border-[#582BE7] text-[#FFFFFF] text-sm rounded-[25px] hover:bg-[#fff] hover:border-[#582BE7] hover:text-[#582BE7]"
                            type="button"
                            onClick={() => setShowModal(true)}
                        >
                            Create Post
                        </button>
                }
                <ReactModal
                    isOpen={showModal}
                    shouldCloseOnEsc={true}
                    onRequestClose={() => setShowModal(false)}
                    style={customModalStyles}
                >
                    <div className="max-w-[600px] md:min-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                        <div>
                            <h4 className="text-lg font-medium text-gray-800">
                                {edit && row ? 'EDIT' : 'CREATE'} POST
                            </h4>
                        </div>

                        <div>
                            <form onSubmit={handleSubmit(onSubmit)} >

                                <div className="mt-[20px] grid grid-cols-12 gap-4">

                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Title</p>
                                        <input className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]' {...register('title')} />
                                        <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                                    </div>
                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>SubTitle</p>
                                        <input className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'{...register('subtitle')} />
                                        <div className="text-[14px] text-[#FF0000]">{errors.subtitle?.message}</div>
                                    </div>

                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Image</p>
                                        <input type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px]'
                                            onChange={(e) => {
                                                // setMediaUrl(e.target.files)
                                                if (e.target.files) {
                                                    setMediaUrl(e.target.files[0]);
                                                } else {
                                                    setMediaUrl(null);
                                                }
                                            }}
                                        />
                                        <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                                    </div>
                                    
                                    <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Platform</p>
                                        <select
                                            {...register('socialNetwork')}
                                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                        >
                                            <option value="Facebook">Facebook</option>
                                            <option value="Instagram">Instagram</option>
                                            <option value="TikTok">TikTok</option>
                                            <option value="Mailing">Mailing</option>
                                            <option value="LinkedIn">LinkedIn</option>
                                            <option value="YouTube">YouTube</option>
                                            <option value="X">X</option>
                                        </select>
                                        <div className="text-[14px] text-[#FF0000]">{errors.socialNetwork?.message}</div>
                                    </div>
                                    <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Type</p>
                                        <select
                                            {...register('type')}
                                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                        >
                                            <option value="Image">Image</option>
                                            <option value="Album">Album</option>
                                            <option value="Carousel">Carousel</option>
                                            <option value="Video">Video</option>
                                            <option value="Reel">Reel</option>
                                            <option value="Story">Story</option>
                                            <option value="CTA">CTA</option>
                                            <option value="Likes">Likes</option>
                                        </select>
                                        <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                                    </div>
                            
                                    <div className="col-span-12 md:col-span-5">
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Date</p>
                                        <input type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`} {...register('plannedDateTime')}  />
                                        <div className="text-[14px] text-[#FF0000]">{errors.plannedDateTime?.message}</div>
                                    </div>
                                    <div className='col-span-7 md:col-span-5'>
                                        <p className='text-xs text-gray-500 mb-[2px]'>*Status</p>
                                        <select
                                            {...register('status')}
                                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                        >
                                            <option value="PLANNED">PLANNED</option>
                                            <option value="DONE">DONE</option>
                                            <option value="POSTED">POSTED</option>
                                        </select>
                                        <div className="text-[14px] text-[#FF0000]">{errors.status?.message}</div>
                                    </div>
                                    <div className="col-span-5 md:col-span-2">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Repost?</p>
                                        <input type="checkbox" className={`w-full h-[17px] mt-2 bg-[#FFFFFF]`} {...register('repost')}  />
                                        <div className="text-[14px] text-[#FF0000]">{errors.repost?.message}</div>
                                    </div>
                                    
                                    {/* <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Select Time</p>
                                        <input name="time" type="time" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                        {...register('time')}
                                        value={time}
                                        onChange={(e) => {
                                            setTime(e.target.value)
                                        }}
                                        />
                                        <div className="text-[14px] text-[#FF0000]">{errors.time?.message}</div>
                                    </div> */}                                    
                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Copy</p>
                                        <textarea className='w-full min-h-[77px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] p-[10px] text-[12px]' {...register('postCopy')} />
                                        <div className="text-[14px] text-[#FF0000]">{errors.postCopy?.message}</div>
                                    </div>
                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Instructions</p>
                                        <input className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]' {...register('instructions')} />
                                        <div className="text-[14px] text-[#FF0000]">{errors.instructions?.message}</div>
                                    </div>


                                    <div className="col-span-12">
                                        <p className='text-xs text-gray-500 mb-[2px]'>Post ID</p>
                                        <input className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`} {...register('postId')} />
                                        <div className="text-[14px] text-[#FF0000]">{errors.postId?.message}</div>
                                    </div>
                                </div>

                                <div className="flex flex-row justify-between mt-[20px]">

                                    <div>
                                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                    </div>

                                    <div>
                                        <input
                                            readOnly={true}
                                            className="w-[75px] h-[35px] text-center border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                            onClick={() => {
                                                    reset();
                                                    setShowModal(false);
                                                }
                                            }
                                            disabled={isLoading}
                                            value={'Cancel'} 
                                        />
                                        <button
                                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                            type={'submit'}
                                            disabled={isLoading}
                                        >
                                            {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : (edit && row ? 'Update' : 'Create')}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>                    
                </ReactModal>
            </div>
        </>
    );
}