import React, { useEffect, useState } from 'react';
import { Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";



export default function ModalPostInsights(props) {

    const { client_plan_post_id} = props;

    const [showModal, setShowModal] = useState(false);

    const [loading, setLoading] = useState(true);

    const [insightsData, setInsightsData] = useState(null);

    const loadPostInsights = async () => {
        setLoading(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansPosts/getInsights/' + client_plan_post_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setInsightsData(data.data.insights.data);
            } else {
                console.error(data.errors);
            }
            setLoading(false);
        })
        .catch((e) => {
            console.error(e);
            setLoading(false);
        });
    }

    useEffect(() => {
        if(showModal){
            loadPostInsights();
        }   
    }, [showModal]);    

    return (
        <>
            <div>
                <button
                    className="hover:bg-[#582BE7] hover:text-[#FFFFFF] rounded-lg border border-[#582BE7] bg-[#fff] text-[#582BE7] px-4 py-1 text-base w-full md:w-auto md:text-xs md:px-2 md:rounded md:py-0"
                    // className="w-[112px] h-[22px] md:w-[200px] md:h-[48px] lg:w-[112px] lg:h-[22px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[10px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Insights
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[1000px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        Post insights
                                    </h4>
                                </div>

                                <div>
                                    <div className="mt-[20px] grid grid-cols-12 gap-4">
                                        {
                                            loading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots>
                                            :
                                            insightsData != null && typeof insightsData == 'object'
                                            ?
                                            (
                                                insightsData.map((insight, index) => {
                                                    return (
                                                        <div key={`insight-${index}`} className="col-span-6 md:col-span-6 lg:col-span-4 pb-4">
                                                            <div className="text-[12px] text-[#000000] text-left font-bold mb-0">{insight.title}</div>
                                                            <div className="text-[10px] text-[#000000] text-left mb-2">{insight.description}</div>
                                                            {
                                                                typeof insight.values[0].value == 'object' ?
                                                                Object.keys(insight.values[0].value).map((val_key) => {
                                                                    return (
                                                                        <p key={`insight-val-${index}`} className='text-[14px] text-[#582BE7] font-black p-0 leading-4'>{val_key}: {insight.values[0].value[val_key]}</p>
                                                                    )
                                                                })
                                                                : 
                                                                <p className='text-[37px] text-[#582BE7] font-black m-0 p-0'>{JSON.stringify(insight.values[0].value)}</p>
                                                            }
                                                            <p className='text-[10px] text-[#C1C1C1] mb-2'>{insight.name}</p>
                                                        </div>
                                                    )
                                                })
                                            ) 
                                            : <></>                                           
                                        }                                            
                                    </div>

                                    <div className="flex flex-row justify-between mt-[20px]">
                                        {/* <div>
                                            <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                        </div> */}

                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                                disabled={loading}
                                            >
                                                Cancelar
                                            </button>
                                            {/* <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                type={'submit'}
                                                disabled={loading}
                                            >
                                                {loading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                            </button> */}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}