import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import { useRouter } from 'next/router'
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useEffect, useState } from 'react'
// import TooltipAdsFormer from './tooltipAdsFormer';

import ImgFacebook from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/ClientPlan/logo.svg"
import marca from "../../../public/ClientPlan/marca.svg"
import editar from "../../../public/ClientPlan/editar.svg"
import DataTable from 'react-data-table-component';
import { getImgUrlForSocialNetwork, getMediaUrlByType } from '../../utils';
import moment from 'moment';
import 'moment/locale/es';
import TooltipAd from './tooltipAd';


const TableAds = (props) => {

    const {data, loading, loadClientPlanData, client_plan_id, editable} = props;

    const columns = [
        {
            name: 'Start',
            center: true,
            selector: row => moment(row.start_datetime).format("ddd MMM Do, YYYY"),            
            // sortable: true,
            // sortField: 'start_datetime',
            width: '147px',
        },
        {
            name: 'End',
            center: true,
            selector: row => moment(row.end_datetime).format("ddd MMM Do, YYYY"),           
            // sortable: true,
            width: '147px',
        },
        {
            name: 'Image',
            center: true,
            cell: (row, index, column, id) => (
                <div className='my-4'>
                    <Image
                        src={ row._matchingData.ClientsPlansPosts.media_url ? row._matchingData.ClientsPlansPosts.media_url : '/SocialMedia/no_img.jpg'}
                        width={57}
                        height={57}
                        objectFit='cover'
                    />
                </div>
            ),
            width: '107px',
        },
        {
            name: 'Platform',
            center: true,
            cell: (row, index, column, id) => (
                <Image
                    src={row._matchingData.ClientsPlansPosts.social_network ? getImgUrlForSocialNetwork(row._matchingData.ClientsPlansPosts.social_network) : '/SocialMedia/no_img.jpg'}
                    layout='fixed'
                    width={37}
                    height={37}
                />                
            ),
            width: '77px',
        },
        // {
        //     name: 'Type',
        //     center: true,
        //     cell: (row, index, column, id)  => (
        //         row._matchingData ?
        //             <Image
        //                 src={getMediaUrlByType(row._matchingData.ClientsPlansPosts.type)}
        //                 width={21}
        //                 height={21}
        //                 objectFit='cover'
        //             />
        //         : <></>                
        //     ),
        //     width: '77px',
        // },
        {
            name: 'Post',
            cell: (row, index, column, id) => (
                row._matchingData ? 
                <div>
                    <div className='font-base font-bold'>{row._matchingData.ClientsPlansPosts.title}</div>
                    <div className='text-sm font-light'>{row._matchingData.ClientsPlansPosts.subtitle}</div>
                    <div className='text-xs font-light text-[#A7A7B7] pt-1'>{row._matchingData.ClientsPlansPosts.post_copy}</div>
                </div>
                : <></>
            ),
        },
        {
            name: 'Amount',
            center: true,
            cell: row => (<div className='text-2xl font-semibold text-[#582BE7]'>{row.amount} Bs.</div>),
            width: '147px',
        },
        {
            name: '',
            right: true,
            cell: (row, index, column, id) => editable ? <TooltipAd row={row} loadClientPlanData={loadClientPlanData} client_plan_id={client_plan_id} /> : <></>,
            width: '27px',
        },
    ];

    return (
        loading ?
            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            :
            <DataTable
                columns={columns}
                data={data}
                highlightOnHover={true}
                striped={true}
            />
            // <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
            //     <TablaProductos data={data} loadClientPlanData={loadClientPlanData}  />
            // </div>
    )
};


const TablaProductos = (props) => {
    const { data } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);


    const splitStartDate = (obj) => {
        data.forEach(element => {
            let date = element.start_datetime.split("T");
            element.start_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitStartDate(data);

    const splitEndDate = (obj) => {
        data.forEach(element => {
            let date = element.end_datetime.split("T");
            element.end_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitEndDate(data);

    return (

        <div className="  bg-[#FFFF] grid grid-cols-12 ">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[12px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9] p-[5px]'>
                        <th>Date</th>
                        <th>SN</th>
                        <th>Post</th>
                        <th>Status</th>
                        <th>Amount</th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                     {

                        const imgUrl = "";

                        if(row._matchingData.ClientsPlansPosts.social_network !== null){
                            switch (row._matchingData.ClientsPlansPosts.social_network) {
                                case 'Facebook': 
                                    imgUrl = '/SocialMedia/Facebook.svg'
                                    break;
                                case 'TikTok':
                                    imgUrl= '/SocialMedia/TikTok.svg'
                                    break;
                        
                                case 'Instagram':
                                    imgUrl = '/SocialMedia/Instagram.svg'
                                break;
                                case 'YouTube':
                                    imgUrl = '/SocialMedia/Youtube.svg'
                                break;
                                case 'Mailing':
                                    imgUrl = '/Plans/gmail.svg'
                                break;
                                case 'LinkedIn':
                                    imgUrl = '/SocialMedia/messenger.svg'
                                break;
                                case 'Twitter':
                                    imgUrl = '/SocialMedia/Twitter.svg'
                                break;
                                    
                                default:
                                    break;
                            }
                        }

                        return (
                            <tr key={row.id} >
                            <td className='h-8 w-16 text-center text-[10px]'>
                                {row.start_datetime} <br></br>
                                {row.end_datetime}
                            </td>
                           
                            <td className='h-8 w-16 text-center '>
                                    {row._matchingData.ClientsPlansPosts.social_network !== null ?
                                        <Image
                                            src={imgUrl}
                                            alt='ImgFacebook'
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }

                            </td>
                            <td className='h-8 w-16 text-center '>

                                <div className='flex flex-row text-center '>
                                    {row._matchingData.ClientsPlansPosts.media_url !== null && row._matchingData.ClientsPlansPosts.media_url.trim().length !== 0 ?
                                        <Image
                                            className='rounded-md'
                                            src={row._matchingData.ClientsPlansPosts.media_url}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                    <div className='text-[12px] ml-[10px]'>
                                        {row._matchingData.ClientsPlansPosts.title}
                                        <div>
                                            <small className='text-gray-500 text-xs'>{row._matchingData.ClientsPlansPosts.subtitle}</small>
                                        </div>
                                        {row._matchingData.ClientsPlansPosts.post_copy}
                                    </div>
                                </div>


                            </td>


                            <td className='h-8 w-16 text-center text-[10px]'>
                                {row._matchingData.ClientsPlansPosts.status}

                            </td>
                            <td className='h-8 w-16 text-center text-[14px] font-semibold text-[#643DCE]'>
                                {row.amount !== null ?

                                    <div>
                                        {row.amount} Bs.
                                    </div>
                                    : <></>

                                }
                            </td>

                            <td className='h-8 w-16 text-center'>
                                {/* <TooltipAds
                                id={row.id}
                                startDatetime={row.start_datetime}
                                endDatetime={row.end_datetime}
                                amount={row.amount}
                                /> */}
                            </td>
                            

                        </tr>

                        )
                    }

                        
                    )}


                </tbody>
            </table>
        </div>
    );
};


export default TableAds;