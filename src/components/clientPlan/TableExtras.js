import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import TooltipPost from './tooltipPost';
import ModalPostInsights from './ModalPostInsights';
import DataTable from 'react-data-table-component';
import { getImgUrlForSocialNetwork, getMediaUrlByType } from '../../utils';
import TooltipExtra from './tooltipExtra';

const TableExtras = (props) => {

    const {data, loading, loadClientPlanData} = props;

    const columns = [
        {
            name: 'Added on',
            center: true,
            selector: row => row.created,
            sortable: true,
            width: '107px',
        },
        {
            name: 'Status',
            center: true,
            cell: (row, index, column, id) => <div className='rounded border border-[#582BE7] px-2 py-1 text-[10px] text-[#582BE7]'>{row.status}</div>,
            width: '107px',
        },
        {
            name: 'Image',
            center: true,
            cell: (row, index, column, id) => (
                <div
                className='my-4'>
                    <Image
                    src={ row.media_url ? row.media_url : '/SocialMedia/no_img.jpg'}
                    width={57}
                    height={57}
                    objectFit='cover'
                />
                </div>
            ),
            width: '107px',
        },
        {
            name: 'Type',
            center: true,
            cell: (row, index, column, id)  => {
                return (
                    row.social_network !== null ?
                        <Image
                            src={getMediaUrlByType(row.type)}
                            width={21}
                            height={21}
                            objectFit='cover'
                        />
                        // <Image
                        //     src={getImgUrlForSocialNetwork(row.social_network)}
                        //     alt=''
                        //     layout='fixed'
                        //     width={30}
                        //     height={30}
                        // />
                        : <></>
                )
                
            },
            width: '77px',
        },
        {
            name: 'Detail',
            cell: (row, index, column, id) => (
                <div>
                    <div className='font-base font-bold'>{row.detail}</div>
                    {/* <div className='text-sm font-light'>{row.subtitle}</div> */}
                    {/* <div className='text-xs font-light text-[#A7A7B7] pt-1'>{row.post_copy}</div> */}
                </div>
            ),
            grow: 1,
        },
        {
            name: 'Amount',
            center: true,
            cell: row => (<div className='text-2xl font-semibold text-[#582BE7]'>{row.price} Bs.</div>),
            width: '147px',
        },
        {
            name: '',
            right: true,
            cell: (row, index, column, id) => <TooltipExtra row={row} loadClientPlanData={loadClientPlanData} />,
            width: '27px',
        },
    ];

    return (
        loading ?
            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            :
            <DataTable
                columns={columns}
                data={data}
                highlightOnHover={true}
                striped={true}
            />
            // <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
            //     <TablaProductos data={data} loadClientPlanData={loadClientPlanData}  />
            // </div>
    )
}


const TablaProductos = (props) => {
    const { data, loadClientPlanData } = props;

    return (

        <div className="grid grid-cols-12 ">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-[12px] font-semibold text-[#000000] border-b-2 border-[#D9D9D9] text-center'>
                        <th>Date</th>
                        <th>SN</th>
                        <th>Post</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                        {

                            const imgUrl = "";

                            if(row.social_network !== null){
                                switch (row.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;
                                        
                                    default:
                                        break;
                                }
                            }
                            
                        return (
                        <tr key={row.id} >
                            <td className='h-8 w-16 text-[12px]'>
                                {row.planned_datetime}
                            </td>
                            <td className='h-8 w-16 text-[12px] text-center '>
                                {row.social_network !== null ?
                                    <Image
                                        src={imgUrl}
                                        alt=''
                                        layout='fixed'
                                        width={30}
                                        height={30}
                                    />
                                    : <></>
                                }

                            </td>

                            <td className='h-8 w-16 text-center'>


                                <div className='flex flex-row'>
                                    {row.media_url !== null && row.media_url.trim().length !== 0 ?
                                        <Image
                                            className='rounded-md'
                                            src={row.media_url}
                                            alt=''
                                            layout='responsive'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                    <div>
                                        <div className='text-[10px] ml-[10px]'>
                                            {row.title}
                                        </div>
                                        <div className='text-[8px] ml-[10px] text-[#D7D7D7]'>
                                            {row.subtitle}
                                        </div>
                                    </div>
                                </div>


                            </td>


                            <td className='h-8 w-16'>
                                <div className='bg-[#D9D9D9] rounded-[6px] text-[7px] text-center items-center pt-[2px] px-2 pb-[2px]'>
                                    {row.status}
                                </div>
                            </td>
                            <td className='h-8 w-16 text-center items-center'>
                                <TooltipPost
                                    row={row}
                                    loadClientPlanData={loadClientPlanData}
                                />
                            </td>

                            <td className='h-8 w-16 text-center items-center'>
                                {row.fb_ig_post_id != null && <ModalPostInsights client_plan_post_id={row.id} />}
                            </td>

                        </tr>
                            )


                        }
                    )}


                </tbody>
            </table>
        </div>
    );
};





export default TableExtras;