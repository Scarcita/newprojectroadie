import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterClient(props) {
    // const [isLoading, setIsLoading] = useState(true)
    // const [data, setData] = useState([ ]);

    const {loading, type, data} = props;
    const [finalValue, setFinalValue] = useState(0);

    useEffect(() => {
        if(data != null && typeof data === 'object'){
            switch (type) {
                case 'service_total':
                    if('service_total' in data) setFinalValue(data.service_total);
                    break;
                case 'ads_total':
                    if('ads_total' in data) setFinalValue(data.ads_total);
                    break;
                case 'total':
                    if('total' in data) setFinalValue(data.total);
                    break;
                default:
                    break;
            }
           }
    }, [data]);

    // useEffect(() => {
    //     fetch('https://slogan.com.bo/vulcano/orders/total/abierto')
    //         .then(response => response.json())
    //         .then(data => {
    //             if (data.status) {
    //                 console.log(data.data);
    //                 setData(data.data)
    //             } else {
    //                 console.error(data.error)
    //             }
    //         setIsLoading(false)
    //         })
    // }, [])

    return (

        loading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{finalValue} <span className="text-[12px]">USD</span> </div>
    )
}