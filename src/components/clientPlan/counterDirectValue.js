import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterDirectValue(props) {

    const {value} = props;

    return (
        <div>{value}<span className="text-[12px] ml-1">Bs.</span> </div>
    )
}