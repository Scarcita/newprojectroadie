import React, { useState, useEffect } from "react";
import Image from 'next/image';
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";
import ReactModal from "react-modal";
import ModalExtra from "./ModalExtra";
import { customModalStyles } from "../../utils";

export default function TooltipExtra(props) {

    const { row, loadClientPlanData} = props;
    const [isLoading, setIsLoading] = useState(false)
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const eliminarDatos = ( ) => {
        
        setIsLoading(true);

        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/clientsPlansExtras/deleteMobile/' + row.id, {
            method: "POST"
        })
        .then(response => response.json())
        .then(data => {
            setIsLoading(false);
            if (data.status) {
                // console.log('ELIMINAR DATOS: ' + data.status);
                loadClientPlanData();
            } else {
                console.error('Errors: ' + data.errors)
            }
        });

    }

    return (
        <>
            <Tippy
                trigger='click'
                placement={'left'}
                animation='scale'
                theme='light'
                interactive={true}
                content={
                    <>
                        <div
                            className="flex flex-col justify-left items-left "
                        >
                            <ModalExtra
                                loadClientPlanData={loadClientPlanData}
                                client_plan_id={row.client_plan_id}
                                row={row}
                                edit={true}
                            />

                            <button
                                className="pt-1 pb-1 items-center flex"
                                onClick={() => setShowDeleteModal(true)}
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth="1.5"
                                    stroke="currentColor"
                                    className="w-5 h-5 text-[#FF0000]"
                                >
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                </svg>
                                {/* <p className="text-[14px] font-semibold hover:text-[#FF0000]">
                                    Eliminar
                                </p> */}
                            </button>
                        </div>
                    </>
                }>
                <button>
                    <svg
                        width="8"
                        height="28"
                        viewBox="0 0 8 28"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className='w-[10px] h-[17px] '
                    >
                        <path d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z" fill="#D9D9D9" />
                    </svg>
                </button>
            </Tippy>

            <ReactModal
                isOpen={showDeleteModal}
                onRequestClose={() => setShowDeleteModal(false)}
                style={customModalStyles}
            >                
                <div className="max-w-[600px] md:min-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                    <div className="flex flex-col justify-center align-center">
                        <div className={'text-red hover:text-red'}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-16 h-16 text-[#FF0000]">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>

                        <div>
                            <h1 className="text-[32px]">
                                Are you sure?
                            </h1>
                        </div>

                        <div className="flex flex-col justify-center items-center mt-[22px] ">
                            <p className="text-[18px]  font-semibold ">
                                Do you really want to delete these records?
                            </p>
                            <p className="text-[18px] font-light ">
                                This process cannot be undone
                            </p>
                        </div>

                        <div className="flex flex-row justify-between mt-[45px]">
                            <div>
                                <input
                                    readOnly={true}
                                    className="w-[85px] h-[45px] text-center border-[1px] border-[#3682F7] rounded-[20px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                    onClick={() =>
                                        setShowDeleteModal(false)
                                    }
                                    value={'Cancel'}                                
                                />
                                <button
                                    className="w-[85px] h-[45px] border-[1px] bg-[#FF0000] rounded-[20px] text-[#FFFF] hover:border-[#FF0000] hover:bg-[#FFFF] hover:text-[#FF0000] text-[14px] mt-[3px]"
                                    onClick={() => {
                                        eliminarDatos();
                                        setShowDeleteModal(false);
                                    }}
                                >                                
                                    {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Delete'}
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </ReactModal>
        </>
    )
}