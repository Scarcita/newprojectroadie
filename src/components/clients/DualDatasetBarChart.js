import { useEffect, useState } from 'react';
import Chart from "chart.js";
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

export default function DualDatasetBarChart(props) {

  const {loading, data, title1, title2} = props;

  const [uberData, setUberData] = useState({'labels': [], 'data1' : [], 'data2' : []}); 

  const [chart, setChart] = useState(null);
  
  const initializeChart = () => {
    let config = {
      type: "bar",
      data: {
        labels: [],
        datasets: [
          {
            label: title1,
            backgroundColor: "#582BE7",
            //borderColor: "#4a5568",
            data: [],
            // fill: false,
            barThickness: 12,
          },
          {
            label: title2,
            backgroundColor: "#4a5568",
            //borderColor: "#4a5568",
            data: [],
            // fill: false,
            barThickness: 12,
          }
        ],
      },
      options: {
        responsive: true,
      },
    };

    let ctx = document.getElementById("bar-chart");
    setChart(new Chart(ctx, config));
  }

  useEffect(() => {
    initializeChart();
  }, []);

  useEffect(() => {
    if(data && data.graph_data){
      if(data.graph_data.labels !== undefined && data.graph_data.ads_by_client !== undefined && data.graph_data.planned_service_earnings_by_client !== undefined){
        console.log('data ->', data);
        setUberData(data.graph_data);
      }
    }
  }, [data]);

  useEffect(() => {
    if(chart && uberData.labels.length > 0){
      chart.data.datasets[0].data = uberData.ads_by_client;
      chart.data.datasets[1].data = uberData.planned_service_earnings_by_client;
      chart.data.labels = uberData.labels;
      chart.update();
      document.getElementById("bar-chart-container").style.opacity = 1;
    }
  }, [uberData]);

  return (
    <>
      { loading && <Skeleton></Skeleton> }
      {/* <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-[20px] shadow-md rounded-md">
        <div className="relative h-[250px]"> */}
      <div id="bar-chart-container" className="" style={{opacity: 0, padding: 20}}>
          <canvas id="bar-chart"></canvas>
      </div>
    </>
  );
}
