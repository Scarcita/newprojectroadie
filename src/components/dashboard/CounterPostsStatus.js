import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';

export default function CounterPostsStatus(props) {
    const { loading, plannedValue, postedValue } = props;
    return (
        <>
            {
                loading 
                ?
                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                        <Spinner color="#fff" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                    </div>
                :
                    <div>{postedValue}&#47;<span className='text-gray-200'>{plannedValue}</span></div>
            }
        </>
    )
}