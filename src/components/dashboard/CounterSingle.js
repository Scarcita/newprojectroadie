import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';

export default function CounterSingle(props) {
    const { loading, value } = props;
    return (
        loading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{value}</div>
    )
}