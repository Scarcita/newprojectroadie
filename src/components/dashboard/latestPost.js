import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import Link from "next/link";
import ModalEditLatestPosted from './EditLatestPosted';
import Post from '../shared/Post';


export default function LatestPost(props) {
  const {loading, data, title} = props;

  return (
    <> 
      {
        loading 
        ?    
          <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
              <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
          </div>
        :
          <div>
            <div className='text-[17px] md:text-[18px] lg:text-[14px] my-4 text-[#000000] font-bold'>
              {title}
            </div>                
            {
              data 
              &&                   
                <Post post={data} />
            }
          </div>
      }
    </>
  )
}