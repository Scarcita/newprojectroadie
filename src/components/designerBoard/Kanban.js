import React, { useEffect, useState } from 'react';
import TaskCard from '../../../pages/designerBoard/TaskCard';
import dynamic from 'next/dynamic';
import { Spinner} from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";

const Kanban = (props) => {

  const {columns, setColumns, reloadFunction, loading, setLoading} = props;

  const DragDropContext = dynamic(
    () =>
      import('react-beautiful-dnd').then(mod => {
        return mod.DragDropContext;
      }),
    {ssr: false},
  );
  
  const Droppable = dynamic(
    () =>
      import('react-beautiful-dnd').then(mod => {
        return mod.Droppable;
      }),
    {ssr: false},
  );

  const updatePostStatus = async (post_id, status) => {
    setLoading(true);

    try {
  
      var data = new FormData();
  
      data.append("status", status );

      const response = await fetch(process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPosts/editMobile/" + post_id , {
        method: 'POST',
        body: data,
      });
      
      const res = await response.json();
      console.log("res", res);

      if (res.status) {

        await reloadFunction();      

      } else {
          console.error(res.errors);
      }

    } catch (error) {
        console.error("Error in updatePostStatus", error);
    } finally {
        setLoading(false);
    }
  }

  const onDragEnd = (result) => {
    if (!result.destination) return;
    const { source, destination } = result;

    // console.log('source', source);
    // console.log('destination', destination);

    if (source.droppableId !== destination.droppableId) {

      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      // const sourceItems = [...sourceColumn.items];
      // const destItems = [...destColumn.items];
      // const [removed] = sourceItems.splice(source.index, 1);
      // destItems.splice(destination.index, 0, removed);

      updatePostStatus( sourceColumn.items[source.index].id, destColumn.title );

    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  return (   
    
    loading ? <></>
    :
    <DragDropContext onDragEnd={(result) => onDragEnd(result)}>

      <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-7 gap-5'>
        {
          Object.entries(columns).map(([columnId, column], index) => {
              return (
                <Droppable key={index} droppableId={columnId}>
                    {(provided) => (
                    <div
                        className='bg-[#F3F3F3] rounded-[17px] px-4 py-2'
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                    >
                      
                      <div className='bg-[#582BE7] mt-2 mb-4 inline-block px-4 rounded-[17px] text-[#fff] font-semibold'>
                          {column.title}
                      </div>

                      <div>
                        {column.items.map((item, index) => (                        
                          <TaskCard key={index} item={item} index={index} reloadFunction={reloadFunction} />                        
                        ))}
                      </div>
                      
                      {/* {provided.placeholder} */}
                    </div>
                    )}
                </Droppable>            
              )
          })
        }
      </div>
      
    </DragDropContext>
  );
};

export default Kanban;