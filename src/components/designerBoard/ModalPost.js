import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactModal from 'react-modal';
import { customModalStyles } from '../../utils';



export default function ModalPost(props) {

    const { item, reloadFunction } = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [mediaUrl, setMediaUrl] = useState(null);

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm();
    const { errors } = formState;

    function onSubmit(data) {
        registerPost(data);
    }

    const registerPost = async (formData) => {

        setIsLoading(true)
        var data = new FormData();
        
        if(mediaUrl){
            data.append("media_url", mediaUrl);
        } else {
            return;
        }
        
        var url = process.env.NEXT_PUBLIC_SERVER_URL + "/clientsPlansPosts/editMobile/"+ item.id;
    
        fetch(url, {
          method: 'POST',
          body: data,
        })
        .then(response => response.json())
        .then(data => {
            //console.log('VALOR ENDPOINTS: ', data);

            setIsLoading(false)
            if(data.status){
                reset();
                reloadFunction();
                setShowModal(false);
            } else {
                alert(JSON.stringify(data.errors, null, 4));
            }
            
        },
        (error) => {
            console.log(error)
        }
        )
    
    }

    return (
        <>               
            {
                <button
                    className="pt-1 pb-1 items-center flex"
                    onClick={() => setShowModal(true)}
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="w-5 h-5 text-[#643DCE]"
                    >
                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                    </svg>
                    {/* <p className="text-[14px] font-semibold hover:text-[#643DCE]">Edit</p> */}

                </button>
            }
            <ReactModal
                isOpen={showModal}
                shouldCloseOnEsc={true}
                onRequestClose={() => setShowModal(false)}
                style={customModalStyles}
            >
                <div className="max-w-[600px] md:min-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                    <div>
                        <h4 className="text-lg font-medium text-gray-800">
                            EDIT POST IMAGE
                        </h4>
                    </div>

                    <div>
                        <form onSubmit={handleSubmit(onSubmit)} >

                            <div className="mt-[20px] grid grid-cols-12 gap-4">

                                <div className="col-span-12">
                                    <p className='text-xs text-gray-500 mb-[2px]'>*Image</p>
                                    <input type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px]'
                                        onChange={(e) => {
                                            // setMediaUrl(e.target.files)
                                            if (e.target.files) {
                                                setMediaUrl(e.target.files[0]);
                                            } else {
                                                setMediaUrl(null);
                                            }
                                        }}
                                    />
                                    <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                                </div>

                            </div>

                            <div className="flex flex-row justify-between mt-[20px]">

                                <div>
                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                </div>

                                <div>
                                    <input
                                        readOnly={true}
                                        className="w-[75px] h-[35px] text-center border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                        onClick={() => {
                                                reset();
                                                setShowModal(false);
                                            }
                                        }
                                        disabled={isLoading}
                                        value={'Cancel'} 
                                    />
                                    <button
                                        className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                        type={'submit'}
                                        disabled={isLoading}
                                    >
                                        {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Update'}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>                    
            </ReactModal>
        </>
    );
}