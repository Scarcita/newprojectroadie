import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterPlans(props) {
    const {reloadPlansAll,} = props
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([ ]);
    const [counterPlans, setCounterPlans] = useState(0);
    console.log(counterPlans);
    useEffect(() => {
        fetch(process.env.NEXT_PUBLIC_SERVER_URL + '/plans/all/')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    data.data.map((result) => {
                        temp.push(result)
                    })

                    let temporal = 0;
                    for (let i = 0; i < temp.length; i++) {
                        temp[i].name !== null ?
                            (
                                temporal++,
                                setCounterPlans(temporal)
                            )
                            :
                            null
                    }
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadPlansAll])

    return (

        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{counterPlans}</div>
    )
}