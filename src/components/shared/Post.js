import Image from "next/image";
import ModalEditLatestPosted from "../dashboard/EditLatestPosted";
import moment from 'moment';

const Post = (props) => {

    const {post} = props;

    return (
        post &&
        <div className='grid grid-cols-12 bg-[#fff] shadow-md rounded-[20px] p-[20px]'>
            <div className="col-span-12 flex flex-row">
                <div className="w-[48px] h-[48px] rounded-full">
                    {post.clients_plan.client.img_url != null && post.clients_plan.client.img_url.length > 0 ?
                    
                        <Image 
                            className='rounded-full'
                            src={post.clients_plan.client.img_url}
                            alt='media'
                            layout='fixed'
                            height={48}
                            width={48}
                        /> 
                        : <></>  
                    }    
                </div>
                <div className="pl-[10px]">        
                    {post.clients_plan.client.name !== null ?
                        <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal '>
                        {post.clients_plan.client.name}
                        </p>
                        : <></>
                    }
                    <p className='text-[12px]'>
                        {moment(post.planned_datetime).format("ddd")} {moment(post.planned_datetime).format("DD")} {moment(post.planned_datetime).format("MMM")}, {moment(post.planned_datetime).format("YYYY")}
                    </p>    
                </div>    
            </div>
            <div className='col-span-12 mt-[15px] items-center text-center'>
                {post.media_url != null && post.media_url.length > 0 ?
                    
                    <Image
                        className=''
                        src={post.media_url}
                        alt='media'
                        layout='responsive'
                        height={260}
                        width={260}
                    />
                    : <></>  
                } 
            </div>
            {post.post_copy !== null && post.post_copy !== undefined ?
                <p className='mt-[5px] text-[12px] leading-4 whitespace-normal'>{post.post_copy}</p>
                : <></>
            }
            {/* <div className='col-span-12 mt-[15px]'> 

                <ModalEditLatestPosted
                    id={post.id}
                    client_plan_id={post.client_plan_id}
                    title={post.title}
                    subtitle={post.subtitle}
                    post_copy={post.post_copy}
                    social_network={post.social_network}
                    planned_datetime={post.planned_datetime}
                    type={post.type}
                    instructions={post.instructions}
                    // reloadLatestPost={reloadLatestPost}
                    // setReloadLatestPosted={setReloadLatestPosted}
                />
                
            </div> */}
            
        </div>
    )
}
export default Post;