import { useSession, signIn, signOut } from "next-auth/react"
// import { useRouter } from 'next/router'

export default function LoginCheckerButton() {
  const { data: session } = useSession();  
  // const router = useRouter();

  if (session) {
    return (
      <>
        Signed in as {session.user.name} <br />
        Role: {session.user.role} <br />
        <button onClick={() => signOut({ callbackUrl: '/' })}>Sign out</button>
      </>
    )
  }
  return (
    <>
      Not signed in <br />
      <button onClick={() => signIn()}>Sign in</button>
      {/* <button onClick={() => router.push('/login')}>Sign in</button> */}
    </>
  )
}