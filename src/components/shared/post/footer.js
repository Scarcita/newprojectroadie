export default function Footer(props) {

  const { caption, username } = props;

  return (
    username && caption
    &&
    <div className="px-4 mt-2 mb-4 line-clamp-2">
      <span className="mr-1 font-bold">{username.toLowerCase()}</span>
      <span>{caption}</span>
    </div>
  );
}