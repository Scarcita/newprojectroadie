import Link from "next/link";
import Image from 'next/image';

export default function Header(props) {

  const { username, profile_img, post_type} = props;

  const getMediaUrlByType = (post_type)=> {
    var imgUrl = '/Board/image.png';

    switch (post_type) {
        case 'Image':
            imgUrl = '/Board/image.png';
            break;
        case 'Video':
            imgUrl = '/Board/video.png';
            break;
        case 'Carousel':
            imgUrl = '/Board/image.png';
            break;
        case 'Story':
            imgUrl = '/Board/image.png';
            break;                        
        case 'Image':
            imgUrl = '/Board/image.png';
            break;
        default:
            break;
    }

    return imgUrl;
}

  const post_type_img = post_type ? getMediaUrlByType(post_type) : null;

  return (
    <div className="flex border-b border-gray-primary h-4 p-4 py-8 justify-between items-center">
      <div className="flex items-center">
        <Link href={`https://instagram.com/p/${username}`} className="flex items-center">
          <>
            {profile_img? (
              <Image
                className="rounded-full h-8 w-8"
                width={27}
                height={27}
                objectFit="cover"
                src={profile_img}
                alt=""
              />
            ) : (
              <svg
                className="h-8 w-8 mr-3 text-black-light cursor-pointer"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
            )}
            <p className="ml-2 font-bold">{username.toLowerCase()}</p>
          </>
        </Link>
      </div>
      <div>
        {
          post_type_img 
          &&
          <Image
              src={post_type_img}
              width={21}
              height={21}
              objectFit='contain'
          />
        }
      </div>
    </div>
  );
}

// Header.propTypes = {
//   username: PropTypes.string.isRequired,
// };
