import Header from "./header";
import PostImage from "./image";
import Footer from "./footer";

export default function IgPost(props) {

  const {content} = props;

  return (
    <div className="rounded col-span-4 border bg-white border-gray-primary mb-4">
      <Header username={content.username} profile_img={content.userImg} post_type={content.postType} />
      <PostImage src={content.imageSrc} />
      <Footer username={content.username} caption={content.caption} />
    </div>
  );
}