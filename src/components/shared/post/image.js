import Image from 'next/image'

export default function PostImage(props) {

  const { src } = props;

  return (
    <Image
          src={src}
          alt={'post_img'}
          width={320}
          height={320}
          objectFit='contain'
      />
  )
}