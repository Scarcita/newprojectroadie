export const getMediaUrlByType = (post_type)=> {
    var imgUrl = '/SocialMedia/missing_img_1.svg';
    switch (post_type) {
        case 'Image':
            imgUrl = '/SocialMedia/image.svg';
            break;
        case 'Video':
            imgUrl = '/SocialMedia/video.svg';
            break;
        case 'Album':
            imgUrl = '/SocialMedia/album.svg';
            break;
        case 'Story':
            imgUrl = '/SocialMedia/story.svg';
            break;  
        case 'Carousel':
            imgUrl = '/SocialMedia/carousel.svg';
            break;                      
        case 'Reel':
            imgUrl = '/SocialMedia/reel.svg';
            break;
        default:
            break;
    }
    return imgUrl;
}

export const getImgUrlForSocialNetwork = (social_network) => {
    var imgUrl = '/SocialMedia/missing_img_1.svg';
    switch (social_network) {
        case 'Facebook': 
            imgUrl = '/SocialMedia/Facebook.svg'
            break;
        case 'TikTok':
            imgUrl= '/SocialMedia/TikTok.svg'
            break;
        case 'Instagram':
            imgUrl = '/SocialMedia/Instagram.svg'
        break;
        case 'YouTube':
            imgUrl = '/SocialMedia/Youtube.svg'
        break;
        case 'Mailing':
            imgUrl = '/Plans/gmail.svg'
        break;
        case 'LinkedIn':
            imgUrl = '/SocialMedia/messenger.svg'
        break;
        case 'Twitter':
            imgUrl = '/SocialMedia/Twitter.svg'
        break;
            
        default:
            break;
    }
    return imgUrl;
}

export const customModalStyles = {
    content: {
        maxWidth: '600px',
        background: 'rgba(0, 0, 0, 0)',
        border: 'none',
        margin: 'auto',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        inset: '10px',
        padding: '0px',
    },
    overlay: {            
        background: 'rgba(0, 0, 0, 0.77)',
    }
}; 