/** @type {import('tailwindcss').Config} */ 
module.exports = {
    content: [
      './src/components/**/*.{html,js}',
      './components/**/*.{html,js}',
      './pages/**/*.{html,js}',
      './index.html',
    ],
    theme: {
      extend: {
        colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'white': '#FFFFFF',
        'whit2': '#FCFDFE',
        'purple': '#3f3cbb',
        'midnight': '#121063',
        'metal': '#565584',
        'tahiti': '#3ab7bf',
        'silver': '#ecebff',
        'bubble-gum': '#ff77e9',
        'bermuda': '#78dcca',
        'gris': '#F6F6FA',
        'blue': '#3682F7',
        'black': '#000000'
        },
        boxShadow: {
          'Shadow-blue': '0px 4px 14px #3682F7;',
        }
      },
      fontFamily: {
        DMSans : ['DM Sans', 'sans-serif'],
      },
      extend: {},
    },
    variants: {
      extend: {
        animation: ['group-hover', 'animate-pulse'],
      },
    },
    plugins: [
      require('@tailwindcss/line-clamp'),
    ],
  }